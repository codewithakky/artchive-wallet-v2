function interceptTimeout(
  method: 'setTimeout' | 'setInterval',
  checkProp: '$$artchiveDisabledSetTimeout' | '$$artchiveDisabledSetInterval',
) {
  const methodOld = global[method];

  // @ts-ignore
  global[method] = function (
    fn: (...args: any[]) => any,
    timeout: number | undefined,
  ) {
    return methodOld(() => {
      if (global[checkProp]) {
        console.error(`${method} is disabled`);
        return;
      }
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return fn();
    }, timeout);
  };
}

function interceptTimerWithDisable() {
  try {
    interceptTimeout('setTimeout', '$$artchiveDisabledSetTimeout');
  } catch (error) {
    console.error(error);
  }
  try {
    interceptTimeout('setInterval', '$$artchiveDisabledSetInterval');
  } catch (error) {
    console.error(error);
  }
}

function enableSetTimeout() {
  global.$$artchiveDisabledSetTimeout = undefined;
}

function disableSetTimeout() {
  global.$$artchiveDisabledSetTimeout = true;
}

function enableSetInterval() {
  global.$$artchiveDisabledSetInterval = undefined;
}

function disableSetInterval() {
  global.$$artchiveDisabledSetInterval = true;
}

export default {
  interceptTimerWithDisable,
  enableSetTimeout,
  disableSetTimeout,
  enableSetInterval,
  disableSetInterval,
};
