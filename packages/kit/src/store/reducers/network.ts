import { createSlice } from '@reduxjs/toolkit';

import type { Network } from '../typings';
import type { PayloadAction } from '@reduxjs/toolkit';

type InitialState = {
  network: Network[] | null;
  rpcEnvrionment: string | null;
};

const initialState: InitialState = {
  network: null,
  rpcEnvrionment: null,
};

export const networkSlice = createSlice({
  name: 'network',
  initialState,
  reducers: {
    updateNetworkMap: (
      state,
      action: PayloadAction<InitialState['network']>,
    ) => {
      state.network = action.payload;
    },
    setRpcEnvironment: (state, action: PayloadAction<string>) => {
      state.rpcEnvrionment = action.payload;
    },
  },
});

export const { updateNetworkMap, setRpcEnvironment } = networkSlice.actions;

export default networkSlice.reducer;
