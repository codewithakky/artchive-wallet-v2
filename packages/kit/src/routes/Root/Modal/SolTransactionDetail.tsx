import { useIsVerticalLayout } from '@onekeyhq/components';

import { SolTxHistoryDetailModal } from '../../../views/TxHistory/SolTxHistoryDetailModal';
import { TransactionDetailModalRoutes } from '../../routesEnum';

import createStackNavigator from './createStackNavigator';

import type { ESolHistoryTx } from '../../../views/Wallet/HistoricalRecords/useHistoricalRecordsData';

export type SolTransactionDetailRoutesParams = {
  [TransactionDetailModalRoutes.SolHistoryDetailModal]: {
    solTxHis: ESolHistoryTx;
  };
};

const TransactionDetailNavigator =
  createStackNavigator<SolTransactionDetailRoutesParams>();

const modalRoutes = [
  {
    name: TransactionDetailModalRoutes.SolHistoryDetailModal,
    // component: HistoryDetail,
    component: SolTxHistoryDetailModal,
  },
];

const TransactionDetailModalStack = () => {
  const isVerticalLayout = useIsVerticalLayout();
  return (
    <TransactionDetailNavigator.Navigator
      screenOptions={{
        headerShown: false,
        animationEnabled: !!isVerticalLayout,
      }}
    >
      {modalRoutes.map((route) => (
        <TransactionDetailNavigator.Screen
          key={route.name}
          name={route.name}
          component={route.component}
        />
      ))}
    </TransactionDetailNavigator.Navigator>
  );
};

export default TransactionDetailModalStack;
