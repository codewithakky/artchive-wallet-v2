import { useIsVerticalLayout } from '@onekeyhq/components';
import type { IWallet } from '@onekeyhq/engine/src/types';
import type { AccountCredential } from '@onekeyhq/engine/src/types/account';

import BackupAttentions from '../../../views/BackupWallet/BackupAttentions';
import BackupLite from '../../../views/BackupWallet/BackupLite';
import BackupManual from '../../../views/BackupWallet/BackupManual';
import BackupMnemonic from '../../../views/BackupWallet/BackupMnemonic';
import BackupOptions from '../../../views/BackupWallet/BackupOptions';
import { KeyTagRoutes } from '../../../views/KeyTag/Routes/enums';
import KeyTagBackupWalletAttentions from '../../../views/KeyTag/Screen/KeyTagAttentions';
import ShowDotMap from '../../../views/KeyTag/Screen/ShowDotMap';
import VerifyLogin from '../../../views/KeyTag/Screen/VerifyLogin';
import VerifyPassword from '../../../views/KeyTag/Screen/VerifyPassword';
import { BackupWalletModalRoutes } from '../../routesEnum';

import { buildModalStackNavigatorOptions } from './buildModalStackNavigatorOptions';
import createStackNavigator from './createStackNavigator';

import type {
  IKeytagRoutesParams,
  IkeyTagShowDotMapParams,
} from '../../../views/KeyTag/Routes/types';

export type BackupWalletRoutesParams = {
  [BackupWalletModalRoutes.BackupWalletManualModal]: {
    walletId: string;
    type: string;
    credentialInfo?: AccountCredential;
  };
  [BackupWalletModalRoutes.BackupWalletOptionsModal]: {
    walletId: string;
  };
  [BackupWalletModalRoutes.BackupWalletLiteModal]: {
    walletId: string;
  };
  [BackupWalletModalRoutes.BackupWalletAttentionsModal]: {
    walletId: string;
    password: string;
    type: string;
    credentialInfo?: AccountCredential;
  };
  [BackupWalletModalRoutes.BackupWalletMnemonicModal]: {
    mnemonic: string;
    walletId: string;
    credentialInfo?: AccountCredential;
    password?: string;
  };
  [KeyTagRoutes.KeyTagVerifyPassword]: {
    walletId: string;
    wallet?: IWallet;
  };
  [KeyTagRoutes.KeyTagAttention]: {
    walletId: string;
    password: string;
    wallet?: IWallet;
  };
  [KeyTagRoutes.VerifyLogin]: undefined;
  [KeyTagRoutes.ShowDotMap]: IkeyTagShowDotMapParams;
};

const BackupWalletNavigator = createStackNavigator<
  BackupWalletRoutesParams & IKeytagRoutesParams
>();

const modalRoutes = [
  {
    name: BackupWalletModalRoutes.BackupWalletManualModal,
    component: BackupManual,
  },
  {
    name: BackupWalletModalRoutes.BackupWalletAttentionsModal,
    component: BackupAttentions,
  },
  {
    name: BackupWalletModalRoutes.BackupWalletMnemonicModal,
    component: BackupMnemonic,
  },
  {
    name: BackupWalletModalRoutes.BackupWalletOptionsModal,
    component: BackupOptions,
  },
  {
    name: BackupWalletModalRoutes.BackupWalletLiteModal,
    component: BackupLite,
  },
  { name: KeyTagRoutes.KeyTagVerifyPassword, component: VerifyPassword },
  { name: KeyTagRoutes.VerifyLogin, component: VerifyLogin },
  {
    name: KeyTagRoutes.KeyTagAttention,
    component: KeyTagBackupWalletAttentions,
  },
  {
    name: KeyTagRoutes.ShowDotMap,
    component: ShowDotMap,
  },
];

const BackupWalletModalStack = () => {
  const isVerticalLayout = useIsVerticalLayout();
  return (
    <BackupWalletNavigator.Navigator
      screenOptions={(navInfo) => ({
        headerShown: false,
        ...buildModalStackNavigatorOptions({ isVerticalLayout, navInfo }),
      })}
    >
      {modalRoutes.map((route) => (
        <BackupWalletNavigator.Screen
          key={route.name}
          name={route.name}
          component={route.component}
        />
      ))}
    </BackupWalletNavigator.Navigator>
  );
};

export default BackupWalletModalStack;
