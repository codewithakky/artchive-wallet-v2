import { useEffect } from 'react';

import { Box, useProviderValue } from '@onekeyhq/components';
import { setMainScreenDom } from '@onekeyhq/components/src/utils/SelectAutoHide';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { NetworkAccountSelectorEffectsSingleton } from '../../../components/NetworkAccountSelector/hooks/useAccountSelectorEffects';
import { WalletSelectorEffectsSingleton } from '../../../components/WalletSelector/hooks/useWalletSelectorEffects';
// import { useActiveWalletAccount, useNavigation } from '../../../hooks';
import { available, enable } from '../../../store/reducers/autoUpdater';
import { createLazyComponent } from '../../../utils/createLazyComponent';
import appUpdates from '../../../utils/updates/AppUpdates';
// import { useAppSelector } from '../../../hooks';
// import { EOnboardingRoutes } from '../../../views/Onboarding/routes/enums';
// import { RootRoutes } from '../../routesEnum';

// import type { ModalScreenProps, RootRoutesParams } from '../../types';

const UpdateAlert = createLazyComponent(
  () => import('../../../views/Update/Alert'),
);

const Drawer = createLazyComponent(() => import('./Drawer'));
// type NavigationProps = ModalScreenProps<RootRoutesParams>;

function MainScreen() {
  // const isStatusUnlock = useAppSelector((s) => s.status.isUnlock);
  const { dispatch } = backgroundApiProxy;
  // const { walletId } = useActiveWalletAccount();
  const { reduxReady } = useProviderValue();
  // const navigation = useNavigation<NavigationProps['navigation']>();
  useEffect(() => {
    if (reduxReady) {
      appUpdates.addUpdaterListener();
      appUpdates
        .checkUpdate()
        ?.then((versionInfo) => {
          if (versionInfo) {
            dispatch(enable(), available(versionInfo));
          }
        })
        .catch();
    }
  }, [dispatch, reduxReady]);
  // if (!walletId) {
  //   return navigation?.navigate(RootRoutes.Onboarding, {
  //     screen: EOnboardingRoutes.Welcome,
  //   });
  // }
  // if (!isStatusUnlock) {
  //   return null;
  // }

  return (
    <>
      {
        <Box ref={setMainScreenDom} w="full" h="full">
          <Drawer />
          <NetworkAccountSelectorEffectsSingleton />
          <WalletSelectorEffectsSingleton />
          {/* TODO Waiting notification component */}
          <UpdateAlert />
        </Box>
      }
    </>
  );
}

export default MainScreen;
