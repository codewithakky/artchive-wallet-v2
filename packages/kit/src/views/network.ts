import debugLogger from '@onekeyhq/shared/src/logger/debugLogger';

import { serverPresetNetworks } from './presetNetworks';

import type { IServerNetwork, PresetNetwork } from './presetNetworks';

// import simpleDb from '../dbs/simple/simpleDb';
//  network.id => PresetNetwork
// let presetNetworks: Record<string, PresetNetwork> = {};

export const formatServerNetworkToPresetNetwork = (
  network: IServerNetwork,
): PresetNetwork => {
  const urls = network.rpcURLs?.map((rpc) => rpc.url) ?? [];
  return {
    id: network.id,
    impl: network.impl,
    name: network.name,
    symbol: network.symbol,
    decimals: network.decimals,
    logoURI: network.logoURI,
    enabled: !network.isTestnet && network.defaultEnabled,
    chainId: network.chainId,
    shortCode: network.shortcode,
    shortName: network.shortname,
    isTestnet: network.isTestnet,
    feeSymbol: network.feeMeta.symbol,
    feeDecimals: network.feeMeta.decimals,
    balance2FeeDecimals: network.balance2FeeDecimals,
    rpcURLs: network.rpcURLs,
    prices: network.priceConfigs,
    explorers: network.explorers,
    extensions: network.extensions,
    presetRpcURLs: urls.length > 0 ? urls : [''],
    clientApi: network.clientApi,
    status: network.status,
  };
};

function initNetworkList() {
  const record: Record<string, PresetNetwork> = {};

  // Need to Concat
  // const serverUpdatedNetworks =
  //   await simpleDb.serverNetworks.getServerNetworks();
  serverPresetNetworks.forEach((s) => {
    try {
      const network = formatServerNetworkToPresetNetwork(s);
      record[network.id] = network;
    } catch (error) {
      debugLogger.common.error(`[initNetworkList] error`, s);
    }
  });
  //   presetNetworks = record;
  return record;
}

function getPresetNetworks(): Record<string, PresetNetwork> {
  const presetNetwork = initNetworkList();
  return presetNetwork;
}

function networkIsPreset(networkId: string): boolean {
  const presetNetwork = initNetworkList();
  return typeof presetNetwork[networkId] !== 'undefined';
}

export { networkIsPreset, getPresetNetworks, initNetworkList };
