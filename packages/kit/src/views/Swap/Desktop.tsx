import { useLayoutEffect, useState } from 'react';

import { useIntl } from 'react-intl';
import { ScrollView } from 'react-native-gesture-handler';

import {
  Box,
  Center,
  IconButton,
  SegmentedControl,
  Select,
  Token,
  Typography,
  // useTheme,
} from '@onekeyhq/components';
import { LayoutHeaderDesktop } from '@onekeyhq/components/src/Layout/Header/LayoutHeaderDesktop';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { useAppSelector, useNavigation } from '../../hooks';
import SwapChart from '../PriceChart/SwapChart';

import { useSwapChartMode } from './hooks/useSwapUtils';
import { Main } from './Main';
import { PendingLimitOrdersProfessionalContent } from './Main/LimitOrder/PendingContent';
import { SwapHeader } from './SwapHeader';
import SwapObserver from './SwapObserver';
import SwapUpdater from './SwapUpdater';

// const DesktopHeader = () => {
//   const intl = useIntl();
//   const swapChartMode = useSwapChartMode();
//   return (
//     <Box
//       h="16"
//       px="8"
//       mt="1"
//       flexDirection="row"
//       alignItems="center"
//       justifyContent="space-between"
//     >
//       <ARTCLogo />
//       <Box>
//         <Select<string>
//           isTriggerPlain
//           footer={null}
//           headerShown={false}
//           defaultValue={swapChartMode}
//           onChange={(value) => {
//             backgroundApiProxy.serviceSwap.setSwapChartMode(value);
//           }}
//           options={[
//             {
//               label: intl.formatMessage({
//                 id: 'title__simple_mode',
//               }),
//               value: 'simple',
//             },
//             {
//               label: intl.formatMessage({
//                 id: 'title__chart_mode',
//               }),
//               value: 'chart',
//             },
//           ]}
//           dropdownProps={{ width: '40' }}
//           dropdownPosition="right"
//           renderTrigger={() => (
//             <IconButton
//               name="EllipsisVerticalOutline"
//               type="plain"
//               size="lg"
//               pointerEvents="none"
//               circle
//               m={-2}
//             />
//           )}
//         />
//       </Box>
//     </Box>
//   );
// };

const DesktopMain = () => (
  <Box>
    <Center>
      <Box maxW={{ md: '480px' }} width="full">
        <Box
          flexDirection="row"
          justifyContent="space-between"
          px="4"
          mb="4"
          zIndex={1}
        >
          <SwapHeader />
        </Box>
        <Box px="4">
          <Main />
        </Box>
      </Box>
    </Center>
    <SwapUpdater />
    <SwapObserver />
  </Box>
);

const DesktopChart = () => {
  const inputToken = useAppSelector((s) => s.swap.inputToken);
  const outputToken = useAppSelector((s) => s.swap.outputToken);

  if (!inputToken || !outputToken) {
    return null;
  }

  return (
    <Box>
      <SwapChart fromToken={inputToken} toToken={outputToken} />
    </Box>
  );
};

const DesktopChartLabel = () => {
  const inputToken = useAppSelector((s) => s.swap.inputToken);
  const outputToken = useAppSelector((s) => s.swap.outputToken);
  return (
    <Box flexDirection="row">
      <Box flexDirection="row">
        <Center
          w="9"
          bg="background-default"
          borderRadius="full"
          overflow="hidden"
        >
          <Token token={inputToken} size={8} />
        </Center>
        <Center
          w="9"
          ml="-4"
          bg="background-default"
          borderRadius="full"
          overflow="hidden"
        >
          <Token token={outputToken} size={8} />
        </Center>
      </Box>
      <Typography.Heading ml="2" color="text-default" fontSize={18}>
        {inputToken?.symbol.toUpperCase()} / {outputToken?.symbol.toUpperCase()}
      </Typography.Heading>
    </Box>
  );
};

export const Desktop = () => {
  const navigation = useNavigation();
  const mode = useSwapChartMode();
  const intl = useIntl();
  const [selectIndex, setSelectedIndex] = useState(1);
  useLayoutEffect(() => {
    navigation.setOptions({ headerShown: false });
  }, [navigation]);
  const options = [
    {
      label: intl.formatMessage({
        id: 'title__simple_mode',
      }),
      value: 'simple',
      index: 0,
    },
    {
      label: intl.formatMessage({
        id: 'title__chart_mode',
      }),
      value: 'chart',
      index: 1,
    },
  ];

  return (
    <ScrollView>
      {/* <DesktopHeader /> */}
      <LayoutHeaderDesktop />
      <Box mt="6">
        <Box
          flexDirection="row"
          alignItems="flex-end"
          justifyContent="flex-end"
          px={mode === 'chart' ? '8' : undefined}
          overflow="hidden"
          mx={3}
          py={2}
        >
          <SegmentedControl
            selectedIndex={selectIndex}
            onChange={(e) => {
              setSelectedIndex(e);
              backgroundApiProxy.serviceSwap.setSwapChartMode(
                options.find((option) => option.index === e)?.value ??
                  options[0].value,
              );
            }}
            values={[
              intl.formatMessage({
                id: 'title__simple_mode',
              }),
              intl.formatMessage({
                id: 'title__chart_mode',
              }),
            ]}
            tabStyle={{
              width: '120px',
            }}
          />
        </Box>
        <Box
          flexDirection="row"
          alignItems="flex-start"
          justifyContent="center"
          px={mode === 'chart' ? '8' : undefined}
          overflow="hidden"
        >
          {mode === 'chart' ? (
            <Box flex="1">
              <Box minH="52px">
                <DesktopChartLabel />
              </Box>
              <DesktopChart />
              <PendingLimitOrdersProfessionalContent />
            </Box>
          ) : null}
          <Box w="480px" bg="background-default">
            <DesktopMain />
          </Box>
        </Box>
      </Box>
    </ScrollView>
  );
};
