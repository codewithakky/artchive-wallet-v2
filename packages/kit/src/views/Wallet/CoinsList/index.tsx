import { useCallback, useEffect, useMemo } from 'react';

// eslint-disable-next-line
import { useFocusEffect, useNavigation } from '@react-navigation/core';
import { omit } from 'lodash';
import { useDebounce } from 'use-debounce';

import {
  // Box,
  Divider,
  FlatList,
  useIsVerticalLayout,
  useUserDevice,
} from '@onekeyhq/components';
import { Tabs } from '@onekeyhq/components/src/CollapsibleTabView';
import type { FlatListProps } from '@onekeyhq/components/src/FlatList';
// eslint-disable-next-line
import { type AggToken, type Token } from '@onekeyhq/engine/src/types/token';
// import {
//   type EVMDecodedItem,
//   EVMDecodedTxType,
// } from '@onekeyhq/engine/src/vaults/impl/evm/decoder/types';
import { MAX_PAGE_CONTAINER_WIDTH } from '@onekeyhq/shared/src/config/appConfig';
import platformEnv from '@onekeyhq/shared/src/platformEnv';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { useActiveSideAccount } from '../../../hooks';
import {
  useAccountCoins,
  useAccountTokenLoading,
  useAccountTokens,
} from '../../../hooks/useTokens';
// import { HomeRoutes } from '../../../routes/routesEnum';
import AssetsListHeader from '../AssetsList/AssetsListHeader';
import { EmptyListOfAccount } from '../AssetsList/EmptyList';
import AssetsListSkeleton from '../AssetsList/Skeleton';
import TokenCell from '../AssetsList/TokenCell';
import { useCryptoMode } from '../AssetsList/useCryptoMode';

import type { SimplifiedToken } from '../../../store/reducers/tokens';
// import type { NavigationProps } from '../AssetsList';

export type IAssetsListProps = Omit<FlatListProps, 'data' | 'renderItem'> & {
  onTokenPress?:
    | null
    | ((event: { token: SimplifiedToken }) => void)
    | undefined;
  singleton?: boolean;
  hidePriceInfo?: boolean;
  showRoundTop?: boolean;
  limitSize?: number;
  flatStyle?: boolean;
  accountId: string;
  networkId: string;
  renderDefiList?: boolean;
  isLoading?: boolean;
};
function CoinsList({
  showRoundTop,
  singleton,
  hidePriceInfo,
  ListHeaderComponent,
  ListFooterComponent,
  contentContainerStyle,
  // onTokenPress,
  limitSize,
  flatStyle,
  accountId,
  // renderDefiList,
  networkId,
  isLoading,
}: IAssetsListProps) {
  const isVerticalLayout = useIsVerticalLayout();
  const loading = useAccountTokenLoading(networkId, accountId);
  const { cryptoMode, isTokenMode } = useCryptoMode();
  // const navigation = useNavigation<NavigationProps>();
  const { size } = useUserDevice();
  const responsivePadding = () => {
    if (['NORMAL', 'LARGE'].includes(size)) return 32;
    return 16;
  };
  const { account, network } = useActiveSideAccount({
    accountId,
    networkId,
  });
  const consolidatedToken = useAccountCoins();
  const accountTokensWithoutLimit = useAccountTokens(
    networkId,
    accountId,
    true,
  );
  const consolidatedTokenMemo = useMemo(() => {
    switch (cryptoMode) {
      case 'active_network':
        return accountTokensWithoutLimit;
      default:
        return consolidatedToken;
    }
  }, [accountTokensWithoutLimit, consolidatedToken, cryptoMode]);
  // const consolidatedToken = useMemo(
  //   () =>
  //     aggTokensWithPrices.reduce<AggToken[]>((acc, val) => {
  //       const { value, usdValue, name, balance } = val;
  //       const ind = acc.findIndex((el) => el.name === name);
  //       if (ind !== -1) {
  //         acc[ind].balance = Number(acc[ind].balance) + Number(balance);
  //         acc[ind].value = Number(acc[ind].value) + Number(value);
  //         acc[ind].usdValue = Number(acc[ind].usdValue) + Number(usdValue);
  //       } else {
  //         acc.push({
  //           ...val,
  //           value,
  //           usdValue,
  //         });
  //       }
  //       return acc;
  //     }, []),
  //   [aggTokensWithPrices],
  // );

  const [startRefresh] = useDebounce(
    useCallback(() => {
      const { serviceToken, serviceOverview } = backgroundApiProxy;
      serviceOverview.startQueryPendingTasks();
      serviceToken.startRefreshAccountTokens();
    }, []),
    1000,
    {
      leading: true,
      trailing: false,
    },
  );

  const [stopRefresh] = useDebounce(
    useCallback(() => {
      const { serviceToken, serviceOverview } = backgroundApiProxy;
      serviceOverview.stopQueryPendingTasks();
      serviceToken.stopRefreshAccountTokens();
    }, []),
    1000,
    {
      leading: true,
      trailing: false,
    },
  );

  const visibilityStateListener = useCallback(() => {
    if (document.visibilityState === 'hidden') {
      stopRefresh();
    }
    if (document.visibilityState === 'visible') {
      startRefresh();
    }
  }, [startRefresh, stopRefresh]);

  useEffect(() => {
    const { serviceOverview } = backgroundApiProxy;
    serviceOverview.subscribe();

    if (platformEnv.isExtensionUi) {
      chrome.runtime.connect();
    }
  }, [networkId, accountId, startRefresh, stopRefresh]);

  useFocusEffect(
    useCallback(() => {
      const { serviceToken } = backgroundApiProxy;
      if (!account || !network || !isTokenMode) {
        return;
      }
      serviceToken.fetchAccountTokens({
        includeTop50TokensQuery: true,
        networkId: network?.id,
        accountId: account?.id,
      });

      startRefresh();
      if (platformEnv.isRuntimeBrowser) {
        document.addEventListener('visibilitychange', visibilityStateListener);
        if (!platformEnv.isDesktop) {
          window.addEventListener('blur', stopRefresh);
          window.addEventListener('focus', startRefresh);
        }
      }

      return () => {
        stopRefresh();
        if (platformEnv.isRuntimeBrowser) {
          document.removeEventListener(
            'visibilitychange',
            visibilityStateListener,
          );
          if (!platformEnv.isDesktop) {
            window.removeEventListener('blur', stopRefresh);
            window.removeEventListener('focus', startRefresh);
          }
        }
      };
    }, [
      account,
      network,
      visibilityStateListener,
      startRefresh,
      stopRefresh,
      isTokenMode,
    ]),
  );

  // const onTokenCellPress = useCallback(
  //   (item: Token) => {
  //     if (onTokenPress) {
  //       onTokenPress({ token: item });
  //       return;
  //     }
  //     // TODO: make it work with multi chains.
  //     const filter = item.tokenIdOnNetwork
  //       ? undefined
  //       : (i: EVMDecodedItem) => i.txType === EVMDecodedTxType.NATIVE_TRANSFER;

  //     navigation.navigate(HomeRoutes.ScreenTokenDetail, {
  //       accountId: account?.id ?? '',
  //       networkId: networkId ?? '',
  //       tokenId: item.tokenIdOnNetwork ?? '',
  //       sendAddress: item.sendAddress,
  //       historyFilter: filter,
  //     });
  //   },
  //   [account?.id, networkId, navigation, onTokenPress],
  // );

  const renderListItem: FlatListProps<AggToken>['renderItem'] = ({
    item,
    index,
  }) => (
    <TokenCell
      accountId={item?.accountId ?? account?.id ?? ''}
      hidePriceInfo={hidePriceInfo}
      bg={flatStyle ? 'transparent' : 'surface-default'}
      borderTopRadius={!flatStyle && showRoundTop && index === 0 ? '12px' : 0}
      borderRadius={
        // eslint-disable-next-line no-unsafe-optional-chaining
        !flatStyle && index === consolidatedTokenMemo?.length - 1
          ? '12px'
          : '0px'
      }
      borderTopWidth={!flatStyle && showRoundTop && index === 0 ? 1 : 0}
      // eslint-disable-next-line no-unsafe-optional-chaining
      borderBottomWidth={index === consolidatedTokenMemo?.length - 1 ? 1 : 0}
      borderColor={flatStyle ? 'transparent' : 'border-subdued'}
      // onPress={onTokenCellPress}
      {...omit(item, 'source')}
      networkId={item?.networkId ?? networkId ?? ''}
    />
  );

  const Container = singleton ? FlatList : Tabs.FlatList;

  return (
    <Container
      style={{
        maxWidth: MAX_PAGE_CONTAINER_WIDTH,
        width: '100%',
        marginHorizontal: 'auto',
        alignSelf: 'center',
      }}
      contentContainerStyle={[
        {
          paddingHorizontal: flatStyle ? 0 : responsivePadding(),
          marginTop: 24,
        },
        contentContainerStyle,
      ]}
      data={!(isLoading || loading) ? consolidatedTokenMemo : null}
      renderItem={renderListItem}
      ListHeaderComponent={
        loading || isLoading
          ? null
          : ListHeaderComponent ?? (
              <AssetsListHeader
                innerHeaderBorderColor={
                  flatStyle ? 'transparent' : 'border-subdued'
                }
                showTokenCount={limitSize !== undefined}
                showOuterHeader={limitSize !== undefined}
                showInnerHeader={consolidatedTokenMemo.length > 0}
                showInnerHeaderRoundTop={!flatStyle}
              />
            )
      }
      ItemSeparatorComponent={Divider}
      ListEmptyComponent={
        loading || isLoading
          ? AssetsListSkeleton
          : // eslint-disable-next-line react/no-unstable-nested-components
            () => <EmptyListOfAccount network={network} accountId={accountId} />
      }
      ListFooterComponent={ListFooterComponent}
      keyExtractor={(_item: SimplifiedToken & AggToken, index) =>
        `${_item.name}--${_item.accountId ?? ''}--${index}`
      }
      extraData={isVerticalLayout}
      showsVerticalScrollIndicator={false}
    />
  );
}

export default CoinsList;
