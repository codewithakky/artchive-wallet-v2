/* eslint-disable no-nested-ternary */
import type { FC } from 'react';
import { useCallback, useMemo, useState } from 'react';

import { useIntl } from 'react-intl';

import {
  Box,
  Divider,
  HStack,
  Icon,
  IconButton,
  Pressable,
  Spinner,
  Text,
  Typography,
  useIsVerticalLayout,
} from '@onekeyhq/components';
import { WALLET_TYPE_WATCHING } from '@onekeyhq/engine/src/types/wallet';
import {
  HomeRoutes,
  ModalRoutes,
  RootRoutes,
} from '@onekeyhq/kit/src/routes/routesEnum';
import type {
  HomeRoutesParams,
  RootRoutesParams,
} from '@onekeyhq/kit/src/routes/types';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { FormatCurrencyNumber } from '../../../components/Format';
import {
  useAccountTokens,
  useAccountValues,
  useAllAccountCoinsValues,
  useNavigation,
} from '../../../hooks';
import { useActiveWalletAccount } from '../../../hooks/redux';
import {
  useAccountCoins,
  useAccountCoinsValues,
  useAccountTokenValues,
} from '../../../hooks/useTokens';
import { ManageTokenModalRoutes } from '../../../routes/routesEnum';
import { showHomeBalanceSettings } from '../../Overlay/HomeBalanceSettings';
import { OverviewBadge } from '../../Overview/components/OverviewBadge';

import { useCryptoMode } from './useCryptoMode';

import type { NativeStackNavigationProp } from '@react-navigation/native-stack';

type NavigationProps = NativeStackNavigationProp<
  RootRoutesParams,
  RootRoutes.Main
> &
  NativeStackNavigationProp<HomeRoutesParams, HomeRoutes.FullTokenListScreen>;

const ListHeader: FC<{
  showTokenCount?: boolean;
  showRoundTop?: boolean;
  borderColor?: string;
  tokenEnabled?: boolean;
}> = ({
  showTokenCount,
  showRoundTop,
  borderColor = 'border-subdued',
  tokenEnabled,
}) => {
  const intl = useIntl();
  const navigation = useNavigation<NavigationProps>();
  const isVerticalLayout = useIsVerticalLayout();
  const { account, network, networkId } = useActiveWalletAccount();
  const { isTokenMode } = useCryptoMode();
  const iconOuterWidth = isVerticalLayout ? '24px' : '32px';
  const iconInnerWidth = isVerticalLayout ? 12 : 16;
  const iconBorderRadius = isVerticalLayout ? '12px' : '16px';
  const testColor = !isTokenMode
    ? 'text-default'
    : network?.isTestnet
    ? 'text-critical'
    : 'text-default';

  // const accountTokens = useAccountTokens(networkId, account?.id, true);
  // const consolidatedToken = useAccountCoins();

  const tokenCountOrAddToken = useMemo(
    () =>
      tokenEnabled && (
        <IconButton
          size="sm"
          borderRadius={17}
          name="PlusMini"
          bg="action-secondary-default"
          onPress={() =>
            navigation.navigate(RootRoutes.Modal, {
              screen: ModalRoutes.ManageToken,
              params: { screen: ManageTokenModalRoutes.Listing },
            })
          }
        />
      ),
    // showTokenCount ? (
    //   <>
    //     <Text
    //       color="text-subdued"
    //       typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
    //     >
    //       {!isTokenMode
    //         ? consolidatedToken.length
    //         : network?.isTestnet
    //         ? 1
    //         : accountTokens.length}
    //     </Text>
    //     <Icon name="ChevronRightMini" color="icon-subdued" />
    //   </>
    // ) : (
    //   tokenEnabled && (
    //     <IconButton
    //       size="sm"
    //       borderRadius={17}
    //       name="PlusMini"
    //       bg="action-secondary-default"
    //       onPress={() =>
    //         navigation.navigate(RootRoutes.Modal, {
    //           screen: ModalRoutes.ManageToken,
    //           params: { screen: ManageTokenModalRoutes.Listing },
    //         })
    //       }
    //     />
    //   )
    //  ),
    [tokenEnabled, navigation],
  );
  const Container = showTokenCount ? Pressable.Item : Box;

  const accountCoinsValue = useAccountCoinsValues().value;
  const accountCoinAllValue = useAllAccountCoinsValues().value;

  const coinRate = useMemo(
    () => accountCoinsValue.div(accountCoinAllValue).multipliedBy(100),
    [accountCoinAllValue, accountCoinsValue],
  );

  const accountTokensValue = useAccountTokenValues(
    networkId,
    account?.id ?? '',
    true,
  ).value;

  const accountAllValue = useAccountValues({
    networkId,
    accountId: account?.id ?? '',
  }).value;

  const rate = useMemo(
    () => accountTokensValue.div(accountAllValue).multipliedBy(100),
    [accountAllValue, accountTokensValue],
  );

  return (
    <Container
      p={4}
      shadow={undefined}
      borderTopRadius={showRoundTop ? '12px' : 0}
      borderTopWidth={showRoundTop ? 1 : 0}
      borderWidth={1}
      borderBottomWidth={0}
      borderColor={borderColor}
      onPress={
        showTokenCount
          ? () => {
              navigation.navigate(HomeRoutes.FullTokenListScreen, {
                accountId: account?.id,
                networkId: network?.id,
              });
            }
          : undefined
      }
      flexDirection="column"
      bg="surface-subdued"
    >
      <Box
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
      >
        <Box
          w={iconOuterWidth}
          h={iconOuterWidth}
          borderRadius={iconBorderRadius}
          bg="decorative-icon-one"
          justifyContent="center"
          alignItems="center"
          mr={isVerticalLayout ? '8px' : '12px'}
        >
          <Icon
            size={iconInnerWidth}
            color="icon-on-primary"
            // color="action-primary-default"
            name="DatabaseOutline"
          />
        </Box>
        <Text
          color={testColor}
          typography={{ sm: 'Body1Strong', md: 'Heading' }}
        >
          {/* {!toke
            ? 'Coins Value'
            : intl.formatMessage({
                id: 'asset__tokens',
              })} */}
          Cryptos Value
        </Text>
        {!isVerticalLayout && (
          <Box flexDirection="row" alignItems="center">
            <Box
              mx="8px"
              my="auto"
              w="4px"
              h="4px"
              borderRadius="2px"
              bg="text-default"
            />
            <Text
              color={testColor}
              typography={{ sm: 'DisplayLarge', md: 'Heading' }}
            >
              {!isTokenMode ? (
                accountCoinsValue.isNaN() ? (
                  ' '
                ) : (
                  <FormatCurrencyNumber
                    decimals={2}
                    value={accountCoinsValue}
                  />
                )
              ) : accountTokensValue.isNaN() ? (
                ' '
              ) : (
                <FormatCurrencyNumber decimals={2} value={accountTokensValue} />
              )}
            </Text>
          </Box>
        )}
        {!isTokenMode ? (
          coinRate.isNaN() ? null : (
            <OverviewBadge rate={coinRate} />
          )
        ) : rate.isNaN() ? null : (
          <OverviewBadge rate={rate} />
        )}
        <Box ml="auto" flexDirection="row" alignItems="center">
          {tokenCountOrAddToken}
        </Box>
      </Box>
      <Box mt={isVerticalLayout ? '8px' : '16px'}>
        {isVerticalLayout ? (
          <Text typography={{ sm: 'DisplayLarge', md: 'Heading' }}>
            {!isTokenMode ? (
              accountCoinsValue.isNaN() ? (
                ' '
              ) : (
                <FormatCurrencyNumber decimals={2} value={accountCoinsValue} />
              )
            ) : accountTokensValue.isNaN() ? (
              ' '
            ) : (
              <FormatCurrencyNumber decimals={2} value={accountTokensValue} />
            )}
          </Text>
        ) : (
          <Box flexDirection="row" w="full">
            <Typography.Subheading color="text-subdued" flex={1}>
              {!isTokenMode
                ? 'Cryptos'
                : intl.formatMessage({ id: 'title__assets' })}
            </Typography.Subheading>
            {!isTokenMode && (
              <Typography.Subheading
                ml={!isTokenMode ? '' : '44px'}
                color="text-subdued"
                flex={1}
                textAlign="right"
                pr="6"
              >
                Wallet | Acc. Address
              </Typography.Subheading>
            )}
            <Typography.Subheading
              ml={!isTokenMode ? '' : '44px'}
              color="text-subdued"
              flex={1}
              textAlign="right"
              pr="6"
            >
              {intl.formatMessage({ id: 'content__price_uppercase' })}
            </Typography.Subheading>
            <Typography.Subheading
              color="text-subdued"
              flex={1}
              textAlign="right"
            >
              {intl.formatMessage({ id: 'form__value' })}
            </Typography.Subheading>
          </Box>
        )}
      </Box>
    </Container>
  );
};

const AssetsListHeader: FC<{
  showInnerHeader?: boolean;
  showOuterHeader?: boolean;
  showTokenCount?: boolean;
  showInnerHeaderRoundTop?: boolean;
  innerHeaderBorderColor?: string;
}> = ({
  showInnerHeader,
  showTokenCount,
  showOuterHeader,
  showInnerHeaderRoundTop,
  innerHeaderBorderColor,
}) => {
  // const intl = useIntl();
  const isVertical = useIsVerticalLayout();
  const [refreshing, setRefreshing] = useState(false);
  // const navigation = useNavigation<NavigationProps>();
  const { network, wallet, accountId } = useActiveWalletAccount();
  const { isTokenMode } = useCryptoMode();
  const { tokenEnabled: networkTokenEnabled, activateTokenRequired } =
    network?.settings ?? { tokenEnabled: false, activateTokenRequired: false };

  const tokenEnabled = useMemo(() => {
    if (wallet?.type === WALLET_TYPE_WATCHING && activateTokenRequired) {
      return false;
    }
    return networkTokenEnabled;
  }, [activateTokenRequired, networkTokenEnabled, wallet?.type]);
  const accountTokens = useAccountTokens(network?.id, accountId, true);
  const consolidatedToken = useAccountCoins();

  const refresh = useCallback(() => {
    setRefreshing(true);
    backgroundApiProxy.serviceOverview.refreshCurrentAccount().finally(() => {
      setTimeout(() => setRefreshing(false), 1000);
    });
  }, []);

  const refreshButton = useMemo(() => {
    if (isVertical) {
      return;
    }
    return (
      <Box alignItems="center" justifyContent="center" w="8" h="8" mr="3">
        {refreshing ? (
          <Spinner size="sm" />
        ) : (
          <IconButton
            onPress={refresh}
            size="sm"
            name="ArrowPathMini"
            type="plain"
            ml="auto"
          />
        )}
      </Box>
    );
  }, [isVertical, refreshing, refresh]);

  return (
    <>
      {showOuterHeader && (
        <Box
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
          pb={3}
        >
          <Typography.Heading>
            {/* {cryptosMode ? 'All Cryptos' : intl.formatMessage({ id: 'title__assets' })} */}
            {`All Cryptos - ${
              !isTokenMode
                ? consolidatedToken.length
                : network?.isTestnet
                ? 1
                : accountTokens.length
            }`}
          </Typography.Heading>
          {tokenEnabled && isTokenMode && (
            <HStack alignItems="center" justifyContent="flex-end">
              {refreshButton}
              {/* <IconButton
                onPress={() => {
                  if (!isTokenMode) {
                    return;
                  }
                  navigation.navigate(RootRoutes.Modal, {
                    screen: ModalRoutes.ManageToken,
                    params: { screen: ManageTokenModalRoutes.Listing },
                  });
                }}
                size="sm"
                name="PlusMini"
                type="plain"
                ml="auto"
                mr={3}
              /> */}
              <IconButton
                onPress={showHomeBalanceSettings}
                size="sm"
                name="Cog8ToothMini"
                type="plain"
                mr={-2}
              />
            </HStack>
          )}
        </Box>
      )}

      {showInnerHeader && (
        <>
          <ListHeader
            borderColor={innerHeaderBorderColor}
            showRoundTop={showInnerHeaderRoundTop}
            showTokenCount={showTokenCount}
            tokenEnabled={tokenEnabled}
          />
          <Divider />
        </>
      )}
    </>
  );
};
AssetsListHeader.displayName = 'AssetsListHeader';

export default AssetsListHeader;
