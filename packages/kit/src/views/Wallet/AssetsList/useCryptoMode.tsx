import { useMemo } from 'react';

import { useAppSelector } from '../../../hooks/redux';

export function useCryptoMode() {
  const cryptoMode = useAppSelector((s) => s.settings.cryptosMode);
  const isTokenMode = useMemo(
    () => cryptoMode === 'active_network',
    [cryptoMode],
  );
  return { cryptoMode, isTokenMode };
}
