export enum WalletHomeTabEnum {
  AllCoins = 'Coins',
  Tokens = 'Tokens',
  Collectibles = 'Collectibles',
  History = 'History',
  Tools = 'Tools',
  Testnets = 'Testnets',
}

export const HomeTabOrder = [
  WalletHomeTabEnum.AllCoins,
  WalletHomeTabEnum.Tokens,
  WalletHomeTabEnum.Collectibles,
  WalletHomeTabEnum.History,
  WalletHomeTabEnum.Tools,
  WalletHomeTabEnum.Testnets,
];

export const HomeTabIndex = {
  [WalletHomeTabEnum.AllCoins]: 0,
  [WalletHomeTabEnum.Tokens]: 1,
  [WalletHomeTabEnum.Collectibles]: 2,
  [WalletHomeTabEnum.History]: 3,
  [WalletHomeTabEnum.Tools]: 4,
  [WalletHomeTabEnum.Testnets]: 5,
};

export enum CryptoModeEnum {
  AllWallets = 'all',
  ActiveWallet = 'active',
  ActiveWalletChain = 'active_network',
}

export const CryptoMode = {
  [CryptoModeEnum.AllWallets]: 'All Wallets',
  [CryptoModeEnum.ActiveWallet]: 'Active Wallet',
  [CryptoModeEnum.ActiveWalletChain]: 'Active Wallet Chain',
};
