import type { FC } from 'react';
import { useCallback, useMemo, useState } from 'react';

import { useNavigation } from '@react-navigation/native';

import { Divider, ToastManager } from '@onekeyhq/components';
import { walletIsHD } from '@onekeyhq/engine/src/managers/wallet';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import {
  useActiveWalletAccount,
  // useAppSelector,
  //   useAppSelector,
  useNavigation as useNavigationAcc,
} from '../../../hooks';
import {
  CreateAccountModalRoutes,
  ManageNetworkModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../routes/routesEnum';
import { setCryptosMode } from '../../../store/reducers/settings';
import { setHomeTabName } from '../../../store/reducers/status';
import { getPresetNetworks, networkIsPreset } from '../../network';
import { showAccountValueSettings } from '../../Overlay/AccountValueSettings';
import BaseMenu from '../../Overlay/BaseMenu';
import useUpdateItem from '../../Overlay/useUpdateItem';
import { useCryptoMode } from '../AssetsList/useCryptoMode';
import { WalletHomeTabEnum } from '../type';

import type { CreateAccountRoutesParams } from '../../../routes';
import type { ModalScreenProps } from '../../../routes/types';
import type { IBaseMenuOptions, IMenu } from '../../Overlay/BaseMenu';

// type NavigationProps = ModalScreenProps<CreateWalletRoutesParams>;
type NavigationPropsAcc = ModalScreenProps<CreateAccountRoutesParams>;

const AccountInfoMoreMenuNew: FC<IMenu> = (props) => {
  const updateItemOptions = useUpdateItem();
  const navigationAcc = useNavigationAcc<NavigationPropsAcc['navigation']>();
  const [, setRefreshing] = useState(false);
  const navigation = useNavigation();
  const { walletId, networkId, accountId } = useActiveWalletAccount();
  const { cryptoMode } = useCryptoMode();

  const selectRpc = useCallback(
    (mode) => {
      const preset = networkIsPreset(networkId);
      if (preset) {
        const presetNetwork = getPresetNetworks()[networkId];
        backgroundApiProxy.serviceNetwork
          .updateNetwork(networkId, {
            rpcURL: presetNetwork.presetRpcURLs[0],
          })
          .then(async () => {
            await backgroundApiProxy.serviceToken.batchFetchAccountBalances({
              walletId,
              accountIds: [accountId],
              networkId,
            });
            backgroundApiProxy.dispatch(setCryptosMode(mode));
            backgroundApiProxy.dispatch(
              setHomeTabName(WalletHomeTabEnum.AllCoins),
            );
          });
      }
    },
    [walletId, networkId, accountId],
  );

  const toViewDetailsPage = useCallback(() => {
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManageNetwork,
      params: {
        screen: ManageNetworkModalRoutes.ViewWalletDetails,
      },
    });
  }, [navigation]);

  const refresh = useCallback(async () => {
    ToastManager.show({ title: 'Refreshing...' });
    await backgroundApiProxy.serviceToken.batchFetchAccountBalances({
      walletId,
      networkId,
      accountIds: [accountId],
    });

    // console.log(balance);
    await backgroundApiProxy.serviceOverview
      .refreshCurrentAccount()
      .finally(() => {
        setTimeout(() => setRefreshing(false), 1000);
      });
  }, [networkId, accountId, walletId]);

  const onPressManageAccount = useCallback(() => {
    setTimeout(() => {
      navigationAcc.navigate(RootRoutes.Modal, {
        screen: ModalRoutes.CreateAccount,
        params: {
          screen: CreateAccountModalRoutes.CreateAccountAuthentication,
          params: {
            walletId: walletId ?? '',
            onDone: (password) => {
              setTimeout(() => {
                navigationAcc.replace(
                  CreateAccountModalRoutes.RecoverAccountsList as any,
                  {
                    walletId,
                    network: networkId,
                    password,
                    purpose: '',
                    template: '',
                  },
                );
              }, 20);
            },
          },
        },
      });
    });
  }, [navigationAcc, walletId, networkId]);

  const onTabIndexChange = useCallback(
    (mode) => {
      selectRpc(mode);
    },
    [selectRpc],
  );

  const options = useMemo(() => {
    const baseOptions: IBaseMenuOptions = [
      {
        type: 'radio',
        title: 'coins__mode_title',
        defaultValue: cryptoMode,
        children: [
          {
            id: 'coins__mode_all',
            value: 'all',
            onPress() {
              onTabIndexChange('all');
            },
            variant: cryptoMode === 'all' ? 'highlight' : 'none',
          },
          {
            id: 'coins__mode_active',
            value: 'active',
            onPress() {
              onTabIndexChange('active');
            },
            variant: cryptoMode === 'active' ? 'highlight' : 'none',
          },
          {
            id: 'coins__mode_active_network',
            value: 'active_network',
            onPress() {
              onTabIndexChange('active_network');
            },
            variant: cryptoMode === 'active_network' ? 'highlight' : 'none',
          },
        ],
      },
      () => <Divider my={3} />,
      {
        id: 'title__settings',
        onPress: () => showAccountValueSettings(),
        icon: 'Cog8ToothMini',
      },
      walletIsHD(walletId) && {
        id: 'action__manage_account',
        onPress: () => onPressManageAccount(),
        icon: 'SquaresPlusMini',
      },
      {
        id: 'action__view_details',
        onPress: () => toViewDetailsPage(),
        icon: 'DocumentOutline',
      },
      {
        id: 'action__refresh',
        onPress: () => refresh(),
        icon: 'ArrowPathMini',
      },
    ];
    return updateItemOptions
      ? baseOptions.concat(updateItemOptions)
      : baseOptions;
  }, [
    updateItemOptions,
    refresh,
    toViewDetailsPage,
    cryptoMode,
    onTabIndexChange,
    onPressManageAccount,
    walletId,
  ]);

  return (
    <BaseMenu
      width="full"
      placement="bottom left"
      options={options}
      {...props}
    />
  );
};

export default AccountInfoMoreMenuNew;
