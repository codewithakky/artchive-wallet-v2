import type { FC } from 'react';
import { useCallback, useEffect, useRef, useState } from 'react';

import { useIntl } from 'react-intl';

import type { ForwardRefHandle } from '@onekeyhq/app/src/views/NestedTabView/NestedTabView';
import {
  Box,
  Center,
  useIsVerticalLayout,
  useUserDevice,
} from '@onekeyhq/components';
import { Tabs } from '@onekeyhq/components/src/CollapsibleTabView';
import {
  getStatus,
  useActiveWalletAccount,
  useStatus,
} from '@onekeyhq/kit/src/hooks/redux';
import { MAX_PAGE_CONTAINER_WIDTH } from '@onekeyhq/shared/src/config/appConfig';
import debugLogger from '@onekeyhq/shared/src/logger/debugLogger';
import platformEnv from '@onekeyhq/shared/src/platformEnv';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { ArtChivePerfTraceLog } from '../../components/ArtChivePerfTraceLog';
import IdentityAssertion from '../../components/IdentityAssertion';
import Protected, { ValidationFields } from '../../components/Protected';
import { useHtmlPreloadSplashLogoRemove } from '../../hooks/useHtmlPreloadSplashLogoRemove';
import { useOnboardingRequired } from '../../hooks/useOnboardingRequired';
import { setCryptosMode } from '../../store/reducers/settings';
import { setHomeTabName } from '../../store/reducers/status';
import { wait } from '../../utils/helper';
import { getPresetNetworks, networkIsPreset } from '../network';
import OfflineView from '../Offline';
import { GuideToPushFirstTimeCheck } from '../PushNotification/GuideToPushFirstTime';
import SolTxHistoryListView from '../TxHistory/SolTxHistoryListView';

import AccountInfo, {
  FIXED_HORIZONTAL_HEDER_HEIGHT,
  FIXED_VERTICAL_HEADER_HEIGHT,
} from './AccountInfo';
import AssetsList from './AssetsList';
import CoinsList from './CoinsList';
import NFTList from './NFT/NFTList';
import ToolsPage from './Tools';
import { HomeTabIndex, HomeTabOrder, WalletHomeTabEnum } from './type';

const AccountHeader = () => <AccountInfo />;
// const AccountHeader = () => null;

// HomeTabs
const WalletTabs: FC = () => {
  const intl = useIntl();
  const ref = useRef<ForwardRefHandle>(null);
  const [loading, setLoading] = useState(false);
  const defaultIndexRef = useRef<number>(
    HomeTabIndex[getStatus().homeTabName as WalletHomeTabEnum] ?? 1,
  );
  const { screenWidth } = useUserDevice();
  const isVerticalLayout = useIsVerticalLayout();
  const { homeTabName } = useStatus();
  const { wallet, account, network, accountId, networkId, walletId } =
    useActiveWalletAccount();
  // const wallets = useAppSelector((s) => s.runtime.wallets);
  // const { enabledNetworks: networks } = useManageNetworks();
  const [refreshing, setRefreshing] = useState(false);
  const enabledTestnetworks = ['sol--101', 'evm--1', 'evm--137', 'evm--56'];
  const selectRpc = useCallback(
    (index: number) => {
      const preset = networkIsPreset(networkId);
      if (preset) {
        setLoading(true);
        const presetNetwork = getPresetNetworks()[networkId];
        backgroundApiProxy.serviceNetwork
          .updateNetwork(networkId, {
            rpcURL: presetNetwork.presetRpcURLs[index === 4 ? 1 : 0],
          })
          .then(async () => {
            await backgroundApiProxy.serviceToken
              .batchFetchAccountBalances({
                walletId,
                accountIds: [accountId],
                networkId,
              })
              ?.then(() => {
                backgroundApiProxy.dispatch(setCryptosMode('active_network'));
                backgroundApiProxy.dispatch(
                  setHomeTabName(HomeTabOrder[index]),
                );
                wait(2000);
                setLoading(false);
              });
          })
          .catch(() => setLoading(false));
      }
    },
    [walletId, networkId, accountId],
  );
  const onIndexChange = useCallback(
    (index: number) => {
      if (index === 4 || index === 0) {
        selectRpc(index);
      } else {
        backgroundApiProxy.dispatch(setCryptosMode('active_network'));
        backgroundApiProxy.dispatch(setHomeTabName(HomeTabOrder[index]));
      }
    },
    [selectRpc],
  );

  useEffect(() => {
    const idx = HomeTabIndex[homeTabName as WalletHomeTabEnum];
    if (typeof idx !== 'number' || idx === defaultIndexRef.current) {
      return;
    }
    debugLogger.common.info(
      `switch wallet tab index, old=${defaultIndexRef.current}, new=${idx}`,
    );
    ref.current?.setPageIndex?.(idx);
    onIndexChange(idx);
    defaultIndexRef.current = idx;
  }, [homeTabName, onIndexChange]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    backgroundApiProxy.serviceOverview.refreshCurrentAccount().finally(() => {
      setTimeout(() => setRefreshing(false), 50);
    });
  }, []);

  const timer = useRef<ReturnType<typeof setTimeout>>();
  const walletTabs = (
    <Tabs.Container
      canOpenDrawer
      initialTabName={homeTabName}
      refreshing={refreshing}
      onRefresh={onRefresh}
      onIndexChange={(index: number) => {
        setLoading(true);
        clearTimeout(timer.current);
        timer.current = setTimeout(() => {
          defaultIndexRef.current = index;
          onIndexChange(index);
        }, 1500);
      }}
      renderHeader={AccountHeader}
      headerHeight={
        isVerticalLayout
          ? FIXED_VERTICAL_HEADER_HEIGHT
          : FIXED_HORIZONTAL_HEDER_HEIGHT
      }
      ref={ref}
      containerStyle={{
        maxWidth: MAX_PAGE_CONTAINER_WIDTH,
        // reduce the width on iPad, sidebar's width is 244
        width: isVerticalLayout ? screenWidth : screenWidth - 224,
        marginHorizontal: 'auto', // Center align vertically
        alignSelf: 'center',
        flex: 1,
      }}
    >
      <Tabs.Tab name={WalletHomeTabEnum.AllCoins} label="Crypto">
        <>
          <CoinsList
            accountId={accountId}
            networkId={networkId}
            ListFooterComponent={<Box h={6} />}
            limitSize={10}
            isLoading={loading}
            renderDefiList
          />
          <ArtChivePerfTraceLog name="App RootTabHome CoinsList render" />
          <GuideToPushFirstTimeCheck />
        </>
      </Tabs.Tab>
      {/* <Tabs.Tab
        name={WalletHomeTabEnum.Tokens}
        label={intl.formatMessage({ id: 'asset__tokens' })}
      >
        <>
          <AssetsList
            accountId={accountId}
            networkId={networkId}
            ListFooterComponent={<Box h={6} />}
            limitSize={10}
            renderDefiList
          />
          <ArtChivePerfTraceLog name="App RootTabHome AssetsList render" />
          <GuideToPushFirstTimeCheck />
        </>
      </Tabs.Tab> */}
      <Tabs.Tab
        name={WalletHomeTabEnum.Collectibles}
        label={intl.formatMessage({ id: 'asset__collectibles' })}
      >
        <NFTList />
      </Tabs.Tab>
      <Tabs.Tab
        name={WalletHomeTabEnum.History}
        label={intl.formatMessage({ id: 'transaction__history' })}
      >
        <SolTxHistoryListView
          address={account?.address ?? ''}
          rpcUrl={network?.rpcURL}
          accountId={account?.id ?? ''}
        />
      </Tabs.Tab>
      <Tabs.Tab
        name={WalletHomeTabEnum.Tools}
        label={intl.formatMessage({ id: 'form__tools' })}
      >
        <ToolsPage />
      </Tabs.Tab>
      {enabledTestnetworks.includes(networkId) ? (
        <Tabs.Tab
          name={WalletHomeTabEnum.Testnets}
          label={WalletHomeTabEnum.Testnets}
        >
          <>
            <AssetsList
              accountId={accountId}
              networkId={networkId}
              ListFooterComponent={<Box h={6} />}
              limitSize={10}
              isLoading={loading}
              renderDefiList
            />
            <ArtChivePerfTraceLog name="App RootTabHome AssetsList render" />
            <GuideToPushFirstTimeCheck />
          </>
        </Tabs.Tab>
      ) : (
        <>{null}</>
      )}
    </Tabs.Container>
  );

  if (!wallet) return null;
  if (network?.settings.validationRequired) {
    return (
      <Center w="full" h="full">
        <Protected
          walletId={wallet.id}
          networkId={network.id}
          field={ValidationFields.Account}
          placeCenter={!platformEnv.isNative}
          subTitle={intl.formatMessage(
            {
              id: 'title__password_verification_is_required_to_view_account_details_on_str',
            },
            { '0': network.name },
          )}
        >
          {() => walletTabs}
        </Protected>
      </Center>
    );
  }
  return walletTabs;
};

export default function Wallet() {
  useOnboardingRequired(true);
  useHtmlPreloadSplashLogoRemove();

  return (
    <>
      <Box flex={1}>
        <IdentityAssertion>
          <WalletTabs />
        </IdentityAssertion>
      </Box>
      <OfflineView />
    </>
  );
}
