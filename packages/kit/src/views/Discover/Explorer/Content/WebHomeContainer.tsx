import type { FC } from 'react';

import { openUrlByWebview } from '../../../../utils/openUrl';
import DiscoverHome from '../../Home';
import { openMatchDApp } from '../Controller/gotoSite';

const WebHomeContainer: FC<{ alwaysOpenNewWindow?: boolean }> = ({
  alwaysOpenNewWindow,
}) => (
  <DiscoverHome
    onItemSelect={(dapp) => {
      openUrlByWebview(dapp.url, dapp.name, { modalMode: true });
      // openMatchDApp({
      //   id: dapp._id,
      //   dapp,
      //   isNewWindow: alwaysOpenNewWindow,
      // });
    }}
    onItemSelectHistory={(item) =>
      openMatchDApp({ ...item, isNewWindow: alwaysOpenNewWindow })
    }
  />
);

WebHomeContainer.displayName = 'WebHomeContainer';
export default WebHomeContainer;
