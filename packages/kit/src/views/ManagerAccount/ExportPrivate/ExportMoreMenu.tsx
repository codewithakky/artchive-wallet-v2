import type { FC } from 'react';
import { useCallback, useMemo } from 'react';

import { useNavigation } from '@react-navigation/core';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { useActiveWalletAccount } from '../../../hooks';
import {
  BackupWalletModalRoutes,
  ManagerAccountModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../routes/routesEnum';
import BaseMenu from '../../Overlay/BaseMenu';
import useUpdateItem from '../../Overlay/useUpdateItem';

import type { IBaseMenuOptions, IMenu } from '../../Overlay/BaseMenu';

const ExportMoreMenu: FC<IMenu> = (props) => {
  const updateItemOptions = useUpdateItem();
  const { walletId, accountId, networkId } = useActiveWalletAccount();
  const navigation = useNavigation();

  const onExportMnemonic = useCallback(async () => {
    const data = await backgroundApiProxy.servicePassword.getPassword();
    const mnemonic = await backgroundApiProxy.engine.revealHDWalletMnemonic(
      walletId,
      data ?? '',
    );
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.BackupWallet,
      params: {
        screen: BackupWalletModalRoutes.BackupWalletMnemonicModal,
        params: {
          mnemonic,
          walletId,
        },
      },
    });
  }, [navigation, walletId]);

  const onExportPublicKey = useCallback(() => {
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManagerAccount,
      params: {
        screen: ManagerAccountModalRoutes.ManagerAccountExportPublicModal,
        params: {
          walletId,
          accountId,
          networkId,
        },
      },
    });
  }, [accountId, navigation, networkId, walletId]);

  const options = useMemo(() => {
    const baseOptions: IBaseMenuOptions = [
      walletId.includes('hd') && {
        id: 'action__export_secret_mnemonic',
        onPress: () => onExportMnemonic(),
        icon: 'KeyOutline',
      },
      {
        id: 'action__export_public_key',
        onPress: () => onExportPublicKey(),
        icon: 'KeyOutline',
      },
    ];
    return updateItemOptions
      ? baseOptions.concat(updateItemOptions)
      : baseOptions;
  }, [updateItemOptions, onExportMnemonic, onExportPublicKey, walletId]);

  return <BaseMenu options={options} {...props} />;
};

export default ExportMoreMenu;
