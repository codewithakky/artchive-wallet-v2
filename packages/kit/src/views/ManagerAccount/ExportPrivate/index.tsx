import type { FC } from 'react';
import { useEffect, useState } from 'react';

import { useRoute } from '@react-navigation/core';

import { IconButton, Modal } from '@onekeyhq/components';
import type {
  AccountCredential,
  Account as AccountEngineType,
} from '@onekeyhq/engine/src/types/account';
// import Protected, {
//   ValidationFields,
// } from '@onekeyhq/kit/src/components/Protected';
import type { ManagerAccountRoutesParams } from '@onekeyhq/kit/src/routes/Root/Modal/ManagerAccount';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { type ManagerAccountModalRoutes } from '../../../routes/routesEnum';

import ExportMoreMenu from './ExportMoreMenu';
import { PrivateOrPublicKeyPreview } from './previewView';

import type { RouteProp } from '@react-navigation/core';

type ExportPrivateViewProps = {
  accountId: string;
  networkId: string;
  password: string;
  credentialType?: AccountCredential;
  onAccountChange: (account: AccountEngineType) => void;
};

const ExportPrivateView: FC<ExportPrivateViewProps> = ({
  accountId,
  networkId,
  password,
  credentialType,
  onAccountChange,
}) => {
  const { engine } = backgroundApiProxy;

  const [privateKey, setPrivateKey] = useState<string>();

  useEffect(() => {
    if (!accountId || !networkId || !password) return;

    engine.getAccount(accountId, networkId).then(($account) => {
      onAccountChange($account);
    });

    engine
      .getAccountPrivateKey({
        accountId,
        credentialType,
        password,
      })
      .then(($privateKey) => {
        setPrivateKey($privateKey);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountId, engine, networkId, password]);

  return (
    <PrivateOrPublicKeyPreview
      privateOrPublicKey={privateKey}
      qrCodeContainerSize={{ base: 296, md: 208 }}
    />
  );
};

export type NavigationProps = RouteProp<
  ManagerAccountRoutesParams,
  ManagerAccountModalRoutes.ManagerAccountExportPrivateModal
>;

const ExportPrivateViewModal = () => {
  const route = useRoute<NavigationProps>();
  const { accountId, networkId, password, accountCredential } = route.params;
  const [account, setAccount] = useState<AccountEngineType>();

  return (
    <Modal
      footer={null}
      header="Private Key"
      headerDescription={account?.name}
      height="auto"
      rightContent={
        <ExportMoreMenu>
          <IconButton
            type="plain"
            size="lg"
            circle
            name="EllipsisVerticalOutline"
          />
        </ExportMoreMenu>
      }
    >
      <ExportPrivateView
        accountId={accountId}
        networkId={networkId}
        credentialType={accountCredential}
        password={password}
        onAccountChange={(acc) => setAccount(acc)}
      />
    </Modal>
  );
};
export default ExportPrivateViewModal;
