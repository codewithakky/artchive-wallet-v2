import type { FC } from 'react';
import { useCallback, useMemo } from 'react';

import { useNavigation, useRoute } from '@react-navigation/core';

import type { AccountCredentialType } from '@onekeyhq/engine/src/types/account';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { useActiveWalletAccount } from '../../../hooks';
import {
  BackupWalletModalRoutes,
  ManagerAccountModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../routes/routesEnum';
import BaseMenu from '../../Overlay/BaseMenu';
import useUpdateItem from '../../Overlay/useUpdateItem';

import type { NavigationProps } from '.';
import type { IBaseMenuOptions, IMenu } from '../../Overlay/BaseMenu';

const ExportPublicMoreMenu: FC<IMenu> = (props) => {
  const updateItemOptions = useUpdateItem();
  const route = useRoute<NavigationProps>();
  const { accountCredential } = route.params;
  console.log(accountCredential);
  const { walletId, accountId, networkId } = useActiveWalletAccount();
  const navigation = useNavigation();

  const onExportMnemonic = useCallback(async () => {
    const data = await backgroundApiProxy.servicePassword.getPassword();
    const mnemonic = await backgroundApiProxy.engine.revealHDWalletMnemonic(
      walletId,
      data ?? '',
    );
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.BackupWallet,
      params: {
        screen: BackupWalletModalRoutes.BackupWalletMnemonicModal,
        params: {
          mnemonic,
          walletId,
        },
      },
    });
  }, [navigation, walletId]);

  const onExportPrivateKey = useCallback(async () => {
    const data = await backgroundApiProxy.servicePassword.getPassword();
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManagerAccount,
      params: {
        screen: ManagerAccountModalRoutes.ManagerAccountExportPrivateModal,
        params: {
          accountId,
          networkId,
          password: data ?? '',
          accountCredential: {
            type: 'PrivateKey' as AccountCredentialType,
            key: 'action__export_private_key',
          },
        },
      },
    });
  }, [accountId, navigation, networkId]);

  const options = useMemo(() => {
    const baseOptions: IBaseMenuOptions = [
      walletId.includes('hd') && {
        id: 'action__export_secret_mnemonic',
        onPress: () => onExportMnemonic(),
        icon: 'KeyOutline',
      },
      {
        id: 'action__export_private_key',
        onPress: () => onExportPrivateKey(),
        icon: 'KeyOutline',
      },
    ];
    return updateItemOptions
      ? baseOptions.concat(updateItemOptions)
      : baseOptions;
  }, [updateItemOptions, onExportMnemonic, onExportPrivateKey, walletId]);

  return <BaseMenu options={options} {...props} />;
};

export default ExportPublicMoreMenu;
