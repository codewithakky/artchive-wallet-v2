import type { FC } from 'react';
import { useCallback, useMemo, useState } from 'react';

import { useIntl } from 'react-intl';

import {
  Dialog,
  Divider,
  Pressable,
  Text,
  ToastManager,
} from '@onekeyhq/components';
import platformEnv from '@onekeyhq/shared/src/platformEnv';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { useAppSelector, useNavigation } from '../../hooks';
import { RootRoutes } from '../../routes/routesEnum';
import {
  THEME_PRELOAD_STORAGE_KEY,
  setTheme,
} from '../../store/reducers/settings';
import { gotoScanQrcode } from '../../utils/gotoScanQrcode';
import { showDialog, showOverlay } from '../../utils/overlayUtils';
import { APP_VERSION } from '../constants';
import { ResetDialog } from '../Me/SecuritySection/ResetButton';

import BaseMenu from './BaseMenu';
// import { showSplashScreen } from './showSplashScreen';
import useUpdateItem from './useUpdateItem';

import type { CreateWalletRoutesParams } from '../../routes';
import type { ModalScreenProps } from '../../routes/types';
import type { IBaseMenuOptions, IMenu } from './BaseMenu';

type NavigationProps = ModalScreenProps<CreateWalletRoutesParams>;

const HomeMoreMenu: FC<IMenu> = (props) => {
  const isPasswordSet = useAppSelector((s) => s.data.isPasswordSet);
  const [pTheme, setPTheme] = useState(
    localStorage.getItem(THEME_PRELOAD_STORAGE_KEY),
  );
  const updateItemOptions = useUpdateItem();
  const [, setRefreshing] = useState(false);
  const { dispatch } = backgroundApiProxy;
  const navigation = useNavigation<NavigationProps['navigation']>();
  const intl = useIntl();
  const onPressHome = useCallback(() => {
    navigation.navigate(RootRoutes.Onboarding);
  }, [navigation]);

  const openResetHintDialog = useCallback(() => {
    showDialog(
      <ResetDialog
        onConfirm={() => {
          // showSplashScreen();
          backgroundApiProxy.serviceApp.resetApp();
        }}
      />,
    );
  }, []);

  const openBackupModal = useCallback(() => {
    showOverlay((onClose) => (
      <Dialog
        visible
        onClose={onClose}
        footerButtonProps={{
          hideSecondaryAction: true,
          onPrimaryActionPress: () => {
            onClose();
            setTimeout(openResetHintDialog, 500);
          },
          primaryActionTranslationId: 'action__i_got_it',
          primaryActionProps: { type: 'primary' },
        }}
        contentProps={{
          iconType: 'info',
          title: intl.formatMessage({
            id: 'modal__back_up_your_wallet',
            defaultMessage: 'Back Up Your Wallet',
          }),
          content: intl.formatMessage({
            id: 'modal__back_up_your_wallet_desc',
            defaultMessage:
              "Before resetting the App, make sure you've backed up all of your wallets.",
          }),
        }}
      />
    ));
  }, [intl, openResetHintDialog]);
  const onOpenResetModal = useCallback(async () => {
    const wallets = await backgroundApiProxy.engine.getWallets();
    const isBackup = wallets.filter((wallet) => !wallet.backuped).length === 0;
    if (isBackup) {
      // setShowResetModal(true);
      openResetHintDialog();
    } else {
      openBackupModal();
    }
  }, [openBackupModal, openResetHintDialog]);

  const refresh = useCallback(() => {
    setRefreshing(true);
    ToastManager.show({ title: 'Refreshing...' });
    backgroundApiProxy.serviceOverview.refreshCurrentAccount().finally(() => {
      setTimeout(() => setRefreshing(false), 1000);
    });
  }, []);

  const changeTheme = useCallback(() => {
    const key = THEME_PRELOAD_STORAGE_KEY;
    const localTheme = localStorage.getItem(key);
    if (localTheme === 'light') {
      setPTheme('dark');
      dispatch(setTheme('dark'));
    } else {
      setPTheme('light');
      dispatch(setTheme('light'));
    }
  }, [dispatch]);

  const options = useMemo(() => {
    const baseOptions: IBaseMenuOptions = [
      {
        id: 'action__back',
        onPress: () => onPressHome(),
        icon: 'HomeSolid',
      },
      {
        id: 'action__scan',
        onPress: () => gotoScanQrcode(),
        icon: 'ViewfinderCircleMini',
      },
      platformEnv.isExtensionUiPopup && {
        id: 'form__expand_view',
        onPress: () => {
          backgroundApiProxy.serviceApp.openExtensionExpandTab({
            routes: '',
          });
        },
        icon: 'ArrowsPointingOutOutline',
      },
      isPasswordSet && {
        id: 'action__lock_now',
        onPress: () => backgroundApiProxy.serviceApp.lock(true),
        icon: 'LockClosedMini',
      },
      isPasswordSet && {
        id: 'action__refresh',
        onPress: () => refresh(),
        icon: 'ArrowPathMini',
      },
      platformEnv.isExtension &&
        platformEnv.isNative && {
          id: 'form__theme',
          onPress: () => changeTheme(),
          icon: pTheme === 'dark' ? 'SunOutline' : 'MoonOutline',
        },
      () => (
        <Pressable
          mx="4"
          py="2"
          flexDirection="row"
          justifyContent="space-between"
          disabled
        >
          <Text>Version</Text>
          <Text>v {APP_VERSION}</Text>
        </Pressable>
      ),
      () => <Divider my={1} />,
      isPasswordSet && {
        id: 'form__reset_app',
        onPress: () => onOpenResetModal(),
        icon: 'RestoreOutline',
        variant: 'desctructive',
      },
    ];
    return updateItemOptions
      ? baseOptions.concat(updateItemOptions)
      : baseOptions;
  }, [
    isPasswordSet,
    updateItemOptions,
    onOpenResetModal,
    refresh,
    changeTheme,
    pTheme,
    onPressHome,
  ]);

  return <BaseMenu options={options} {...props} />;
};

export default HomeMoreMenu;
