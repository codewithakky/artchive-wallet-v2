import type { FC } from 'react';
import { useCallback, useMemo } from 'react';

import { useNavigation } from '@react-navigation/core';

import { Divider } from '@onekeyhq/components';
import { walletIsHD } from '@onekeyhq/engine/src/managers/wallet';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { useCreateAccountInWallet } from '../../components/NetworkAccountSelector/hooks/useCreateAccountInWallet';
import {
  useActiveWalletAccount,
  useNavigation as useNavigationAcc,
} from '../../hooks';
import {
  CreateAccountModalRoutes,
  ManageNetworkModalRoutes,
  ManagerAccountModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../routes/routesEnum';
import { refreshAccountSelector } from '../../store/reducers/refresher';
import useRemoveAccountDialog from '../ManagerAccount/RemoveAccount';

import BaseMenu from './BaseMenu';
import useUpdateItem from './useUpdateItem';

import type { CreateAccountRoutesParams } from '../../routes';
import type { ModalScreenProps, RootRoutesParams } from '../../routes/types';
import type { IBaseMenuOptions, IMenu } from './BaseMenu';
import type { NativeStackNavigationProp } from '@react-navigation/native-stack';

type NavigationProps = NativeStackNavigationProp<
  RootRoutesParams,
  RootRoutes.Main
>;
type NavigationPropsAcc = ModalScreenProps<CreateAccountRoutesParams>;

const DesktopSidebarMoreMenu: FC<IMenu> = (props) => {
  const updateItemOptions = useUpdateItem();
  const navigationNode = useNavigation<NavigationProps>();
  const navigationAcc = useNavigationAcc<NavigationPropsAcc['navigation']>();
  const { networkId, walletId, accountId, wallet } = useActiveWalletAccount();
  const { dispatch } = backgroundApiProxy;
  // const { refreshAccounts } = useAccountSelectorInfo();
  // TODO refreshAccounts
  const refreshAccounts = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (...args: any[]) => {
      dispatch(refreshAccountSelector());
    },
    [dispatch],
  );
  const { goToRemoveAccount, RemoveAccountDialog } = useRemoveAccountDialog();
  const { createAccount } = useCreateAccountInWallet({
    networkId,
    walletId,
    isFromAccountSelector: true,
  });

  const toCheckNodePage = useCallback(() => {
    navigationNode.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManageNetwork,
      params: {
        screen: ManageNetworkModalRoutes.RPCNode,
        params: { networkId: networkId || '' },
      },
    });
  }, [navigationNode, networkId]);

  const toExportPage = useCallback(() => {
    navigationNode.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManagerAccount,
      params: {
        screen: ManagerAccountModalRoutes.ManagerAccountModal,
        params: {
          walletId: walletId ?? '',
          accountId,
          networkId: networkId ?? '',
          refreshAccounts: () => refreshAccounts(),
        },
      },
    });
  }, [navigationNode, walletId, accountId, networkId, refreshAccounts]);

  const toViewDetailsPage = useCallback(() => {
    navigationNode.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManageNetwork,
      params: {
        screen: ManageNetworkModalRoutes.ViewWalletDetails,
      },
    });
  }, [navigationNode]);

  const onPressRemoveAccount = useCallback(() => {
    goToRemoveAccount({
      wallet,
      accountId,
      networkId: networkId ?? '',
      callback: () => refreshAccounts(wallet?.id ?? '', networkId ?? ''),
    });
  }, [accountId, goToRemoveAccount, wallet, networkId, refreshAccounts]);

  const onPressCreateAccount = useCallback(async () => {
    if (!walletId) return;
    await createAccount();
  }, [walletId, createAccount]);

  const onPressManageAccount = useCallback(() => {
    setTimeout(() => {
      navigationAcc.navigate(RootRoutes.Modal, {
        screen: ModalRoutes.CreateAccount,
        params: {
          screen: CreateAccountModalRoutes.CreateAccountAuthentication,
          params: {
            walletId: walletId ?? '',
            onDone: (password) => {
              setTimeout(() => {
                navigationAcc.replace(
                  CreateAccountModalRoutes.RecoverAccountsList as any,
                  {
                    walletId,
                    network: networkId,
                    password,
                    purpose: '',
                    template: '',
                  },
                );
              }, 20);
            },
          },
        },
      });
    });
  }, [navigationAcc, walletId, networkId]);

  const options = useMemo(() => {
    const baseOptions: IBaseMenuOptions = [
      {
        id: 'form__rpc_node',
        onPress: () => toCheckNodePage(),
        icon: 'LinkOutline',
      },
      {
        id: 'action__export',
        onPress: () => toExportPage(),
        icon: 'KeytagOutline',
      },
      {
        id: 'action__view_details',
        onPress: () => toViewDetailsPage(),
        icon: 'DocumentOutline',
      },
      {
        id: 'action__add_account',
        onPress: () => onPressCreateAccount(),
        icon: 'PlusCircleOutline',
      },
      walletIsHD(walletId) && {
        id: 'action__manage_account',
        onPress: onPressManageAccount,
        icon: 'SquaresPlusMini',
      },
      () => <Divider my={1} />,
      {
        id: 'action__remove_account',
        onPress: () => onPressRemoveAccount(),
        icon: 'TrashOutline',
        variant: 'desctructive',
      },
    ];
    return updateItemOptions
      ? baseOptions.concat(updateItemOptions)
      : baseOptions;
  }, [
    updateItemOptions,
    toCheckNodePage,
    onPressCreateAccount,
    toExportPage,
    toViewDetailsPage,
    onPressRemoveAccount,
    onPressManageAccount,
    walletId,
  ]);

  return (
    <>
      {RemoveAccountDialog}
      <BaseMenu placement="bottom left" options={options} {...props} />
    </>
  );
};

export default DesktopSidebarMoreMenu;
