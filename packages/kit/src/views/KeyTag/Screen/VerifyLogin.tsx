import { useMemo } from 'react';

import { useNavigation } from '@react-navigation/native';

import { Modal } from '@onekeyhq/components';

import Protected, { ValidationFields } from '../../../components/Protected';
import { useActiveWalletAccount } from '../../../hooks';
import { RootRoutes } from '../../../routes/routesEnum';
import LayoutContainer from '../../Onboarding/Layout';

const VerifyLogin = () => {
  const navigation = useNavigation();
  const { walletId } = useActiveWalletAccount();
  const modalContent = useMemo(
    () => (
      <Modal footer={null} hideBackButton closeable={false}>
        <Protected walletId={walletId} field={ValidationFields.Secret}>
          {() => {
            navigation.navigate(RootRoutes.Main);
          }}
        </Protected>
      </Modal>
    ),
    [navigation, walletId],
  );
  //   if (!navigateMode) return modalContent;
  return <LayoutContainer>{modalContent}</LayoutContainer>;
};

export default VerifyLogin;
