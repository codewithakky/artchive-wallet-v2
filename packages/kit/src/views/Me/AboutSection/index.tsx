import { useCallback } from 'react';

import { useNavigation } from '@react-navigation/core';
import { useIntl } from 'react-intl';

import {
  Box,
  Icon,
  Pressable,
  Text,
  ToastManager,
  Typography,
  useTheme,
} from '@onekeyhq/components';
import { copyToClipboard } from '@onekeyhq/components/src/utils/ClipboardUtils';
import backgroundApiProxy from '@onekeyhq/kit/src/background/instance/backgroundApiProxy';
// import { useSettings } from '@onekeyhq/kit/src/hooks/redux';
// import { useHelpLink } from '@onekeyhq/kit/src/hooks/useHelpLink';
import { setDevMode } from '@onekeyhq/kit/src/store/reducers/settings';

import {
  ManageNetworkModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../routes/routesEnum';
import { openUrlExternal } from '../../../utils/openUrl';
import { APP_VERSION } from '../../constants';

import AppRateSectionItem from './AppRateSectionItem';
import AutoUpdateSectionItem from './AutoUpdateSectionItem';

import type { ModalScreenProps } from '../../../routes/types';
import type { ManageConnectedSitesRoutesParams } from '../../ManageConnectedSites/types';

type NavigationProps = ModalScreenProps<ManageConnectedSitesRoutesParams>;

export const AboutSection = () => {
  const intl = useIntl();

  const { dispatch } = backgroundApiProxy;
  const { themeVariant } = useTheme();
  const navigation = useNavigation<NavigationProps['navigation']>();
  // const userAgreementUrl = useHelpLink({ path: 'articles/360002014776' });
  // const privacyPolicyUrl = useHelpLink({
  //   path: 'https://artchivewallet.com/privacy-policy.html',
  // });
  // const settings = useSettings();
  let lastTime: Date | undefined;
  let num = 0;
  const openDebugMode = () => {
    const nowTime = new Date();
    if (
      lastTime === undefined ||
      Math.round(nowTime.getTime() - lastTime.getTime()) > 5000
    ) {
      lastTime = nowTime;
      num = 0;
    } else {
      num += 1;
    }
    if (num >= 9) {
      dispatch(setDevMode(true));
    }
  };

  const handleCopyVersion = useCallback(
    (version) => {
      copyToClipboard(version);
      ToastManager.show({ title: intl.formatMessage({ id: 'msg__copied' }) });
    },
    [intl],
  );
  return (
    <Box w="full" mb="6">
      <Box pb="2">
        <Typography.Subheading color="text-subdued">
          {intl.formatMessage({
            id: 'form__about_uppercase',
          })}
        </Typography.Subheading>
      </Box>
      <Box
        borderRadius="12"
        bg="surface-default"
        borderWidth={themeVariant === 'light' ? 1 : undefined}
        borderColor="border-subdued"
      >
        <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          borderBottomWidth="1"
          borderBottomColor="divider"
          onPress={() => {
            openDebugMode();
            handleCopyVersion(
              `${APP_VERSION}`,
              // `${settings.version}${
              //   settings.buildNumber ? `-${settings.buildNumber}` : ''
              // }`,
            );
          }}
        >
          <Icon name="HashtagOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
            _web={{
              style: {
                // @ts-ignore
                WebkitUserSelect: 'none',
                userSelect: 'none',
              },
            }}
          >
            {intl.formatMessage({
              id: 'form__version',
            })}
          </Text>
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            color="text-subdued"
          >
            {APP_VERSION}
            {/* {settings.version}
            {settings.buildNumber ? `-${settings.buildNumber}` : ''} */}
          </Text>
        </Pressable>
        <AutoUpdateSectionItem />
        <AppRateSectionItem />
        {/* Need to User Agreement */}
        {/* <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          borderBottomWidth="1"
          borderBottomColor="divider"
          onPress={() =>
            // openUrlByWebview(
            //   userAgreementUrl,
            //   intl.formatMessage({
            //     id: 'form__user_agreement',
            //   }),
            // )
            ToastManager.show({ title: intl.formatMessage({ id: 'msg__copied' }) })
          }
        >
          <Icon name="UserCircleOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
          >
            {intl.formatMessage({
              id: 'form__user_agreement',
            })}
          </Text>
          <Box>
            <Icon name="ChevronRightMini" color="icon-subdued" size={20} />
          </Box>
        </Pressable> */}
        <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          borderBottomWidth="1"
          borderBottomColor="divider"
          onPress={() =>
            openUrlExternal('https://artchivewallet.com/privacy-policy.html')
          }
        >
          <Icon name="EllipsisHorizontalCircleOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
          >
            {intl.formatMessage({
              id: 'form__privacy_policy',
            })}
          </Text>
          <Box>
            <Icon name="ChevronRightMini" color="icon-subdued" size={20} />
          </Box>
        </Pressable>
        <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          borderBottomWidth="1"
          borderBottomColor="divider"
          onPress={() => openUrlExternal('https://artchivewallet.com')}
        >
          <Icon name="GlobeAltOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
          >
            {intl.formatMessage({
              id: 'form__website',
            })}
          </Text>
          <Icon
            name="ArrowTopRightOnSquareMini"
            color="icon-subdued"
            size={20}
          />
        </Pressable>
        <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          borderBottomWidth="1"
          borderBottomColor="divider"
          onPress={() =>
            navigation.navigate(RootRoutes.Modal, {
              screen: ModalRoutes.ManageNetwork,
              params: {
                screen: ManageNetworkModalRoutes.ViewWalletDetails,
              },
            })
          }
        >
          <Icon name="DocumentTextOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
          >
            View Active Wallet Details
          </Text>
          <Box>
            <Icon name="ChevronRightMini" color="icon-subdued" size={20} />
          </Box>
        </Pressable>
        {/* Commenting discord and Twitter as of now */}
        {/* <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          borderBottomWidth="1"
          borderBottomColor="divider"
          onPress={() => openUrlExternal('https://www.discord.gg/onekey')}
        >
          <Icon name="DiscordOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
          >
            Discord
          </Text>
          <Icon
            name="ArrowTopRightOnSquareMini"
            color="icon-subdued"
            size={20}
          />
        </Pressable>
        <Pressable
          display="flex"
          flexDirection="row"
          alignItems="center"
          py={4}
          px={{ base: 4, md: 6 }}
          onPress={() => openUrlExternal('https://www.twitter.com/onekeyhq')}
        >
          <Icon name="TwitterOutline" />
          <Text
            typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
            flex={1}
            mx={3}
          >
            Twitter
          </Text>
          <Icon
            name="ArrowTopRightOnSquareMini"
            color="icon-subdued"
            size={20}
          />
        </Pressable> */}
      </Box>
    </Box>
  );
};
