import AptosMartianLogo from '@onekeyhq/kit/assets/walletLogo/aptos_martian.png';
import AptosPetraLogo from '@onekeyhq/kit/assets/walletLogo/aptos_petra.png';
import CardanoNamiLogo from '@onekeyhq/kit/assets/walletLogo/cardano_nami.png';
import ConflusFluentLogo from '@onekeyhq/kit/assets/walletLogo/conflux_fluent_wallet.png';
import CosmosKeplrLogo from '@onekeyhq/kit/assets/walletLogo/cosmos_keplr.png';
import MetamaskLogo from '@onekeyhq/kit/assets/walletLogo/evm_metamask.png';
import SolanaPhantomLogo from '@onekeyhq/kit/assets/walletLogo/solana_phantom.png';
import StarcoinStarmaskLogo from '@onekeyhq/kit/assets/walletLogo/starcoin_starmask.png';
import SuiWalletLogo from '@onekeyhq/kit/assets/walletLogo/sui_sui_wallet.png';
import TronLinkLogo from '@onekeyhq/kit/assets/walletLogo/tron_tronlink.png';

import type { WalletSwitchItem } from '../../../../store/reducers/settings';

export const CWalletSwitchDefaultConfig: Record<string, WalletSwitchItem> = {
  'EVM-Metamask': {
    logo: MetamaskLogo,
    title: 'Metamask',
    propertyKeys: ['ethereum'],
    enable: false,
  },
  'COSMOS-Keplr': {
    logo: CosmosKeplrLogo,
    title: 'Keplr',
    propertyKeys: [
      'keplr',
      'getOfflineSigner',
      'getOfflineSignerOnlyAmino',
      'getOfflineSignerAuto',
    ],
    enable: false,
  },
  'APTOS-Petra': {
    logo: AptosPetraLogo,
    title: 'Petra',
    propertyKeys: ['aptos'],
    enable: false,
  },
  'APTOS-Martian': {
    logo: AptosMartianLogo,
    title: 'Martian',
    propertyKeys: ['martian'],
    enable: false,
  },
  'SUI-Sui Wallet': {
    logo: SuiWalletLogo,
    title: 'Sui Wallet',
    propertyKeys: ['suiWallet'],
    enable: false,
  },
  'SOLANA-Phantom': {
    logo: SolanaPhantomLogo,
    title: 'Phantom',
    propertyKeys: ['solana', 'phantom'],
    enable: false,
  },
  'TRON-TronLink': {
    logo: TronLinkLogo,
    title: 'TronLink',
    propertyKeys: ['tronLink', 'tronWeb', 'sunWeb'],
    enable: false,
  },
  'CARDAND-Nami': {
    logo: CardanoNamiLogo,
    title: 'Nami',
    propertyKeys: ['cardano'],
    enable: false,
  },
  'STARCOIN-StarMask': {
    logo: StarcoinStarmaskLogo,
    title: 'StarMask',
    propertyKeys: ['starcoin'],
    enable: false,
  },
  'CONFLUX-Fluent': {
    logo: ConflusFluentLogo,
    title: 'Fluent',
    propertyKeys: ['conflux'],
    enable: false,
  },
};
