import type { FC } from 'react';
import { useCallback, useMemo } from 'react';

import { useNavigation } from '@react-navigation/core';

import type { AccountCredentialType } from '@onekeyhq/engine/src/types/account';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { useActiveWalletAccount } from '../../../hooks';
import {
  ManagerAccountModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../routes/routesEnum';
import BaseMenu from '../../Overlay/BaseMenu';
import useUpdateItem from '../../Overlay/useUpdateItem';

import type { IBaseMenuOptions, IMenu } from '../../Overlay/BaseMenu';

const ExportMnemonicMoreMenu: FC<IMenu> = (props) => {
  const updateItemOptions = useUpdateItem();
  const navigation = useNavigation();
  const { accountId, networkId, walletId } = useActiveWalletAccount();

  const onExportPrivateKey = useCallback(async () => {
    const data = await backgroundApiProxy.servicePassword.getPassword();
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManagerAccount,
      params: {
        screen: ManagerAccountModalRoutes.ManagerAccountExportPrivateModal,
        params: {
          accountId,
          networkId,
          password: data ?? '',
          accountCredential: {
            type: 'PrivateKey' as AccountCredentialType,
            key: 'action__export_private_key',
          },
        },
      },
    });
  }, [navigation, accountId, networkId]);

  const onExportPublicKey = useCallback(() => {
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManagerAccount,
      params: {
        screen: ManagerAccountModalRoutes.ManagerAccountExportPublicModal,
        params: {
          walletId,
          accountId,
          networkId,
        },
      },
    });
  }, [accountId, navigation, networkId, walletId]);

  const options = useMemo(() => {
    const baseOptions: IBaseMenuOptions = [
      {
        id: 'action__export_private_key',
        onPress: () => onExportPrivateKey(),
        icon: 'KeyOutline',
      },
      {
        id: 'action__export_public_key',
        onPress: () => onExportPublicKey(),
        icon: 'KeyOutline',
      },
    ];
    return updateItemOptions
      ? baseOptions.concat(updateItemOptions)
      : baseOptions;
  }, [updateItemOptions, onExportPrivateKey, onExportPublicKey]);

  return <BaseMenu options={options} {...props} />;
};

export default ExportMnemonicMoreMenu;
