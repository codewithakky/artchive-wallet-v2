import type { FC } from 'react';
import { useEffect, useState } from 'react';

import { useIntl } from 'react-intl';

import {
  Box,
  Button,
  Center,
  Icon,
  Modal,
  Typography,
  useSafeAreaInsets,
} from '@onekeyhq/components';

type AttentionsProps = {
  navigateMode?: boolean;
  pressTitle?: string;
  onPress: () => void;
  type?: string;
};

type ListType = {
  emoji: string;
  desc: string;
};

export const Attentions: FC<AttentionsProps> = ({
  navigateMode,
  onPress,
  pressTitle,
  type,
}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const [list, setList] = useState<Array<ListType>>([{ emoji: '', desc: '' }]);
  useEffect(() => {
    if (type === 'export_mnemonic_type') {
      setList([
        {
          emoji: '🔐',
          desc: intl.formatMessage({ id: 'modal__attention_unlock' }),
        },
        {
          emoji: '🤫',
          desc: intl.formatMessage({ id: 'modal__attention_shh' }),
        },
        {
          emoji: '🙅‍♂️',
          desc: intl.formatMessage({ id: 'modal__attention_gesturing_no' }),
        },
      ]);
    } else {
      setList([
        {
          emoji: '🔐',
          desc: 'The private key alone gives you full access to your wallets and funds.',
        },
        {
          emoji: '🤫',
          desc: intl.formatMessage({ id: 'modal__attention_shh' }),
        },
        {
          emoji: '🙅‍♂️',
          desc: 'ArtChive Wallet will never ask for your private key.',
        },
      ]);
    }
  }, [intl, type]);
  return (
    <Modal
      footer={null}
      hideBackButton={!!navigateMode}
      headerShown={!navigateMode}
    >
      <Box flex={1} px={{ base: 2, md: 0 }}>
        <Box flex={1}>
          <Center mb={8}>
            <Center p={4} mb={4} bg="surface-warning-default" rounded="full">
              <Icon
                name="ExclamationTriangleOutline"
                width={24}
                height={24}
                color="icon-warning"
              />
            </Center>
            <Typography.DisplayLarge>
              {intl.formatMessage({ id: 'modal__attention' })}
            </Typography.DisplayLarge>
          </Center>
          {list.map((item) => (
            <Box flexDirection="row" mb={4} key={item?.desc}>
              <Typography.DisplayLarge mt={-1} mr={4}>
                {item?.emoji}
              </Typography.DisplayLarge>
              <Typography.Body1 flex={1}>{item?.desc}</Typography.Body1>
            </Box>
          ))}
        </Box>
        <Button
          mt={4}
          mb={`${insets.bottom}px`}
          size="xl"
          type="primary"
          onPress={onPress}
        >
          {pressTitle ?? type === 'export_mnemonic_type'
            ? intl.formatMessage({ id: 'action__reveal_recovery_phrase' })
            : 'Reveal Private Key'}
        </Button>
      </Box>
    </Modal>
  );
};
