import { useCallback, useMemo } from 'react';

import {
  Box,
  Button,
  Icon,
  ListItem,
  Pressable,
  SectionList,
  Text,
} from '@onekeyhq/components';
import type { SectionListProps } from '@onekeyhq/components/src/SectionList';
import { TokenIcon } from '@onekeyhq/components/src/Token';
import { shortenAddress } from '@onekeyhq/components/src/utils';

import { useActiveWalletAccount } from '../../hooks';
import useFormatDate from '../../hooks/useFormatDate';
import useOpenBlockBrowser from '../../hooks/useOpenBlockBrowser';
import { LAMPORTS_PER_SOL } from '../TxHistory/SolTrxListView';

import type {
  ESolHistoryTx,
  ISolHistoryTx,
} from '../Wallet/HistoricalRecords/useHistoricalRecordsData';

export type ISolTxListSectionData = {
  title: string;
  data: ISolHistoryTx;
};
type ISolListItem = {
  key: number;
  label: string;
  data: string | JSX.Element;
  token?: {
    changeAmount?: number;
    changeType?: string;
    tokenName: string;
    tokenIcon: string;
  };
};
export function SolTxDetailView({ item }: { item: ESolHistoryTx }) {
  const { formatDate } = useFormatDate();
  const { network } = useActiveWalletAccount();
  const openBlockBrowser = useOpenBlockBrowser(network);
  const dataSource = useMemo(() => {
    if (item?.parsedInstruction?.length) {
      return [
        {
          title: 'Parsed Transactions Details',
          data: [
            {
              key: 1,
              label: 'Timestamp',
              data:
                item.blockTime && formatDate(new Date(item.blockTime * 1000)),
            },
            {
              key: 2,
              label: 'Tx Hash',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openTransactionDetails(item?.txHash)
                  }
                >
                  {shortenAddress(item?.txHash, 7)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 3,
              label: 'Fee (in SOL)',
              data: item.fee * LAMPORTS_PER_SOL,
            },
            {
              key: 4,
              label: 'Tx Signers',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openAddressDetails(
                      item.signer && item.signer[0],
                    )
                  }
                >
                  {item.signer && shortenAddress(item.signer[0], 5)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 5,
              label: 'Instructions',
              data: item.parsedInstruction[0]?.program,
            },
            {
              key: 6,
              label: 'Tx. Status',
              data: (
                <Text
                  color={
                    // eslint-disable-next-line no-nested-ternary
                    item.status === 'Success'
                      ? 'icon-success'
                      : item.status === 'Fail'
                      ? 'focused-critical'
                      : 'icon-warning'
                  }
                >
                  {item.status}
                </Text>
              ),
            },
          ],
        },
      ];
    }

    if (item?.src) {
      return [
        {
          title: 'Token Transfer Detail',
          data: [
            {
              key: 1,
              label: 'Timestamp',
              data:
                item?.blockTime && formatDate(new Date(item.blockTime * 1000)),
            },
            {
              key: 2,
              label: 'Tx Hash',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openTransactionDetails(item?.txHash)
                  }
                >
                  {shortenAddress(item?.txHash, 7)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 3,
              label: 'Fee (in SOL)',
              data: item.fee * LAMPORTS_PER_SOL,
            },
            {
              key: 4,
              label: 'From',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() => openBlockBrowser.openAddressDetails(item?.src)}
                >
                  {shortenAddress(item?.src, 5)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 5,
              label: 'To',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() => openBlockBrowser.openAddressDetails(item?.dst)}
                >
                  {shortenAddress(item?.dst, 5)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 6,
              label: 'Amount in (SOL)',
              data: item.lamport * LAMPORTS_PER_SOL,
            },
            {
              key: 7,
              label: 'Tx. Status',
              data: (
                <Text
                  color={
                    // eslint-disable-next-line no-nested-ternary
                    item.status === 'Success'
                      ? 'icon-success'
                      : item.status === 'Fail'
                      ? 'focused-critical'
                      : 'icon-warning'
                  }
                >
                  {item.status}
                </Text>
              ),
            },
          ],
        },
      ];
    }

    if (item?.change) {
      return [
        {
          title: 'SPL Transfer Detail',
          data: [
            {
              key: 1,
              label: 'Timestamp',
              data:
                item.blockTime && formatDate(new Date(item.blockTime * 1000)),
            },
            {
              key: 2,
              label: 'Tx Hash',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openTransactionDetails(item?.txHash)
                  }
                >
                  {shortenAddress(item?.txHash, 7)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 3,
              label: 'Fee (in SOL)',
              data: item.fee * LAMPORTS_PER_SOL,
            },
            {
              key: 4,
              label: 'Owner',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openAddressDetails(item?.change?.owner)
                  }
                >
                  {shortenAddress(item?.change?.owner, 5)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 5,
              label: 'To',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openAddressDetails(item?.change?.address)
                  }
                >
                  {shortenAddress(item?.change?.address, 5)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 6,
              label: 'Token Address',
              data: (
                <Pressable
                  color="interactive-default"
                  flexDirection={{ base: 'row' }}
                  alignItems="center"
                  onPress={() =>
                    openBlockBrowser.openAddressDetails(
                      item?.change?.tokenAddress,
                    )
                  }
                >
                  {shortenAddress(item?.change?.tokenAddress, 5)}
                  <Box ml={2} p={0} alignItems="center">
                    <Icon
                      name="ArrowTopRightOnSquareMini"
                      size={20}
                      color="interactive-default"
                    />
                  </Box>
                </Pressable>
              ),
            },
            {
              key: 7,
              label: 'Token',
              token: item?.change,
            },
            {
              key: 8,
              label: 'Tx. Status',
              data: (
                <Text
                  color={
                    // eslint-disable-next-line no-nested-ternary
                    item.status === 'Success'
                      ? 'icon-success'
                      : item.status === 'Fail'
                      ? 'focused-critical'
                      : 'icon-warning'
                  }
                >
                  {item.status}
                </Text>
              ),
            },
          ],
        },
      ];
    }
  }, [formatDate, item, openBlockBrowser]);

  const onExportDetails = useCallback(() => {
    const fileData = JSON.stringify(item);
    const blob = new Blob([fileData], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.download = 'Trx Details.json';
    link.href = url;
    link.click();
  }, [item]);

  const sectionListProps = useMemo<SectionListProps<any>>(
    () => ({
      // @ts-expect-error
      sections: dataSource,
      // @ts-expect-error
      renderSectionHeader: ({
        section,
      }: {
        section: ISolTxListSectionData;
      }) => (
        <ListItem
          mx={-2}
          mb={1.5}
          alignItems="center"
          justifyContent="space-between"
        >
          <Text
            typography={{ sm: 'Subheading', md: 'Subheading' }}
            color="text-subdued"
          >
            {section.title}
          </Text>
          <Box alignItems="center" flexDirection="row">
            <Button
              type="primary"
              rightIconName="ArrowDownTrayOutline"
              color="action-primary-default"
              onPress={() => onExportDetails()}
            >
              Export Details
            </Button>
          </Box>
        </ListItem>
      ),
      renderItem: ({ item: dataItem }: { item: ISolListItem }) => (
        <ListItem
          mx={-2}
          my={1.5}
          alignItems="center"
          justifyContent="space-between"
        >
          <Text typography="Body1Strong">{dataItem?.label}</Text>
          <Box alignItems="center" flexDirection="row">
            {dataItem?.token && (
              <>
                <Text mx={1} typography="Body1Strong" color="text-subdued">
                  {dataItem?.token?.changeType === 'inc' ? '+ ' : '- '}
                  {dataItem?.token?.changeAmount}
                </Text>
                <TokenIcon token={{ logoURI: dataItem.token?.tokenIcon }} />
              </>
            )}
            <Text
              ellipsizeMode="head"
              mx={1}
              typography="Body1Strong"
              color="text-subdued"
            >
              {dataItem.data || dataItem?.token?.tokenName}
            </Text>
          </Box>
        </ListItem>
      ),
    }),
    [dataSource, onExportDetails],
  );
  return <SectionList {...sectionListProps} />;
}
