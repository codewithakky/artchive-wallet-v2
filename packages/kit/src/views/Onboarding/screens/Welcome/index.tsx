import { useCallback, useEffect, useState } from 'react';

import { useNavigation } from '@react-navigation/core';
import { useRoute } from '@react-navigation/native';
import { useIntl } from 'react-intl';

import {
  Box,
  HStack,
  // Divider,
  // Hidden,
  // Icon,
  // Image,
  Text,
  VStack,
  useTheme,
  useUserDevice,
} from '@onekeyhq/components';
// import ContentHardwareImage from '@onekeyhq/kit/assets/onboarding/welcome_hardware.png';
import {
  AppUIEventBusNames,
  appUIEventBus,
} from '@onekeyhq/shared/src/eventBus/appUIEventBus';
import platformEnv from '@onekeyhq/shared/src/platformEnv';

import backgroundApiProxy from '../../../../background/instance/backgroundApiProxy';
import ARTCLogo, {
  ArtcIcon,
  BannerImage,
} from '../../../../components/ARTCLogo';
import {
  useActiveWalletAccount,
  useNavigationActions,
} from '../../../../hooks';
import {
  ModalRoutes,
  // CreateWalletModalRoutes,
  // ModalRoutes,
  RootRoutes,
} from '../../../../routes/routesEnum';
import { setOnBoardingLoadingBehindModal } from '../../../../store/reducers/runtime';
import {
  ArtchiveIcon,
  WelcomeBannerShow,
  WelcomeBannerShowDark,
} from '../../../constants';
import { KeyTagRoutes } from '../../../KeyTag/Routes/enums';
import Layout from '../../Layout';
// import { useOnboardingContext } from '../../OnboardingContext';
import { EOnboardingRoutes } from '../../routes/enums';

// import { ConnectThirdPartyWallet } from './ConnectThirdPartyWallet';
import PressableListItem from './PressableListItem';
import TermsOfService from './TermsOfService';

import type { IOnboardingRoutesParams } from '../../routes/types';
import type { RouteProp } from '@react-navigation/native';
import type { StackNavigationProp } from '@react-navigation/stack';

type NavigationProps = StackNavigationProp<
  IOnboardingRoutesParams,
  EOnboardingRoutes.Welcome
>;

type RouteProps = RouteProp<IOnboardingRoutesParams, EOnboardingRoutes.Welcome>;

const Welcome = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // const navigation = useAppNavigation();
  const navigation = useNavigation<NavigationProps>();
  const navigateHome = useNavigation();
  const navigationActions = useNavigationActions();
  if (process.env.NODE_ENV !== 'production') {
    global.$$navigationActions = navigationActions;
  }

  const route = useRoute<RouteProps>();
  const [disableAnimation, setDisableAnimation] = useState(
    !!route?.params?.disableAnimation,
  );
  const resetLayoutAnimation = useCallback(
    () => setDisableAnimation(!!route?.params?.disableAnimation),
    [route],
  );

  // const context = useOnboardingContext();
  // const forceVisibleUnfocused = context?.forceVisibleUnfocused;

  useEffect(() => {
    (async function () {
      if (
        platformEnv.isExtensionUiPopup ||
        platformEnv.isExtensionUiStandaloneWindow
      ) {
        if (await backgroundApiProxy.serviceApp.isResettingApp()) {
          return;
        }
        // open onBoarding by browser tab
        backgroundApiProxy.serviceApp.openExtensionExpandTab({
          routes: [RootRoutes.Onboarding, EOnboardingRoutes.Welcome],
          params: {},
        });
        setTimeout(() => {
          window.close();
        }, 200);
      }
    })();
  }, []);

  useEffect(() => {
    // Fix cardano webembed crash when onboarding page is closed on Android platform.
    if (platformEnv.isNative) {
      appUIEventBus.emit(AppUIEventBusNames.ChainWebEmbedDisabled);
    }
  }, []);

  const intl = useIntl();
  const { themeVariant } = useTheme();
  const isSmallHeight = useUserDevice().screenHeight <= 667;
  const { wallet } = useActiveWalletAccount();
  // const goBack = useNavigationBack();
  // const insets = useSafeAreaInsets();

  const onPressHome = useCallback(() => {
    resetLayoutAnimation();
    backgroundApiProxy.dispatch(setOnBoardingLoadingBehindModal(false));
    // backgroundApiProxy.serviceApp.lock(true);
    navigateHome.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.BackupWallet,
      params: {
        screen: KeyTagRoutes.VerifyLogin,
      },
    });
    // navigateHome.navigate(RootRoutes.Main);
  }, [navigateHome, resetLayoutAnimation]);
  const onPressCreateWallet = useCallback(() => {
    resetLayoutAnimation();
    backgroundApiProxy.dispatch(setOnBoardingLoadingBehindModal(false));
    navigation.navigate(EOnboardingRoutes.SetPassword);
  }, [navigation, resetLayoutAnimation]);
  const onPressImportWallet = useCallback(() => {
    resetLayoutAnimation();
    backgroundApiProxy.dispatch(setOnBoardingLoadingBehindModal(false));
    navigation.navigate(EOnboardingRoutes.ImportWallet);
  }, [navigation, resetLayoutAnimation]);

  // Commenting Hardware wallet as of now
  // const onPressHardwareWallet = useCallback(() => {
  //   setDisableAnimation(true);
  //   forceVisibleUnfocused?.();
  //   backgroundApiProxy.dispatch(setOnBoardingLoadingBehindModal(false));
  //   if (disableAnimation) {
  //     navigation.navigate(
  //       RootRoutes.Modal as any,
  //       {
  //         screen: ModalRoutes.CreateWallet,
  //         params: {
  //           screen: CreateWalletModalRoutes.ConnectHardwareModal,
  //         },
  //       } as any,
  //     );
  //   } else {
  //     setTimeout(() => {
  //       navigation.navigate(
  //         RootRoutes.Modal as any,
  //         {
  //           screen: ModalRoutes.CreateWallet,
  //           params: {
  //             screen: CreateWalletModalRoutes.ConnectHardwareModal,
  //           },
  //         } as any,
  //       );
  //     }, 100);
  //   }
  // }, [forceVisibleUnfocused, navigation, disableAnimation]);

  // const onPressThirdPartyWallet = useCallback(() => {
  //   resetLayoutAnimation();
  //   backgroundApiProxy.dispatch(setOnBoardingLoadingBehindModal(false));
  //   setTimeout(() => navigation.navigate(EOnboardingRoutes.ThirdPartyWallet));
  // }, [navigation, resetLayoutAnimation]);

  return (
    <>
      <Layout
        showCloseButton
        backButton={false}
        // pt={{ base: isSmallHeight ? 8 : 20, sm: 0 }}
        scaleFade
        disableAnimation={disableAnimation}
        maxWidthVal="100%"
        innerMarginTop="0"
      >
        {/* <Box
          maxW="100%"
          maxH={{ base: '100%', md: '90vh' }}
          alignItems="center"
          minH={{ base: '100%', md: '90vh' }}
          rounded="lg"
          overflow="hidden"
        > */}
        <Box
          maxW="100%"
          w="100%"
          minH="100%"
          rounded="lg"
          overflow="hidden"
          flexDir={{ base: 'column' }}
          justifyContent={{ base: 'center', md: 'center' }}
        >
          <HStack
            direction="row"
            mb="2.5"
            mt="1.5"
            flexDir={{ base: 'column', md: 'row' }}
            justifyContent={{ base: 'center', md: 'center' }}
          >
            <Box
              maxW={{ base: '100%', md: '60%' }}
              w="100%"
              minH="auto"
              alignItems="center"
              rounded="lg"
              // overflow="hidden"
            >
              <VStack
                // flexDir={{ sm: 'row' }}
                // flexWrap={{ sm: 'wrap' }}
                // mt={{ base: isSmallHeight ? 8 : 16, sm: 20 }}
                // mx={-2}
                space={2}
                alignItems="start"
              >
                {/* <Icon name="BrandLogoIllus" size={48} /> */}

                <Box
                  maxW={{ base: '100%', md: '100%' }}
                  w="100%"
                  minH="auto"
                  justifyContent="center"
                  alignItems="center"
                  overflow="hidden"
                  // display={{ base: 'none', md: 'flex' }}
                >
                  <HStack
                    direction="row"
                    mb="2.5"
                    mt={{ base: '10', md: '1.5' }}
                    space={0}
                    w={{ base: '100%', md: '100%' }}
                    justifyContent="space-between"
                    flexDir={{ base: 'row', md: 'row' }}
                    // flexWrap={{ base: 'wrap', md: 'nowrap' }}
                  >
                    <Box
                      maxW={{ base: '130', sm: '140' }}
                      w={{ base: '130', sm: '140' }}
                      justifyContent="center"
                      alignItems="center"
                      overflow="hidden"
                      // display={{ base: 'none', md: 'flex' }}
                    >
                      <ARTCLogo />
                    </Box>

                    {/* Banner Image Code */}

                    {/* <Box
                        maxW={{ base: '130', sm: '300' }}
                        w={{ base: '130', sm: '300' }}
                        justifyContent="center"
                        alignItems="center"
                        overflow="hidden"
                        // mt={10}
                        // p={2}
                        mr={3}
                        display={{ base: 'flex', sm: 'none' }}
                      >
                        <img
                          alt="logo"
                          width="100%"
                          height="100%"
                          src={
                            themeVariant === 'dark'
                              ? WelcomeBannerShow
                              : WelcomeBannerShowDark
                          }
                        />
                      </Box> */}
                  </HStack>
                </Box>
                <Text
                  typography={{ sm: 'DisplayXLarge', md: 'Display2XLarge' }}
                  mt={{ base: 0, md: 2 }}
                  flexGrow={1}
                >
                  {intl.formatMessage({
                    id: 'onboarding__landing_welcome_title',
                  })}
                  {'\n'}
                  <Text color="text-subdued">
                    {intl.formatMessage({
                      id: 'onboarding__landing_welcome_desc',
                    })}
                  </Text>
                </Text>
                <Box
                  flexDir={{ sm: 'row' }}
                  flexWrap={{ sm: 'wrap' }}
                  w={{ base: '100%', md: 'auto' }}
                  mt={{ base: isSmallHeight ? 8 : 10, sm: 10 }}
                  // mx={-2}
                  alignItems="start"
                >
                  <Box flexDirection={{ sm: 'row' }} w={{ base: '100%' }}>
                    {wallet && (
                      <PressableListItem
                        icon="LoginOutline"
                        label="Login"
                        description="Login to your existing wallet."
                        roundedBottom={{ base: 0, sm: 'xl' }}
                        onPress={onPressHome}
                      />
                    )}
                    <PressableListItem
                      icon="PlusCircleOutline"
                      label={intl.formatMessage({
                        id: 'action__create_wallet',
                      })}
                      description={intl.formatMessage({
                        id: 'content__create_wallet_desc',
                      })}
                      roundedBottom={{ base: 0, sm: 'xl' }}
                      onPress={onPressCreateWallet}
                    />
                    <PressableListItem
                      icon="ArrowDownCircleOutline"
                      label={intl.formatMessage({
                        id: 'action__import_wallet',
                      })}
                      description={intl.formatMessage({
                        id: 'content__onboarding_import_wallet_desc',
                      })}
                      mt="-1px"
                      mb={{ base: 0, sm: 0 }}
                      roundedTop={{ base: 0, sm: 'xl' }}
                      onPress={onPressImportWallet}
                    />
                    {/* Commenting hardware wallet as of now */}
                    {/* <PressableListItem
              icon="UsbCableOutline"
              label={intl.formatMessage({
                id: 'action__connect_hardware_wallet',
              })}
              description={intl.formatMessage({
                id: 'content__conenct_hardware_wallet_desc',
              })}
              onPress={onPressHardwareWallet}
              overflow="hidden"
            >
              <Hidden till="sm">
                <Box position="absolute" zIndex={-1} right="0" top="0">
                  <Image
                    source={ContentHardwareImage}
                    w="256px"
                    h="207px"
                    opacity={0.75}
                  />
                </Box>
              </Hidden>
            </PressableListItem> */}
                  </Box>
                </Box>
                <Box
                  flexDir={{ sm: 'row' }}
                  flexWrap={{ sm: 'wrap' }}
                  w={{ base: '100%', md: '100%' }}
                  mt={{ base: isSmallHeight ? 3 : 3, sm: 6 }}
                  mb={{ base: isSmallHeight ? 6 : 6, sm: 6 }}
                  alignItems="start"
                >
                  <HStack
                    direction="row"
                    mb="2.5"
                    mt="1.5"
                    space={0}
                    w={{ base: '100%', md: '100%' }}
                    justifyContent="space-between"
                    flexDir={{ base: 'row', md: 'row' }}
                    // flexWrap={{ base: 'wrap', md: 'nowrap' }}
                  >
                    <Box
                      maxW={{ base: '16.67%' }}
                      w="100%"
                      minH="100%"
                      alignItems="center"
                      overflow="hidden"
                    >
                      <Box maxW={{ base: '60%', md: '33%' }} w="100%">
                        <img
                          alt="logo"
                          width="100%"
                          src="https://onekey-asset.com/assets/eth/eth.png"
                        />
                      </Box>
                      {!platformEnv.isNative &&
                        !platformEnv.isExtensionUiPopup && (
                          <Text
                            fontSize="lg"
                            marginTop={3}
                            display={{ base: 'none', md: 'flex' }}
                          >
                            ETHEREUM
                          </Text>
                        )}
                    </Box>
                    <Box
                      maxW={{ base: '16.67%' }}
                      w="100%"
                      minH="100%"
                      alignItems="center"
                      overflow="hidden"
                    >
                      <Box maxW={{ base: '60%', md: '33%' }} w="100%">
                        <img
                          alt="logo"
                          width="100%"
                          src="https://onekey-asset.com/assets/tbtc/tbtc.png"
                        />
                      </Box>
                      {!platformEnv.isNative &&
                        !platformEnv.isExtensionUiPopup && (
                          <Text
                            fontSize="lg"
                            marginTop={3}
                            display={{ base: 'none', md: 'flex' }}
                          >
                            BTC
                          </Text>
                        )}
                    </Box>
                    <Box
                      maxW={{ base: '16.67%' }}
                      w="100%"
                      minH="100%"
                      alignItems="center"
                      rounded="lg"
                      overflow="hidden"
                    >
                      <Box maxW={{ base: '60%', md: '33%' }} w="100%">
                        <img
                          alt="logo"
                          width="100%"
                          src="https://onekey-asset.com/assets/sol/sol.png"
                        />
                      </Box>
                      {!platformEnv.isNative &&
                        !platformEnv.isExtensionUiPopup && (
                          <Text
                            fontSize="lg"
                            marginTop={3}
                            display={{ base: 'none', md: 'flex' }}
                          >
                            SOLANA
                          </Text>
                        )}
                    </Box>
                    <Box
                      maxW={{ base: '16.67%' }}
                      w="100%"
                      minH="100%"
                      alignItems="center"
                      overflow="hidden"
                    >
                      <Box maxW={{ base: '60%', md: '33%' }} w="100%">
                        <img
                          alt="logo"
                          width="100%"
                          src="https://onekey-asset.com/assets/tpolygon/tpolygon.png"
                        />
                      </Box>
                      {!platformEnv.isNative &&
                        !platformEnv.isExtensionUiPopup && (
                          <Text
                            fontSize="lg"
                            marginTop={3}
                            display={{ base: 'none', md: 'flex' }}
                          >
                            POLYGON
                          </Text>
                        )}
                    </Box>
                    <Box
                      maxW={{ base: '16.67%' }}
                      w="100%"
                      minH="100%"
                      alignItems="center"
                      overflow="hidden"
                    >
                      <Box maxW={{ base: '60%', md: '33%' }} w="100%">
                        <img
                          alt="logo"
                          width="100%"
                          src="https://onekey-asset.com/assets/bsc/bsc.png"
                        />
                      </Box>
                      {!platformEnv.isNative &&
                        !platformEnv.isExtensionUiPopup && (
                          <Text
                            fontSize="lg"
                            marginTop={3}
                            display={{ base: 'none', md: 'flex' }}
                          >
                            BNB
                          </Text>
                        )}
                    </Box>
                    <Box
                      maxW={{ base: '16.67%' }}
                      w="100%"
                      minH="100%"
                      alignItems="center"
                      rounded="lg"
                      overflow="hidden"
                    >
                      <Box maxW={{ base: '60%', md: '33%' }} w="100%">
                        <ArtcIcon />
                      </Box>
                      {!platformEnv.isNative &&
                        !platformEnv.isExtensionUiPopup && (
                          <Text
                            fontSize="lg"
                            marginTop={3}
                            display={{ base: 'none', md: 'flex' }}
                          >
                            ARTCHIVE
                          </Text>
                        )}
                    </Box>
                  </HStack>
                </Box>
                {/* Commenting Third party wallet */}
                {/* <Hidden till="sm">
          <Box flexDirection="row" alignItems="center" mt="24px" mb="-12px">
            <Divider flex={1} />
            <Text mx="14px" typography="Subheading" color="text-disabled">
              {intl.formatMessage({ id: 'content__or_lowercase' })}
            </Text>
            <Divider flex={1} />
          </Box>
        </Hidden> */}
                {/* <ConnectThirdPartyWallet onPress={onPressThirdPartyWallet} /> */}
              </VStack>
            </Box>
            {!platformEnv.isNative && !platformEnv.isExtensionUiPopup && (
              <Box
                maxW={{ base: '100%', md: '40%' }}
                w="100%"
                minH="auto"
                justifyContent="center"
                alignItems="center"
                overflow="hidden"
                display={{ base: 'none', md: 'flex' }}
              >
                <Box
                  maxW={{ base: '60%', md: '100%' }}
                  w="100%"
                  minH="100%"
                  justifyContent="center"
                  alignItems="center"
                  overflow="hidden"
                  // display={{ base: 'none', md: 'flex' }}
                >
                  <VStack
                    // flexDir={{ sm: 'row' }}
                    // flexWrap={{ sm: 'wrap' }}
                    // mt={{ base: isSmallHeight ? 8 : 16, sm: 20 }}
                    // mx={-2}
                    h="auto"
                    space={2}
                    alignItems="center"
                  >
                    <BannerImage />
                  </VStack>
                </Box>
              </Box>
            )}
          </HStack>
        </Box>
        {/* </Box> */}
      </Layout>
      <TermsOfService />
    </>
  );
};

export default Welcome;
