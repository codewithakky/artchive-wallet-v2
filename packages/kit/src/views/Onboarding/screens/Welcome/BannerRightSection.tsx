// import React from 'react';
// import type { FC } from 'react';

import { WelcomeBannerShow } from '../../../constants';
// type Props = {}

const BannerRightSection = () => (
  <img alt="logo" width={140} src={WelcomeBannerShow} />
);

export default BannerRightSection;
