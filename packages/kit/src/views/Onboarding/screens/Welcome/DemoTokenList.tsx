import { Box, Image, Text } from '@onekeyhq/components';
import { getPresetNetworks } from '@onekeyhq/engine/src/presets';
import platformEnv from '@onekeyhq/shared/src/platformEnv';

const DemoTokenList = () => {
  const implNetwork = ['evm--1', 'btc--1', 'sol--101', 'evm-137', 'evm-56'];
  const implTokenList = implNetwork.map((network) => {
    const presetNetwork = getPresetNetworks()[network];
    return (
      <Box
        maxW={{ base: '16.67%' }}
        w="100%"
        minH="100%"
        alignItems="center"
        overflow="hidden"
      >
        <Box maxW={{ base: '60%', md: '33%' }} w="100%">
          <Image
            alt="logo"
            width="100%"
            source={{ uri: presetNetwork.logoURI }}
          />
        </Box>
        {!platformEnv.isNative && !platformEnv.isExtensionUiPopup && (
          <Text
            fontSize="lg"
            marginTop={3}
            display={{ base: 'none', md: 'flex' }}
          >
            {String(presetNetwork?.name).toUpperCase()}
          </Text>
        )}
      </Box>
    );
  });
  return { implTokenList };
};

export default DemoTokenList;
