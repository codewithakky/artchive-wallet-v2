import { memo, useEffect, useMemo, useState } from 'react';

import { useNavigation } from '@react-navigation/core';
import { type RouteProp, useRoute } from '@react-navigation/native';
import { type StackNavigationProp } from '@react-navigation/stack';
import { useIntl } from 'react-intl';

import { Center, Select, Spinner } from '@onekeyhq/components';
import type { SelectItem } from '@onekeyhq/components/src/Select';

import backgroundApiProxy from '../../../../../background/instance/backgroundApiProxy';
import Protected, {
  ValidationFields,
} from '../../../../../components/Protected';
import { useData } from '../../../../../hooks/redux';
import { wait } from '../../../../../utils/helper';
import Layout from '../../../Layout';
import { EOnboardingRoutes } from '../../../routes/enums';
import { type IOnboardingRoutesParams } from '../../../routes/types';

import SecondaryContent from './SecondaryContent';

type NavigationProps = StackNavigationProp<
  IOnboardingRoutesParams,
  EOnboardingRoutes.SetPassword
>;
type RouteProps = RouteProp<
  IOnboardingRoutesParams,
  EOnboardingRoutes.SetPassword
>;

function RedirectToRecoveryPhrase({
  password,
  withEnableAuthentication,
  importedMnemonic,
  selectedNetwork,
}: {
  password: string;
  withEnableAuthentication?: boolean;
  importedMnemonic?: string;
  selectedNetwork?: string;
}) {
  const navigation = useNavigation<NavigationProps>();

  useEffect(() => {
    (async function () {
      if (importedMnemonic) {
        navigation.replace(EOnboardingRoutes.BehindTheScene, {
          password,
          mnemonic: importedMnemonic,
          withEnableAuthentication,
          selectedNetwork,
          entryW: true,
        });
      } else {
        const mnemonic = await backgroundApiProxy.engine.generateMnemonic();

        // return;
        await wait(600);
        navigation.replace(EOnboardingRoutes.RecoveryPhrase, {
          password,
          mnemonic,
          withEnableAuthentication,
          selectedNetwork,
        });
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Center h="full" w="full">
      <Spinner size="lg" />
    </Center>
  );
}

const RedirectToRecoveryPhraseMemo = memo(RedirectToRecoveryPhrase);

const SetPassword = () => {
  const intl = useIntl();
  const { isPasswordSet } = useData();
  const route = useRoute<RouteProps>();
  const mnemonic = route.params?.mnemonic;
  const disableAnimation = route?.params?.disableAnimation;
  const [selectedNetworkId, setSelectedNetworkId] = useState('evm--1');

  const title = useMemo(
    () =>
      isPasswordSet
        ? intl.formatMessage({
            id: 'Verify_Password',
          })
        : intl.formatMessage({ id: 'title__set_password' }),
    [intl, isPasswordSet],
  );
  const subTitle = useMemo(
    () =>
      isPasswordSet
        ? intl.formatMessage({
            id: 'Verify_password_to_continue',
          })
        : undefined,
    [intl, isPasswordSet],
  );

  const options = useMemo<SelectItem<string>[]>(
    () => [
      {
        label: 'EVM (ETH, MATIC, BNB)',
        value: 'evm--1',
        tokenProps: {
          token: {
            logoURI: 'https://onekey-asset.com/assets/eth/eth.png',
          },
        },
      },
      // {
      //   label: 'Binance Smart Chain',
      //   value: 'evm--56',
      //   tokenProps: {
      //     token: {
      //       logoURI: 'https://onekey-asset.com/assets/bsc/bsc.png',
      //     },
      //   },
      // },
      // {
      //   label: 'Polygon',
      //   value: 'evm--137',
      //   tokenProps: {
      //     token: {
      //       logoURI: 'https://onekey-asset.com/assets/polygon/polygon.png',
      //     },
      //   },
      // },
      {
        label: 'Bitcoin',
        value: 'btc--0',
        tokenProps: {
          token: {
            logoURI: 'https://onekey-asset.com/assets/btc/btc.png',
          },
        },
      },
      {
        label: 'Solana',
        value: 'sol--101',
        tokenProps: {
          token: {
            logoURI: 'https://onekey-asset.com/assets/sol/sol.png',
          },
        },
      },
      {
        label: 'All',
        value: 'all',
        iconProps: {
          name: 'TagOutline',
        },
      },
    ],
    [],
  );

  return (
    <Layout
      // make sure Spinner display
      showCloseButton
      fullHeight
      disableAnimation={disableAnimation}
      title={title}
      subTitle={subTitle}
      secondaryContent={
        isPasswordSet ? <SecondaryContent /> : <SecondaryContent />
      }
    >
      <Select
        dropdownPosition="right"
        defaultValue={selectedNetworkId}
        value={selectedNetworkId}
        onChange={(v) => setSelectedNetworkId(v)}
        activatable={false}
        options={options}
        headerShown={false}
        footer={null}
        containerProps={{ width: 'auto', marginBottom: '4' }}
        dropdownProps={{
          width: 248,
          height: 'auto',
        }}
      />
      <Protected
        isAutoHeight
        hideTitle
        walletId={null}
        skipSavePassword
        field={ValidationFields.Wallet}
      >
        {(password, { withEnableAuthentication }) => (
          <RedirectToRecoveryPhraseMemo
            password={password}
            withEnableAuthentication={withEnableAuthentication}
            importedMnemonic={mnemonic}
            selectedNetwork={selectedNetworkId}
          />
        )}
      </Protected>
    </Layout>
  );
};

export default SetPassword;
