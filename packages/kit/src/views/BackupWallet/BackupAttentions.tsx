import { useCallback } from 'react';

import { useNavigation as useNavigationProp } from '@react-navigation/core';
import { useNavigation, useRoute } from '@react-navigation/native';

import type { BackupWalletRoutesParams } from '@onekeyhq/kit/src/routes/Root/Modal/BackupWallet';
import {
  BackupWalletModalRoutes,
  ManagerAccountModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '@onekeyhq/kit/src/routes/routesEnum';
import type { ModalScreenProps } from '@onekeyhq/kit/src/routes/types';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { useActiveWalletAccount } from '../../hooks';
import { Attentions } from '../CreateWallet/AppWallet/Attentions';

import type { RouteProp } from '@react-navigation/native';

type RouteProps = RouteProp<
  BackupWalletRoutesParams,
  BackupWalletModalRoutes.BackupWalletAttentionsModal
>;

type NavigationProps = ModalScreenProps<BackupWalletRoutesParams>;

const BackupWalletAttentions = () => {
  const route = useRoute<RouteProps>();
  const navigation = useNavigation<NavigationProps['navigation']>();
  const navigationNode = useNavigationProp();
  const { walletId, password, type, credentialInfo } = route.params;
  const { accountId, networkId } = useActiveWalletAccount();

  const onExportMnemonic = useCallback(async () => {
    const mnemonic = await backgroundApiProxy.engine.revealHDWalletMnemonic(
      walletId,
      password,
    );
    navigation.navigate(BackupWalletModalRoutes.BackupWalletMnemonicModal, {
      mnemonic,
      walletId,
      credentialInfo,
      password,
    });
  }, [navigation, password, walletId, credentialInfo]);

  const onExportPrivateKey = useCallback(() => {
    navigationNode.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManagerAccount,
      params: {
        screen: ManagerAccountModalRoutes.ManagerAccountExportPrivateModal,
        params: {
          accountId,
          networkId,
          password,
          accountCredential: credentialInfo,
        },
      },
    });
  }, [navigationNode, accountId, networkId, credentialInfo, password]);

  const onPress = useCallback(async () => {
    switch (type) {
      case 'export_mnemonic_type':
        return onExportMnemonic();
      case 'export_private_type':
        return onExportPrivateKey();
      default:
        return null;
    }
  }, [onExportMnemonic, onExportPrivateKey, type]);

  return <Attentions type={type} onPress={onPress} />;
};

export default BackupWalletAttentions;
