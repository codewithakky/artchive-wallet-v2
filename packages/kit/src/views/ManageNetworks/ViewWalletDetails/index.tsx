import type { FC } from 'react';
import { useCallback, useMemo } from 'react';

import { Box, Button, ListItem, Modal, Text } from '@onekeyhq/components';
import type { SectionListProps } from '@onekeyhq/components/src/SectionList';
import { shortenAddress } from '@onekeyhq/components/src/utils';

import { useActiveWalletAccount } from '../../../hooks';

import type { IAccountInfoListSectionData } from '../../ManagerAccount/AccountInfo';

type IAccountInfoListItem = {
  key: number;
  label: string;
  data: string;
};

const ViewWalletDetailsModal: FC = () => {
  const {
    wallet,
    // walletId,
    accountId,
    accountAddress,
    networkId,
    account,
    network,
  } = useActiveWalletAccount();
  const dataSource = useMemo(
    () => [
      {
        title: 'Active Wallet Info',
        data: [
          {
            key: 1,
            label: 'Wallet Id',
            data: wallet?.name,
          },
          {
            key: 2,
            label: 'Account Id',
            data: accountId,
          },
          {
            key: 3,
            label: 'Account Address',
            data: accountAddress,
          },
          {
            key: 4,
            label: 'Network & Chain Id',
            data: networkId,
          },
          {
            key: 5,
            label: 'BlockChain',
            data: network?.name,
          },
          {
            key: 6,
            label: 'Native Token',
            data: network?.symbol,
          },
          account?.path && {
            key: 7,
            label: 'Path',
            data: account?.path,
          },
        ],
      },
    ],
    [
      account?.path,
      networkId,
      accountAddress,
      accountId,
      network?.name,
      network?.symbol,
      wallet?.name,
    ],
  );

  const onExportDetails = useCallback(() => {
    const fileData = JSON.stringify(dataSource[0]?.data);
    const blob = new Blob([fileData], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.download = 'Wallet Details.json';
    link.href = url;
    link.click();
  }, [dataSource]);

  const sectionListProps = useMemo<SectionListProps<any>>(
    () => ({
      sections: dataSource,
      // @ts-expect-error
      renderSectionHeader: ({
        section,
      }: {
        section: IAccountInfoListSectionData;
      }) => (
        <ListItem
          mx={-2}
          my={1.5}
          alignItems="center"
          justifyContent="space-between"
        >
          <Text
            typography={{ sm: 'Subheading', md: 'Subheading' }}
            color="text-subdued"
          >
            {section.title}
          </Text>
          <Box alignItems="center" flexDirection="row">
            <Button
              type="primary"
              rightIconName="ArrowDownTrayOutline"
              color="action-primary-default"
              onPress={() => onExportDetails()}
            >
              Export Details
            </Button>
          </Box>
        </ListItem>
      ),
      renderItem: ({ item }: { item: IAccountInfoListItem }) => (
        <ListItem
          mx={-2}
          my={1.5}
          alignItems="center"
          justifyContent="space-between"
        >
          <Text typography="Body1Strong">{item.label}</Text>
          <Box alignItems="center" flexDirection="row">
            {item.key === 6 && (
              <img width={20} alt="tokensymbol" src={network?.logoURI} />
            )}
            <Text
              ellipsizeMode="head"
              mx={1}
              typography="Body1Strong"
              color="text-subdued"
            >
              {item.key === 1 && wallet?.avatar?.emoji}
              {item.key === 3
                ? shortenAddress(account?.address ?? '')
                : item.data}
            </Text>
          </Box>
        </ListItem>
      ),
    }),
    [
      dataSource,
      network?.logoURI,
      wallet?.avatar?.emoji,
      onExportDetails,
      account?.address,
    ],
  );

  return (
    <Modal
      header="Wallet Details"
      headerDescription={account?.name}
      sectionListProps={sectionListProps}
      footer={null}
    />
  );
};

export default ViewWalletDetailsModal;
