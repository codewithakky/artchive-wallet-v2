import WelcomeBanner from '@onekeyhq/kit/assets/onboarding/banner/onboard_banner.png';
import WelcomeBannerDark from '@onekeyhq/kit/assets/onboarding/banner/onboard_banner_dark.png';
import ArtcIcon from '@onekeyhq/kit/assets/tokenIcon/artc.png';
import EthIcon from '@onekeyhq/kit/assets/tokenIcon/eth.png';
import SolIcon from '@onekeyhq/kit/assets/tokenIcon/solana.png';

export const ARTC_DARK =
  'https://nltmarketplace.s3.us-west-1.amazonaws.com/512x512+(1).png';

export const ARTC_LIGHT =
  'https://nltmarketplace.s3.us-west-1.amazonaws.com/W-512x512.png';

export const APP_VERSION = '0.0.96';

export const WelcomeBannerShow =
  'https://nltmarketplace.s3.us-west-1.amazonaws.com/onboard_banner.png';
export const WelcomeBannerShowDark =
  'https://nltmarketplace.s3.us-west-1.amazonaws.com/onboard_banner_dark.png';
export const SolanaIcon = SolIcon;
export const EtherumIcon = EthIcon;
export const ArtchiveIcon =
  'https://nltmarketplace.s3.us-west-1.amazonaws.com/artc_logo.png';
