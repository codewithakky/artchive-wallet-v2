import type { FC } from 'react';
import { useCallback, useEffect } from 'react';

import { useRoute } from '@react-navigation/core';
import { useIntl } from 'react-intl';

import { Modal, Spinner, useIsVerticalLayout } from '@onekeyhq/components';
import type { TransactionDetailRoutesParams } from '@onekeyhq/kit/src/routes/Root/Modal/TransactionDetail';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { TxActionElementTime } from '../TxDetail/elements/TxActionElementTime';
import { TxDetailView } from '../TxDetail/TxDetailView';

import type { TransactionDetailModalRoutes } from '../../routes/routesEnum';
import type { RouteProp } from '@react-navigation/native';

type TransactionDetailRouteProp = RouteProp<
  TransactionDetailRoutesParams,
  TransactionDetailModalRoutes.HistoryDetailModal
>;

/* TODO status ICON
    switch (status) {
      case TxStatus.Pending:
        statusTitle = 'transaction__pending';
        statusIconName = 'DotsCircleHorizontalOutline';
        iconColor = 'icon-warning';
        textColor = 'text-warning';
        iconContainerColor = 'surface-warning-default';
        break;
      case TxStatus.Confirmed:
        statusTitle = 'transaction__success';
        statusIconName = 'CheckCircleOutline';
        iconColor = 'icon-success';
        textColor = 'text-success';
        iconContainerColor = 'surface-success-default';
        break;
      case TxStatus.Dropped:
        statusTitle = 'transaction__dropped';
        break;
      default:
        break;
    }
 */

const TxHistoryDetailModal: FC = () => {
  const route = useRoute<TransactionDetailRouteProp>();
  const intl = useIntl();
  const isSmallScreen = useIsVerticalLayout();
  const { decodedTx, historyTx } = route.params;
  useEffect(() => {
    if (!historyTx) {
      return;
    }
    const { accountId, networkId } = historyTx.decodedTx;
    const timer = setTimeout(() => {
      backgroundApiProxy.serviceHistory.updateHistoryStatus({
        networkId,
        accountId,
        items: [historyTx],
      });
      backgroundApiProxy.serviceHistory.updateHistoryFee({
        networkId,
        accountId,
        tx: historyTx,
      });
    }, 1500);
    return () => {
      clearTimeout(timer);
    };
  }, [historyTx]);

  const onExportDetails = useCallback(() => {
    const fileData = JSON.stringify(decodedTx);
    const blob = new Blob([fileData], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.download = 'Trx Details.json';
    link.href = url;
    link.click();
  }, [decodedTx]);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const headerDescription = (
    <TxActionElementTime
      timestamp={decodedTx?.updatedAt ?? decodedTx?.createdAt}
    />
  );
  return (
    <Modal
      header={intl.formatMessage({ id: 'transaction__transaction_details' })}
      headerDescription={headerDescription}
      hidePrimaryAction={false}
      hideSecondaryAction
      primaryActionProps={{
        type: 'primary',
        w: isSmallScreen ? 'full' : undefined,
        rightIconName: 'ArrowDownTrayOutline',
      }}
      primaryActionTranslationId="title__backup_details"
      onPrimaryActionPress={() => onExportDetails()}
      height="560px"
      scrollViewProps={{
        children: decodedTx ? (
          <TxDetailView
            isHistoryDetail
            decodedTx={decodedTx}
            historyTx={historyTx}
          />
        ) : (
          <Spinner />
        ),
      }}
    />
  );
};

export { TxHistoryDetailModal };
