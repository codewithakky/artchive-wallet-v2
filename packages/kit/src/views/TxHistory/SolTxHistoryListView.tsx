import type { FC } from 'react';
import { memo, useCallback, useEffect, useMemo, useRef, useState } from 'react';

// import { useNavigation } from '@react-navigation/native';
import { orderBy } from 'lodash';

import {
  Box,
  Divider,
  FlatList,
  IconButton,
  useUserDevice,
} from '@onekeyhq/components';
import type { IHistoryTx } from '@onekeyhq/engine/src/vaults/types';

import backgroundApiProxy from '../../background/instance/backgroundApiProxy';
import { useActiveWalletAccount } from '../../hooks';
import {
  type ESolHistoryTx,
  type ISolHistoryTx,
  solTrxRecordsData,
} from '../Wallet/HistoricalRecords/useHistoricalRecordsData';

import { SOLTX_FAKE_SKELETON_LIST_ARRAY, SolTxListHeadTags } from './Config';
import SolTrxListView from './SolTrxListView';
import SolTxListHeader from './SolTxListHeader';
// import type { HomeRoutesParams } from '../../routes/types';
// import type { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { TxHistoryListViewEmpty } from './TxHistoryListViewEmpty';

import type {
  FlatList as FlatListType,
  ListRenderItem,
  NativeScrollEvent,
  NativeSyntheticEvent,
} from 'react-native';

// type NavigationProps = NativeStackNavigationProp<HomeRoutesParams>;
type IProps = {
  address: string;
  rpcUrl: string | undefined;
  accountId: string;
};

const SolTxHistoryListView: FC<IProps> = ({ address, rpcUrl }) => {
  const { size } = useUserDevice();
  const isVerticalLayout = size === 'SMALL';
  const [solTrx, setSolTrx] = useState<ISolHistoryTx | IHistoryTx[]>([]);
  const [refreshing, setRefreshing] = useState(false);
  const { serviceHistory } = backgroundApiProxy;
  const { networkId, accountId, network } = useActiveWalletAccount();
  const getTransactionData = useCallback(async () => {
    if (!address || !networkId) {
      return;
    }
    // Special case of Solana History
    if (networkId.includes('sol')) {
      setRefreshing(true);
      const cluster =
        // eslint-disable-next-line no-nested-ternary
        rpcUrl?.includes('devnet')
          ? 'devnet'
          : rpcUrl?.includes('testnet')
          ? 'testnet'
          : undefined;
      const trx = await solTrxRecordsData({
        address,
        cluster,
      });
      const copyTrx = orderBy([...trx], ['blockTime'], ['desc']);
      setSolTrx(copyTrx);
      setRefreshing(false);
    } else {
      setRefreshing(true);
      const txHist = await serviceHistory.refreshHistory({
        networkId,
        accountId,
      });
      setSolTrx(txHist);
      setRefreshing(false);
    }
  }, [address, rpcUrl, accountId, networkId, serviceHistory]);
  useEffect(() => {
    getTransactionData();
    // return trx;
  }, [getTransactionData]);

  const listHeadTags = useMemo(() => {
    const isMidLayout = size === 'NORMAL';
    if (isMidLayout) {
      return SolTxListHeadTags.filter((t) => t.showNorMalDevice);
    }
    if (isVerticalLayout) {
      return SolTxListHeadTags.filter((t) => t.showVerticalLayout);
    }
    return SolTxListHeadTags;
  }, [isVerticalLayout, size]);
  //   const navigation = useNavigation<NavigationProps>();
  const scrollRef = useRef<FlatListType>(null);
  const renderItem: ListRenderItem<ESolHistoryTx & IHistoryTx> = useCallback(
    ({ item }) =>
      isVerticalLayout ? (
        <SolTrxListView network={network} item={item} headTags={listHeadTags} />
      ) : (
        <SolTrxListView network={network} item={item} headTags={listHeadTags} />
      ),
    [isVerticalLayout, listHeadTags, network],
  );
  const [goToTopBtnShow, setGoToTopBtnShow] = useState(false);
  const onScroll = useCallback((e: NativeSyntheticEvent<NativeScrollEvent>) => {
    const offsetHeight = e.nativeEvent.contentOffset.y;
    console.log(offsetHeight);
    setGoToTopBtnShow((offsetHeight ?? 0) > 20);
  }, []);

  const onRefresh = useCallback(() => {
    getTransactionData();
  }, [getTransactionData]);

  if (!refreshing && !solTrx.length) {
    return (
      <TxHistoryListViewEmpty isLoading={refreshing} refresh={onRefresh} />
    );
  }

  return (
    <>
      <FlatList
        flex={1}
        mt={3}
        px={isVerticalLayout ? 2 : 3}
        refreshing={refreshing}
        onRefresh={onRefresh}
        ref={scrollRef}
        bg="background-default"
        onScroll={onScroll}
        contentContainerStyle={{
          paddingBottom: 24,
        }}
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        keyExtractor={(item, index) => `${item}-${index}`}
        stickyHeaderIndices={[0]}
        scrollEventThrottle={200}
        data={
          // eslint-disable-next-line no-nested-ternary
          solTrx?.length ? solTrx : SOLTX_FAKE_SKELETON_LIST_ARRAY
        }
        renderItem={renderItem}
        ItemSeparatorComponent={!isVerticalLayout ? Divider : null}
        ListHeaderComponent={
          <Box pt={1} mt={-1} bgColor="background-default">
            <SolTxListHeader headTags={listHeadTags} />
          </Box>
        }
      />
      {goToTopBtnShow && (
        <IconButton
          circle
          name="UploadMini"
          position="absolute"
          size="base"
          type="basic"
          bottom="80px"
          right="20px"
          onPress={() => {
            scrollRef.current?.scrollToOffset({ offset: 0, animated: true });
          }}
        />
      )}
    </>
  );
};

export default memo(SolTxHistoryListView);
