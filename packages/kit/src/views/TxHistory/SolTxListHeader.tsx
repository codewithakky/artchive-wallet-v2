import type { FC } from 'react';
import { memo, useMemo } from 'react';

import {
  Box,
  Divider,
  ListItem,
  useIsVerticalLayout,
  useUserDevice,
} from '@onekeyhq/components';

import { ESolTxCellData, type SolListHeadTagType } from './Config';

interface SolListHeaderProps {
  headTags: SolListHeadTagType[];
}

const SolTxListHeader: FC<SolListHeaderProps> = ({ headTags }) => {
  //   const listSort = useListSort();
  const isVertical = useIsVerticalLayout();
  const { size } = useUserDevice();
  const isNormalDevice = useMemo(() => ['NORMAL'].includes(size), [size]);
  const ContainComponent = useMemo(
    () => (isVertical ? Box : ListItem),
    [isVertical],
  );
  const useDislocation = useMemo(
    () => isVertical || isNormalDevice,
    [isNormalDevice, isVertical],
  );
  return (
    <Box mt="2" w="full">
      <ContainComponent
        display="flex"
        alignItems="center"
        justifyContent="space-evenly"
        flexDirection="row"
        px={2}
        py={1.5}
        my={0.5}
      >
        {headTags.map((tag) => {
          if (tag.id === ESolTxCellData.Signature) {
            return (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: 'SIGNATURE',
                  labelProps: {
                    typography: 'Subheading',
                    textAlign: tag.textAlign,
                    color: 'text-subdued',
                  },
                }}
                w={tag.minW}
              />
            );
          }
          if (tag.id === ESolTxCellData.Instructions) {
            return (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: 'INSTRUCTIONS',
                  labelProps: {
                    typography: 'Subheading',
                    textAlign: tag.textAlign,
                    color: 'text-subdued',
                    isTruncated: true,
                  },
                }}
                w={tag.minW}
              />
            );
          }
          if (
            tag.id === ESolTxCellData.Block ||
            tag.id === ESolTxCellData.By ||
            tag.id === ESolTxCellData.Fee ||
            tag.id === ESolTxCellData.Status
          ) {
            const dislocationTitle = useDislocation
              ? tag.dislocation?.title ?? tag.title
              : tag.title;
            return (
              <ListItem.Column
                w={tag.minW}
                key={`${tag.title ?? ''}--${tag.id}`}
                text={{
                  label: dislocationTitle,
                  labelProps: {
                    typography: 'Subheading',
                    textAlign: tag.textAlign,
                    color: 'text-subdued',
                  },
                }}
              >
                {/* <Typography.Subheading
                  color="text-subdued"
                  textAlign={tag.textAlign}
                >
                  {dislocationTitle}
                </Typography.Subheading> */}
                {/* <Icon name="ChevronDownMini" size={16} color="icon-subdued" /> */}
              </ListItem.Column>
            );
          }
          return null;
        })}
      </ContainComponent>
      {!isVertical ? <Divider /> : null}
    </Box>
  );
};

export default memo(SolTxListHeader);
