import type { FC } from 'react';
import { memo } from 'react';

import {
  Box,
  IconButton,
  //   Button,
  ListItem,
  //   Pressable,
  Skeleton,
  Text,
  // Typography,
} from '@onekeyhq/components';
// import type { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { shortenAddress } from '@onekeyhq/components/src/utils';
import type { Network } from '@onekeyhq/engine/src/types/network';
import type { IHistoryTx } from '@onekeyhq/engine/src/vaults/types';

import { useNavigation } from '../../hooks';
import useFormatDate from '../../hooks/useFormatDate';
import {
  ModalRoutes,
  RootRoutes,
  TransactionDetailModalRoutes,
} from '../../routes/routesEnum';
import { getFeeInNativeText } from '../TxDetail/components/TxDetailExtraInfoBox';

import { ESolTxCellData, type SolListHeadTagType } from './Config';

import type { SolTransactionDetailRoutesParams } from '../../routes/Root/Modal/SolTransactionDetail';
import type { ModalScreenProps } from '../../routes/types';
import type { ESolHistoryTx } from '../Wallet/HistoricalRecords/useHistoricalRecordsData';

interface MarketTokenCellProps {
  //   onPress?: (marketTokenItem: MarketTokenItem) => void;
  //   onLongPress?: (marketTokenItem: MarketTokenItem) => void;
  //   marketTokenId: string;
  item: ESolHistoryTx & IHistoryTx;
  network: Network | null;
  headTags: SolListHeadTagType[];
}

// type NavigationProps = NativeStackNavigationProp<TabRoutesParams>;
export const LAMPORTS_PER_SOL = 0.000000001;
export type SolHistoryListViewNavigationProp =
  ModalScreenProps<SolTransactionDetailRoutesParams>;
const SolTrxListView: FC<MarketTokenCellProps> = ({
  headTags,
  item,
  network,
}) => {
  //   const marketTokenItem: MarketTokenItem | undefined = useMarketTokenItem({
  //     coingeckoId: marketTokenId,
  //     isList: true,
  //   });
  //   const moreButtonRef = useRef();
  const { formatDate } = useFormatDate();
  const navigation =
    useNavigation<SolHistoryListViewNavigationProp['navigation']>();

  return (
    <ListItem>
      {headTags.map((tag) => {
        switch (tag.id) {
          case ESolTxCellData.Signature: {
            return item &&
              (item.txHash !== undefined ||
                item.decodedTx.txid !== undefined) ? (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: (
                    <Box
                      flexDirection={{ base: 'row' }}
                      alignItems={{ base: 'center' }}
                    >
                      <IconButton
                        name="EyeOutline"
                        color="action-primary-default"
                        onPress={() => {
                          if (network?.impl.includes('sol')) {
                            return navigation.navigate(RootRoutes.Modal, {
                              screen: ModalRoutes.SolTransactionDetail,
                              params: {
                                screen:
                                  TransactionDetailModalRoutes.SolHistoryDetailModal,
                                params: {
                                  solTxHis: item,
                                },
                              },
                            });
                          }
                          return navigation.navigate(RootRoutes.Modal, {
                            screen: ModalRoutes.TransactionDetail,
                            params: {
                              screen:
                                TransactionDetailModalRoutes.HistoryDetailModal,
                              params: {
                                decodedTx: item?.decodedTx,
                              },
                            },
                          });
                        }}
                        p="1px"
                        mr={2}
                      />
                      <Text
                        textAlign="left"
                        fontWeight="500"
                        color="interactive-default"
                        wordBreak="break-all"
                      >
                        {shortenAddress(item.txHash || item.decodedTx.txid, 4)}
                      </Text>
                    </Box>
                  ),
                  labelProps: {
                    textAlign: tag.textAlign,
                    color: 'interactive-default',
                  },
                  size: 'sm',
                }}
                flex={1}
              />
            ) : (
              <ListItem.Column key={tag.id}>
                <Box
                  flex={1}
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Skeleton shape="Body2" />
                </Box>
              </ListItem.Column>
            );
          }
          case ESolTxCellData.Block: {
            return item &&
              (item.blockTime !== undefined ||
                item.decodedTx.createdAt !== undefined) ? (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: item.blockTime
                    ? formatDate(new Date(item.blockTime * 1000))
                    : item.decodedTx.createdAt &&
                      formatDate(new Date(item.decodedTx.createdAt)),
                  labelProps: { textAlign: tag.textAlign },
                  size: 'sm',
                }}
                flex={1}
              />
            ) : (
              <ListItem.Column key={tag.id}>
                <Box
                  flex={1}
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Skeleton shape="Body2" />
                </Box>
              </ListItem.Column>
            );
          }
          case ESolTxCellData.Instructions: {
            // eslint-disable-next-line no-nested-ternary
            const instruction = item?.change
              ? 'spl-transfer' // eslint-disable-next-line no-nested-ternary
              : item?.parsedInstruction?.length
              ? item?.parsedInstruction[0]?.type // eslint-disable-next-line no-nested-ternary
              : item?.dst
              ? 'token-transfer'
              : item?.decodedTx?.outputActions?.length &&
                item?.decodedTx?.outputActions[0]?.type === 'UNKNOWN'
              ? 'CONTRACT_INTERACTION'
              : item?.decodedTx?.outputActions?.length &&
                item?.decodedTx?.outputActions[0]?.type;
            return item ? (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: String(instruction).toUpperCase(),
                  labelProps: {
                    textAlign: tag.textAlign,
                    isTruncated: true,
                  },
                  size: 'sm',
                }}
                flex={1}
              />
            ) : (
              <ListItem.Column key={tag.id}>
                <Box
                  flex={1}
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Skeleton shape="Body2" />
                </Box>
              </ListItem.Column>
            );
          }
          case ESolTxCellData.By: {
            // eslint-disable-next-line no-nested-ternary
            const address = item?.signer?.length
              ? item?.signer[0]
              : item?.change
              ? item?.change?.owner
              : item?.src || item?.decodedTx?.signer;
            return item ? (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: address && (
                    <Text
                      textAlign="center"
                      fontWeight="500"
                      color="interactive-default"
                      wordBreak="break-all"
                    >
                      {shortenAddress(address, 4)}
                    </Text>
                  ),
                  labelProps: {
                    textAlign: tag.textAlign,
                    color: 'interactive-default',
                  },
                  size: 'sm',
                }}
                flex={1}
              />
            ) : (
              <ListItem.Column key={tag.id}>
                <Box
                  flex={1}
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Skeleton shape="Body2" />
                </Box>
              </ListItem.Column>
            );
          }
          case ESolTxCellData.Fee: {
            const fee = item?.decodedTx
              ? getFeeInNativeText({
                  network,
                  decodedTx: item.decodedTx,
                })
              : item?.fee && `${item.fee * LAMPORTS_PER_SOL} Sol`;
            return item ? (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: fee,
                  labelProps: { textAlign: tag.textAlign },
                  size: 'sm',
                }}
                flex={1}
              />
            ) : (
              <ListItem.Column key={tag.id}>
                <Box
                  flex={1}
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Skeleton shape="Body2" />
                </Box>
              </ListItem.Column>
            );
          }
          case ESolTxCellData.Status: {
            const statusColor =
              // eslint-disable-next-line no-nested-ternary
              item &&
              ['Success', 'Confirmed'].includes(
                item?.status || item?.decodedTx.status,
              )
                ? 'icon-success'
                : item?.status === 'Fail'
                ? 'focused-critical'
                : 'icon-warning';
            return item ? (
              <ListItem.Column
                key={tag.id}
                text={{
                  label: item.status || item.decodedTx.status,
                  labelProps: {
                    textAlign: tag.textAlign,
                    color: statusColor,
                  },
                  size: 'sm',
                }}
                flex={1}
              />
            ) : (
              <ListItem.Column key={tag.id}>
                <Box
                  flex={1}
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Skeleton shape="Body2" />
                </Box>
              </ListItem.Column>
            );
          }
          default:
            return null;
        }
      })}
    </ListItem>
  );
};

export default memo(SolTrxListView);
