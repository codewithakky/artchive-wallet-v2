export const SOLTX_FAKE_SKELETON_LIST_ARRAY = new Array(10).fill(null);

export type SolListHeadTagType = {
  id: ESolTxCellData;
  title?: string;
  minW: string;
  textAlign?: 'center' | 'left' | 'right';
  showVerticalLayout?: boolean;
  showNorMalDevice?: boolean;
  dislocation?: { id: ESolTxCellData; title: string };
};

export enum ESolTxCellData {
  Signature = 'Signature',
  Block = 'Block',
  Status = 'Status',
  Instructions = 'Instructions',
  By = 'By',
  Fee = 'Fee',
}

export const SolTxListHeadTags: SolListHeadTagType[] = [
  {
    id: ESolTxCellData.Signature,
    title: 'SIGNATURE',
    minW: '100px',
    textAlign: 'center',
    showVerticalLayout: false,
  },
  {
    id: ESolTxCellData.Block,
    title: 'BLOCK AGE',
    minW: '100px',
    textAlign: 'center',
    showVerticalLayout: false,
  },
  {
    id: ESolTxCellData.Instructions,
    title: 'INSTRUCTIIONS',
    minW: '100px',
    textAlign: 'center',
    showVerticalLayout: true,
    showNorMalDevice: true,
  },
  {
    id: ESolTxCellData.By,
    title: 'BY',
    minW: '100px',
    textAlign: 'center',
    showVerticalLayout: true,
    showNorMalDevice: true,
  },
  {
    id: ESolTxCellData.Fee,
    title: 'FEE',
    minW: '100px',
    textAlign: 'center',
    showVerticalLayout: true,
    showNorMalDevice: true,
  },
  {
    id: ESolTxCellData.Status,
    title: 'STATUS',
    minW: '100px',
    textAlign: 'center',
    showVerticalLayout: true,
    showNorMalDevice: true,
  },
];

export const TransactionStatus = {
  Success: {
    iconColor: 'icon-success',
    textColor: 'text-success',
    text: 'Success',
  },
  Pending: {
    iconColor: 'icon-warning',
    textColor: 'text-warning',
    text: 'Pending',
  },
  Fail: {
    iconColor: 'icon-critical',
    textColor: 'text-critical',
    text: 'Fail',
  },
} as const;
