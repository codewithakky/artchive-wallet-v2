import type { FC } from 'react';

import { useRoute } from '@react-navigation/core';
import { useIntl } from 'react-intl';

import { Modal, Spinner } from '@onekeyhq/components';

import { TxActionElementTime } from '../TxDetail/elements/TxActionElementTime';
import { SolTxDetailView } from '../TxDetail/SolTxDetailView';

import type { SolTransactionDetailRoutesParams } from '../../routes/Root/Modal/SolTransactionDetail';
import type { TransactionDetailModalRoutes } from '../../routes/routesEnum';
import type { RouteProp } from '@react-navigation/native';

type TransactionDetailRouteProp = RouteProp<
  SolTransactionDetailRoutesParams,
  TransactionDetailModalRoutes.SolHistoryDetailModal
>;

/* TODO status ICON
    switch (status) {
      case TxStatus.Pending:
        statusTitle = 'transaction__pending';
        statusIconName = 'DotsCircleHorizontalOutline';
        iconColor = 'icon-warning';
        textColor = 'text-warning';
        iconContainerColor = 'surface-warning-default';
        break;
      case TxStatus.Confirmed:
        statusTitle = 'transaction__success';
        statusIconName = 'CheckCircleOutline';
        iconColor = 'icon-success';
        textColor = 'text-success';
        iconContainerColor = 'surface-success-default';
        break;
      case TxStatus.Dropped:
        statusTitle = 'transaction__dropped';
        break;
      default:
        break;
    }
 */

const SolTxHistoryDetailModal: FC = () => {
  const route = useRoute<TransactionDetailRouteProp>();
  const { solTxHis } = route.params;
  const intl = useIntl();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const headerDescription = (
    <TxActionElementTime
      timestamp={
        solTxHis.blockTime !== undefined ? solTxHis.blockTime * 1000 : 0
      }
    />
  );
  return (
    <Modal
      header={intl.formatMessage({ id: 'transaction__transaction_details' })}
      headerDescription={headerDescription}
      footer={null}
      scrollViewProps={{
        children: solTxHis ? (
          <SolTxDetailView item={route.params.solTxHis} />
        ) : (
          <Spinner />
        ),
      }}
    />
  );
};

export { SolTxHistoryDetailModal };
