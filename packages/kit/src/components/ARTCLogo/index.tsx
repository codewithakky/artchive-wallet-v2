import { Pressable, Tooltip, useTheme } from '@onekeyhq/components';

import { useNavigation } from '../../hooks';
import { RootRoutes } from '../../routes/routesEnum';
import {
  ARTC_DARK,
  ARTC_LIGHT,
  ArtchiveIcon,
  WelcomeBannerShow,
  WelcomeBannerShowDark,
} from '../../views/constants';

type Props = {
  size?: number;
};

function ARTCLogo({ size = 100 }: Props) {
  const { themeVariant } = useTheme();
  const navigation = useNavigation();
  const logo = themeVariant === 'light' ? ARTC_DARK : ARTC_LIGHT;
  return (
    <Tooltip hasArrow placement="bottom right" label="Artchive Home">
      <Pressable onPress={() => navigation.navigate(RootRoutes.Onboarding)}>
        <img width={size} alt="Artchive Logo" src={logo} />
      </Pressable>
    </Tooltip>
  );
}
export function BannerImage({ size = 100 }: Props) {
  const { themeVariant } = useTheme();
  const image =
    themeVariant === 'light' ? WelcomeBannerShowDark : WelcomeBannerShow;
  return <img alt="banner" width="100%" height="100%" src={image} />;
}
export function ArtcIcon({ size = 100 }: Props) {
  // const { themeVariant } = useTheme();
  const image = ArtchiveIcon;
  return <img alt="logo" width="100%" src={image} />;
}
export default ARTCLogo;
