import { useMemo } from 'react';

import {
  CustomSkeleton,
  HStack,
  // Icon,
  Text,
  // Tooltip,
} from '@onekeyhq/components';
import { implToChainSimplied } from '@onekeyhq/engine/src/managers/impl';

import { useActiveWalletAccount } from '../../hooks';
import {
  getChainNetorkEnvironment,
  useRpcMeasureStatus,
} from '../../views/ManageNetworks/hooks';

import { useImplChainId } from './hooks/useImplChainId';
import Speedindicator from './modals/NetworkAccountSelectorModal/SpeedIndicator';

interface IRpcStatusButtonProps {
  networkId: string;
  isTestNetwork?: boolean;
  rpcUrl?: string | undefined;
}
function RpcStatusButton({ networkId, rpcUrl }: IRpcStatusButtonProps) {
  const { loading, status } = useRpcMeasureStatus(networkId || '');
  const implChainId = useImplChainId();
  const { network } = useActiveWalletAccount();

  const rpcStatusElement = useMemo(() => {
    if (!status || loading || !implChainId) {
      return (
        <HStack mx={2}>
          <CustomSkeleton borderRadius="3px" height="10px" width="32px" />
        </HStack>
      );
    }
    return (
      <>
        <Speedindicator
          mx="8px"
          mr="8px"
          borderWidth="0"
          backgroundColor={status.iconColor}
        />
        <Text
          typography="Body2Strong"
          isTruncated
          color={status.textColor}
          mr="4px"
        >
          {!rpcUrl?.includes('solana')
            ? String(implToChainSimplied[implChainId]).toUpperCase()
            : getChainNetorkEnvironment(network)}
          {/* {`${getChainNewtork(rpcUrl || '')} | ${
            !rpcUrl?.includes('solana')
              ? String(implToChainSimplied[implChainId]).toUpperCase()
              : ''
          }`} */}
        </Text>
      </>
    );
  }, [status, loading, implChainId, rpcUrl, network]);

  return <>{rpcStatusElement}</>;
}

export { RpcStatusButton };
