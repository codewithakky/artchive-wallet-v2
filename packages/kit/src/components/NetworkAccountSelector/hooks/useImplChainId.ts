import { useCallback, useEffect, useState } from 'react';

import backgroundApiProxy from '../../../background/instance/backgroundApiProxy';
import { useActiveWalletAccount } from '../../../hooks';

export function useImplChainId() {
  const [implChainId, setImplChainId] = useState<string>();
  const { serviceToken } = backgroundApiProxy;
  const { networkId } = useActiveWalletAccount();
  const getChainIdImpl = useCallback(async (): Promise<string> => {
    if (!networkId) return 'evm--1';
    const ChainId = await serviceToken.getChainIdImpl(networkId);
    return ChainId;
  }, [networkId, serviceToken]);

  useEffect(() => {
    async function callSelf() {
      const implChain = await getChainIdImpl();
      setImplChainId(implChain);
      return implChain;
    }
    callSelf();
  }, [getChainIdImpl]);
  return implChainId;
}
