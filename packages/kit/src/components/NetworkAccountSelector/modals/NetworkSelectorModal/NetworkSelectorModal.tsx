/* eslint-disable @typescript-eslint/ban-types */

import { useNavigation, useRoute } from '@react-navigation/core';
import { useIntl } from 'react-intl';

import {
  Box,
  IconButton,
  Modal,
  useIsVerticalLayout,
} from '@onekeyhq/components';
import type { Network } from '@onekeyhq/engine/src/types/network';

import useAppNavigation from '../../../../hooks/useAppNavigation';
import {
  ManageNetworkModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../../routes/routesEnum';
import { LazyDisplayView } from '../../../LazyDisplayView';
import { useAccountSelectorChangeAccountOnPress } from '../../hooks/useAccountSelectorChangeAccountOnPress';
import { useAccountSelectorModalInfo } from '../../hooks/useAccountSelectorModalInfo';
import SideChainSelector from '../NetworkAccountSelectorModal/SideChainSelector';

import type { NavigationProps } from '../../../../views/ManageNetworks/Listing';
import type { ManageNetworkRoutesParams } from '../../../../views/ManageNetworks/types';
import type { RouteProp } from '@react-navigation/core';

type RouteProps = RouteProp<
  ManageNetworkRoutesParams,
  ManageNetworkModalRoutes.NetworkSelector
>;

function NetworkSelectorModal() {
  const intl = useIntl();
  const { onPressChangeAccount } = useAccountSelectorChangeAccountOnPress();
  const navigation = useAppNavigation();
  const customChainNav = useNavigation<NavigationProps>();
  const route = useRoute<RouteProps>();
  const isSmallScreen = useIsVerticalLayout();
  const params = route?.params ?? {};

  const {
    networkImpl,
    onSelected,
    selectedNetworkId,
    selectableNetworks,
    sortDisabled,
    // customDisabled,
    rpcStatusDisabled,
  } = params;
  const { accountSelectorInfo, shouldShowModal } =
    useAccountSelectorModalInfo();
  if (!shouldShowModal) {
    return null;
  }

  const customizeChain = () =>
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManageNetwork,
      params: { screen: ManageNetworkModalRoutes.Listing },
    });

  const addCustomChain = (network?: Network, mode: 'edit' | 'add' = 'add') => {
    customChainNav.navigate(ManageNetworkModalRoutes.AddNetwork, {
      network,
      mode,
    });
  };
  return (
    <Modal
      header={intl.formatMessage({ id: 'network__networks' })}
      // footer={null}
      staticChildrenProps={{
        flex: 1,
        padding: 0,
      }}
      height="560px"
      primaryActionProps={{
        type: 'primary',
        w: isSmallScreen ? 'full' : undefined,
      }}
      secondaryActionProps={{
        type: 'primary',
        w: isSmallScreen ? 'full' : undefined,
      }}
      primaryActionTranslationId="action__add_network"
      onPrimaryActionPress={() => addCustomChain()}
      secondaryActionTranslationId="action__customize_network"
      onSecondaryActionPress={() => customizeChain()}
      rightContent={
        <>
          {sortDisabled ? null : (
            <IconButton
              type="plain"
              size="lg"
              circle
              name="BarsArrowUpOutline"
              onPress={() => {
                navigation.navigate(RootRoutes.Modal, {
                  screen: ModalRoutes.ManageNetwork,
                  params: { screen: ManageNetworkModalRoutes.Sort },
                });
              }}
            />
          )}
          {/* Commenting as of now */}
          {/* {customDisabled ? null : (
            <IconButton
              type="plain"
              size="lg"
              circle
              name="PlusCircleOutline"
              onPress={() => {
                navigation.navigate(RootRoutes.Modal, {
                  screen: ModalRoutes.ManageNetwork,
                  params: { screen: ManageNetworkModalRoutes.Listing },
                });
              }}
            />
          )} */}
        </>
      }
    >
      <LazyDisplayView delay={0}>
        <Box flex={1} flexDirection="row">
          <SideChainSelector
            networkImpl={networkImpl}
            fullWidthMode // should be fullWidthMode here
            rpcStatusDisabled={rpcStatusDisabled}
            accountSelectorInfo={accountSelectorInfo}
            selectedNetworkId={selectedNetworkId}
            selectableNetworks={selectableNetworks}
            onPress={async ({ networkId }) => {
              if (onSelected) {
                onSelected(networkId);
              } else {
                await onPressChangeAccount({
                  networkId,
                  accountSelectorMode: accountSelectorInfo.accountSelectorMode,
                });
              }
            }}
          />
        </Box>
      </LazyDisplayView>
    </Modal>
  );
}

export { NetworkSelectorModal };
