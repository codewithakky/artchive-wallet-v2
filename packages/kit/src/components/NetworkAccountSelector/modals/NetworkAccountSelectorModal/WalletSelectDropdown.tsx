/* eslint-disable no-nested-ternary */
import type { Dispatch, SetStateAction } from 'react';
import { useCallback, useEffect, useMemo, useState } from 'react';

import { debounce } from 'lodash';
import { useIntl } from 'react-intl';

import {
  Box,
  Icon,
  IconButton,
  Pressable,
  Select,
  Text,
  useIsVerticalLayout,
} from '@onekeyhq/components';
import useModalClose from '@onekeyhq/components/src/Modal/Container/useModalClose';
import type { IWallet } from '@onekeyhq/engine/src/types';
import debugLogger from '@onekeyhq/shared/src/logger/debugLogger';

import backgroundApiProxy from '../../../../background/instance/backgroundApiProxy';
import { useNavigation } from '../../../../hooks';
import { useActiveWalletAccount, useRuntime } from '../../../../hooks/redux';
import { useIsMounted } from '../../../../hooks/useIsMounted';
import { getWalletName } from '../../../../hooks/useWalletName';
import {
  CreateAccountModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../../routes/routesEnum';
import { WalletAvatarPro } from '../../../WalletSelector/WalletAvatar';

import { CreateAccountButton } from './CreateAccountButton';

import type { CreateAccountRoutesParams } from '../../../../routes';
import type { ModalScreenProps } from '../../../../routes/types';
import type { useAccountSelectorInfo } from '../../hooks/useAccountSelectorInfo';
import type { IntlShape } from 'react-intl';

const buildData = debounce(
  ({
    setData,
    wallets,
    intl,
  }: {
    intl: IntlShape;
    wallets: IWallet[];
    setData: Dispatch<
      SetStateAction<{ label: string; value: string; wallet: IWallet }[]>
    >;
  }) => {
    const data = wallets.map((wallet) => ({
      label: getWalletName({ wallet, intl }) || '-',
      value: wallet.id,
      wallet,
    }));
    debugLogger.accountSelector.info(
      'rebuild NetworkAccountSelector walletList data',
    );
    setData(data);
  },
  150,
  {
    leading: false,
    trailing: true,
  },
);
type NavigationProps = ModalScreenProps<CreateAccountRoutesParams>;

export function WalletSelectDropdown({
  accountSelectorInfo,
}: {
  accountSelectorInfo: ReturnType<typeof useAccountSelectorInfo>;
}) {
  const {
    selectedNetworkId,
    selectedWallet,
    selectedWalletId,
    isOpenDelay,
    isOpen,
    preloadingCreateAccount,
  } = accountSelectorInfo;
  const { serviceAccountSelector } = backgroundApiProxy;
  const navigationAcc = useNavigation<NavigationProps['navigation']>();
  const intl = useIntl();
  const isVerticalLayout = useIsVerticalLayout();
  const { wallets } = useRuntime();
  const [data, setData] = useState<
    { label: string; value: string; wallet: IWallet }[]
  >([]);
  const isMounted = useIsMounted();
  const { walletId, networkId } = useActiveWalletAccount();
  // const deviceId = wallet.associatedDevice || '';
  const { serviceAccount } = backgroundApiProxy;
  const closeModal = useModalClose();

  useEffect(() => {
    if (isMounted.current && isOpenDelay && isOpen) {
      buildData({
        wallets,
        setData,
        intl,
      });
    }
  }, [intl, isOpenDelay, isOpen, wallets, isMounted]);

  const isPreloadingCreate = useMemo(
    () =>
      Boolean(
        preloadingCreateAccount?.walletId &&
          preloadingCreateAccount?.networkId &&
          preloadingCreateAccount?.walletId === selectedWalletId &&
          preloadingCreateAccount?.networkId === selectedNetworkId,
      ),
    [
      preloadingCreateAccount?.networkId,
      preloadingCreateAccount?.walletId,
      selectedNetworkId,
      selectedWalletId,
    ],
  );

  const onPressManageAccount = useCallback(() => {
    closeModal();
    setTimeout(() => {
      navigationAcc.navigate(RootRoutes.Modal, {
        screen: ModalRoutes.CreateAccount,
        params: {
          screen: CreateAccountModalRoutes.CreateAccountAuthentication,
          params: {
            walletId: walletId ?? '',
            onDone: (password) => {
              setTimeout(() => {
                navigationAcc.replace(
                  CreateAccountModalRoutes.RecoverAccountsList as any,
                  {
                    walletId,
                    network: networkId,
                    password,
                    purpose: '',
                    template: '',
                  },
                );
              }, 20);
            },
          },
        },
      });
    });
  }, [navigationAcc, walletId, networkId, closeModal]);

  return (
    <>
      <Select
        title={intl.formatMessage({ id: 'title__wallets' })}
        footer={null}
        value={selectedWalletId}
        activatable={false}
        containerProps={{
          flex: 1,
          alignItems: 'flex-start',
        }}
        options={data}
        renderTrigger={({ visible, onPress }) => (
          <Pressable onPress={onPress}>
            {({ isHovered, isPressed }) => (
              <Box
                flexDirection="row"
                alignItems="center"
                maxW="240px"
                p={2}
                rounded="xl"
                bgColor={
                  visible
                    ? 'surface-selected'
                    : isPressed
                    ? 'surface-pressed'
                    : isHovered
                    ? 'surface-hovered'
                    : undefined
                }
              >
                {selectedWallet ? (
                  <WalletAvatarPro
                    wallet={selectedWallet}
                    size="xs"
                    devicesStatus={undefined}
                  />
                ) : null}
                <Text
                  typography="Body2Strong"
                  ml={2}
                  mr={1}
                  color="text-subdued"
                  isTruncated
                >
                  {getWalletName({
                    wallet: selectedWallet,
                    intl,
                  })}
                </Text>
                <Box>
                  <Icon
                    name="ChevronUpDownMini"
                    color="icon-subdued"
                    size={20}
                  />
                </Box>
              </Box>
            )}
          </Pressable>
        )}
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        renderItem={(item, isActive, onChange) => (
          <Pressable
            key={item.value}
            onPress={async () => {
              // call internal select onChange to make sure selector closed
              onChange?.(item.value, item);
              await serviceAccountSelector.updateSelectedWallet(item.value);
              await serviceAccount.autoChangeAccount({
                walletId: item.value ?? '',
                skipIfSameWallet: true,
              });
              if (isVerticalLayout) {
                closeModal();
              }
            }}
          >
            {({ isHovered, isPressed }) => (
              <Box
                p={2}
                pr={{ base: 3, md: 2 }}
                flexDirection="row"
                alignItems="center"
                borderWidth={isActive ? 2 : 0}
                borderColor={isActive ? 'interactive-default' : 'none'}
                bgColor={
                  isActive
                    ? 'surface-highlight-default'
                    : isPressed
                    ? 'surface-pressed'
                    : isHovered
                    ? 'surface-hovered'
                    : undefined
                }
                rounded="xl"
              >
                {
                  // @ts-expect-error
                  item.wallet ? (
                    <WalletAvatarPro
                      // @ts-expect-error
                      wallet={item.wallet}
                      devicesStatus={undefined}
                      size={isVerticalLayout ? 'lg' : 'xs'}
                    />
                  ) : null
                }

                <Text
                  typography={{ sm: 'Body1Strong', md: 'Body2Strong' }}
                  flex={1}
                  mx={3}
                >
                  {item.label}
                </Text>
                {isActive ? (
                  <>
                    <Icon
                      name="CheckCircleSolid"
                      size={isVerticalLayout ? 24 : 20}
                      color="interactive-default"
                    />
                    <Pressable
                      rounded="full"
                      _hover={{ bg: 'surface-hovered' }}
                      _pressed={{ bg: 'surface-pressed' }}
                      ml={2}
                    >
                      <IconButton
                        name="EllipsisVerticalSolid"
                        type="plain"
                        size={isVerticalLayout ? 'sm' : 'xs'}
                        onPress={onPressManageAccount}
                        circle
                      />
                    </Pressable>
                  </>
                ) : null}
              </Box>
            )}
          </Pressable>
        )}
      />
      <CreateAccountButton
        walletId={selectedWalletId || ''}
        networkId={selectedNetworkId}
        isLoading={isPreloadingCreate}
      />
    </>
  );
}
