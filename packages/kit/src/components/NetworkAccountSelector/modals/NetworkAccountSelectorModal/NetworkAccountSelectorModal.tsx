/* eslint-disable @typescript-eslint/ban-types */
import { useState } from 'react';

import {
  type RouteProp,
  useNavigation,
  useRoute,
} from '@react-navigation/core';
import { useIntl } from 'react-intl';

import {
  Box,
  // Button,
  HStack,
  Image,
  Modal,
  // Pressable,
  // Pressable,
  ScrollView,
  Searchbar,
  // Text,
  Typography,
  useIsVerticalLayout,
} from '@onekeyhq/components';
import type { INetwork } from '@onekeyhq/engine/src/types';
import type { Network } from '@onekeyhq/engine/src/types/network';
import { WALLET_TYPE_HW } from '@onekeyhq/engine/src/types/wallet';

import {
  ManageNetworkModalRoutes,
  ModalRoutes,
  RootRoutes,
} from '../../../../routes/routesEnum';
import { LazyDisplayView } from '../../../LazyDisplayView';
import { useAccountSelectorChangeAccountOnPress } from '../../hooks/useAccountSelectorChangeAccountOnPress';
import { useAccountSelectorModalInfo } from '../../hooks/useAccountSelectorModalInfo';
import { RpcStatusButton } from '../../RpcStatusButton';

import AccountList from './AccountList';
import Header from './Header';
import { NetWorkExtraInfo } from './NetworkExtraInfo';
import SideChainSelector from './SideChainSelector';

import type { ManageNetworkRoutesParams } from '../../../../routes';
import type { useAccountSelectorInfo } from '../../hooks/useAccountSelectorInfo';

type RouteProps = RouteProp<
  ManageNetworkRoutesParams,
  ManageNetworkModalRoutes.NetworkAccountSelector
>;

function LazyDisplayContentView({
  // showSideChainSelector,
  // showCustomLegacyHeader,
  networkImpl,
  onSelected,
  selectedNetworkId,
  selectableNetworks,
  // sortDisabled,
  // customDisabled,
  rpcStatusDisabled,
  accountSelectorInfo,
}: {
  // showSideChainSelector: boolean;
  // showCustomLegacyHeader: boolean;
  networkImpl?: string;
  onSelected?: (networkId: string) => void;
  selectableNetworks?: INetwork[];
  sortDisabled?: boolean;
  customDisabled?: boolean;
  rpcStatusDisabled?: boolean;
  selectedNetworkId?: string;
  accountSelectorInfo: ReturnType<typeof useAccountSelectorInfo>;
}) {
  const [search, setSearch] = useState('');
  const { onPressChangeAccount } = useAccountSelectorChangeAccountOnPress();

  return (
    <Box flex={1} flexDirection="row">
      <SideChainSelector
        networkImpl={networkImpl}
        fullWidthMode // should be fullWidthMode here
        rpcStatusDisabled={rpcStatusDisabled}
        accountSelectorInfo={accountSelectorInfo}
        selectedNetworkId={selectedNetworkId}
        selectableNetworks={selectableNetworks}
        onPress={async ({ networkId }) => {
          if (onSelected) {
            onSelected(networkId);
          } else {
            await onPressChangeAccount({
              networkId,
              accountSelectorMode: accountSelectorInfo.accountSelectorMode,
              fromDesktopSideBar: true,
            });
          }
        }}
      />
      <Box alignSelf="stretch" flex={1}>
        <Header accountSelectorInfo={accountSelectorInfo} />
        <Box px={{ base: 4, md: 6 }} mb="16px">
          <Searchbar
            w="full"
            value={search}
            onChangeText={setSearch}
            onClear={() => setSearch('')}
          />
        </Box>
        <ScrollView>
          <AccountList
            accountSelectorInfo={accountSelectorInfo}
            searchValue={search}
          />
          <NetWorkExtraInfo
            accountId={accountSelectorInfo.activeAccount?.id}
            networkId={accountSelectorInfo.selectedNetworkId}
          />
        </ScrollView>
      </Box>
    </Box>
  );
}
function NetworkAccountSelectorModal() {
  const intl = useIntl();
  const route = useRoute<RouteProps>();
  const navigation = useNavigation();
  const isSmallScreen = useIsVerticalLayout();
  const params = route?.params ?? {};

  const {
    networkImpl,
    onSelected,
    selectedNetworkId,
    selectableNetworks,
    sortDisabled,
    rpcStatusDisabled,
  } = params;

  const { accountSelectorInfo, shouldShowModal } =
    useAccountSelectorModalInfo();

  const isDisabled =
    accountSelectorInfo?.activeWallet?.type === WALLET_TYPE_HW &&
    !accountSelectorInfo?.activeNetwork?.settings.hardwareAccountEnabled;

  const customizeChain = () =>
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManageNetwork,
      params: { screen: ManageNetworkModalRoutes.Listing },
    });

  const addCustomChain = (network?: Network, mode: 'edit' | 'add' = 'add') => {
    navigation.navigate(RootRoutes.Modal, {
      screen: ModalRoutes.ManageNetwork,
      params: {
        screen: ManageNetworkModalRoutes.AddNetwork,
        params: {
          network,
          mode,
        },
      },
    });
  };

  if (!shouldShowModal) {
    return null;
  }

  return (
    <Modal
      // headerShown={!showCustomLegacyHeader}
      header={intl.formatMessage({ id: 'title__accounts' })}
      headerDescription={
        <HStack space={1} alignItems="center">
          {accountSelectorInfo?.selectedNetwork?.logoURI ? (
            <Image
              source={{ uri: accountSelectorInfo?.selectedNetwork?.logoURI }}
              size={4}
              borderRadius="full"
              mr={2}
            />
          ) : null}
          <Typography.Caption color="text-subdued">
            {accountSelectorInfo?.selectedNetwork?.name || '-'}
          </Typography.Caption>
          {accountSelectorInfo?.selectedNetwork?.id ===
            accountSelectorInfo?.activeNetwork?.id &&
            !rpcStatusDisabled &&
            !isDisabled && (
              <RpcStatusButton
                isTestNetwork={accountSelectorInfo?.selectedNetwork?.isTestnet}
                networkId={accountSelectorInfo?.selectedNetwork?.id ?? ''}
                rpcUrl={accountSelectorInfo?.selectedNetwork?.rpcURL}
              />
            )}
        </HStack>
      }
      staticChildrenProps={{
        flex: 1,
        padding: 0,
      }}
      height="560px"
      primaryActionProps={{
        type: 'primary',
        w: isSmallScreen ? 'full' : undefined,
      }}
      secondaryActionProps={{
        type: 'primary',
        w: isSmallScreen ? 'full' : undefined,
      }}
      primaryActionTranslationId="action__add_network"
      onPrimaryActionPress={() => addCustomChain()}
      secondaryActionTranslationId="action__customize_network"
      onSecondaryActionPress={() => customizeChain()}
      size="lg"
    >
      <LazyDisplayView delay={0}>
        <LazyDisplayContentView
          networkImpl={networkImpl}
          onSelected={onSelected}
          selectedNetworkId={selectedNetworkId}
          selectableNetworks={selectableNetworks}
          sortDisabled={sortDisabled}
          // customDisabled,
          rpcStatusDisabled={rpcStatusDisabled}
          accountSelectorInfo={accountSelectorInfo}
          // showSideChainSelector={showSideChainSelector}
          // showCustomLegacyHeader={showCustomLegacyHeader}
        />
      </LazyDisplayView>
    </Modal>
  );
}

export { NetworkAccountSelectorModal };
