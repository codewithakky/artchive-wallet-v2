import type { FC } from 'react';
import { useMemo } from 'react';

import { useIntl } from 'react-intl';

import { Box, Token } from '@onekeyhq/components';

import {
  useActiveWalletAccount,
  // useAppSelector,
  useNavigationActions,
} from '../../../hooks';
// import ExternalAccountImg from '../../../views/ExternalAccount/components/ExternalAccountImg';
import { useRpcMeasureStatus } from '../../../views/ManageNetworks/hooks';
import Speedindicator from '../modals/NetworkAccountSelectorModal/SpeedIndicator';

import { BaseSelectorTrigger } from './BaseSelectorTrigger';

import type { INetworkAccountSelectorTriggerProps } from './BaseSelectorTrigger';

interface AccountSelectorTriggerProps
  extends INetworkAccountSelectorTriggerProps {
  showAddress?: boolean;
}

const AccountSelectorTrigger: FC<AccountSelectorTriggerProps> = ({
  showAddress = true,
  type = 'plain',
  bg,
  mode,
}) => {
  const { account, network } = useActiveWalletAccount();
  const { openAccountSelector } = useNavigationActions();
  const { status: rpcStatus, loading } = useRpcMeasureStatus(network?.id ?? '');
  // const activeExternalWalletName = useAppSelector(
  //   (s) => s.general.activeExternalWalletName,
  // );
  // const generalizeBTCAccount = account?.name?.includes('BTC')
  //   ? 'BTC'
  //   : account?.name;

  const intl = useIntl();
  const activeOption = useMemo(
    () => ({
      label:
        network?.name || intl.formatMessage({ id: 'empty__no_account_title' }),
      description:
        (account?.displayAddress || account?.address || '').slice(-4) || '',
      value: account?.id,
      tokenProps: {
        token: {
          logoURI: network?.logoURI,
          name: network?.shortName,
        },
      },
    }),
    [
      account?.displayAddress,
      account?.address,
      account?.id,
      intl,
      // generalizeBTCAccount,
      network,
    ],
  );
  // const isVertical = useIsVerticalLayout();

  return (
    <BaseSelectorTrigger
      type={type}
      bg={bg}
      icon={
        <Box position="relative">
          <Token size={6} {...activeOption.tokenProps} />
          {rpcStatus && !loading && (
            <Speedindicator
              position="absolute"
              top={-1}
              right={-1}
              size="10px"
              backgroundColor={rpcStatus?.iconColor}
            />
          )}
        </Box>
      }
      // icon={
      //   // eslint-disable-next-line no-nested-ternary
      //   isVertical ? null : account?.id ? (
      //     <ExternalAccountImg
      //       accountId={account?.id}
      //       walletName={activeExternalWalletName}
      //     />
      //   ) : null
      // }
      label={activeOption.label}
      description={showAddress && `...${activeOption.description}`}
      onPress={() => {
        openAccountSelector({ mode });
      }}
    />
  );
};

export { AccountSelectorTrigger };
