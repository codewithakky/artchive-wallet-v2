/* eslint-disable @typescript-eslint/no-unsafe-return */
import type { FC, ReactNode } from 'react';
import {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  // useRef,
  useState,
} from 'react';

import { CommonActions } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { AnimatePresence, MotiView } from 'moti';
import { Image } from 'native-base';
import { useIntl } from 'react-intl';

import {
  Center,
  Divider,
  IconButton,
  Text,
  Tooltip,
  useThemeValue,
} from '@onekeyhq/components';
import {
  walletIsHD,
  // walletIsHW,
  walletIsImported,
} from '@onekeyhq/engine/src/managers/wallet';
import type { Account } from '@onekeyhq/engine/src/types/account';
import backgroundApiProxy from '@onekeyhq/kit/src/background/instance/backgroundApiProxy';
import { useAccountSelectorChangeAccountOnPress } from '@onekeyhq/kit/src/components/NetworkAccountSelector/hooks/useAccountSelectorChangeAccountOnPress';
import Speedindicator from '@onekeyhq/kit/src/components/NetworkAccountSelector/modals/NetworkAccountSelectorModal/SpeedIndicator';
import WalletSelectorTrigger from '@onekeyhq/kit/src/components/WalletSelector/WalletSelectorTrigger/WalletSelectorTrigger';
import {
  useActiveWalletAccount,
  useAppSelector,
} from '@onekeyhq/kit/src/hooks';
import { setHomeTabName } from '@onekeyhq/kit/src/store/reducers/status';
import DesktopSidebarMoreMenu from '@onekeyhq/kit/src/views/Overlay/DesktopSidebarMoreMenu';
import { WalletHomeTabEnum } from '@onekeyhq/kit/src/views/Wallet/type';
import platformEnv from '@onekeyhq/shared/src/platformEnv';

import Box from '../../Box';
import { DesktopDragZoneAbsoluteBar } from '../../DesktopDragZoneBox';
import Icon from '../../Icon';
import Pressable from '../../Pressable';
import { Context } from '../../Provider/hooks/useProviderValue';
import useSafeAreaInsets from '../../Provider/hooks/useSafeAreaInsets';
import ScrollView from '../../ScrollView';
import Typography from '../../Typography';
import { shortenAddress } from '../../utils';
import VStack from '../../VStack';

import type { ICON_NAMES } from '../../Icon';
import type { BottomTabBarProps } from '../BottomTabs';
import type { Dictionary } from 'lodash';

export const derivedChainFromCoinType = (coinType: string) => {
  switch (coinType) {
    case '0':
      return {
        name: 'Bitcoin',
        logo: 'https://onekey-asset.com/assets/btc/btc.png',
        networkId: 'btc--0',
        keyChain: '0',
      };
    case '1':
      return {
        name: 'Ethereum',
        logo: 'https://onekey-asset.com/assets/eth/eth.png',
        networkId: 'evm--1',
        keyChain: '1',
      };
    case '3':
      return {
        name: 'DogeCoin',
        logo: 'https://onekey-asset.com/assets/doge/doge.png',
        networkId: 'doge--0',
        keyChain: '3',
      };
    case '56':
      return {
        name: 'BNB Smart Chain',
        logo: 'https://onekey-asset.com/assets/bsc/bsc.png',
        networkId: 'evm--56',
        keyChain: '56',
      };
    case '137':
      return {
        name: 'Polygon',
        logo: 'https://onekey-asset.com/assets/polygon/polygon.png',
        networkId: 'evm--137',
        keyChain: '137',
      };
    case '144':
      return {
        name: 'Ripple',
        logo: 'https://common.onekey-asset.com/chain/xrp.png',
        networkId: 'xrp--0',
        keyChain: '144',
      };
    case '195':
      return {
        name: 'Tron',
        logo: 'https://onekey-asset.com/assets/trx/trx.png',
        networkId: 'tron--0x2b6653dc',
        keyChain: '195',
      };
    case '354':
      return {
        name: 'Polkadot',
        logo: 'https://onekey-asset.com/assets/polkadot/polkadot.png',
        networkId: 'dot--polkadot',
      };
    case '501':
      return {
        name: 'Solana',
        logo: 'https://onekey-asset.com/assets/sol/sol.png',
        networkId: 'sol--101',
      };
    case '1815':
      return {
        name: 'Cardano',
        logo: 'https://onekey-asset.com/assets/ada/ada.png',
        networkId: 'ada--0',
        keyChain: '1815',
      };
    case '43114':
      return {
        name: 'Avalanche',
        logo: 'https://onekey-asset.com/assets/avalanche/avalanche.png',
        networkId: 'evm--43114',
        keyChain: '43114',
      };
    default:
      return null;
  }
};
const Sidebar: FC<BottomTabBarProps> = ({ navigation, state, descriptors }) => {
  const { routes } = state;
  const {
    leftSidebarCollapsed: isCollpase,
    setLeftSidebarCollapsed: setIsCollapse,
  } = useContext(Context);
  const { top } = useSafeAreaInsets(); // used for ipad
  const dragZoneAbsoluteBarHeight = platformEnv.isDesktopMac ? 20 : 0; // used for desktop
  const paddingTopValue = 12 + top + dragZoneAbsoluteBarHeight;
  const [openAccord, setOpenAccord] = useState(false);
  const [openAccord2, setOpenAccord2] = useState({
    isPanelShow: false,
    activePanel: -1,
  });
  const [consolidatedWallet, setConsolidatedWallet] = useState<
    Dictionary<Account[]>
  >({});
  const { wallet, walletId, accountAddress, networkId } =
    useActiveWalletAccount();
  const { onPressChangeAccount } = useAccountSelectorChangeAccountOnPress();
  const accountSelectorMode = useAppSelector(
    (s) => s.accountSelector.accountSelectorMode,
  );
  const intl = useIntl();
  const { engine } = backgroundApiProxy;
  // console.log(wallet?.accounts);

  useEffect(() => {
    engine.getConsolidatedWallet(wallet?.accounts || []).then((res) => {
      setConsolidatedWallet(res);
    });
  }, [engine, wallet]);

  // console.log(Object.values(consolidatedWallet).flat());
  const [
    sidebarBackgroundColor,
    activeFontColor,
    inactiveFontColor,
    shadowColor,
  ] = useThemeValue([
    'surface-subdued',
    'text-default',
    'text-subdued',
    'interactive-default',
  ]);

  const onPressAddress = useCallback(
    async (idx: number, _key: string) => {
      backgroundApiProxy.dispatch(setHomeTabName(WalletHomeTabEnum.AllCoins));
      setOpenAccord2({
        ...openAccord2,
        isPanelShow:
          idx === openAccord2?.activePanel ? !openAccord2?.isPanelShow : true,
        activePanel: idx,
      });
      await onPressChangeAccount({
        accountId: consolidatedWallet[_key][0]?.id,
        networkId: derivedChainFromCoinType(_key)?.networkId,
        walletId,
        accountSelectorMode,
        fromDesktopSideBar: true,
      });
    },
    [
      openAccord2,
      accountSelectorMode,
      onPressChangeAccount,
      walletId,
      consolidatedWallet,
    ],
  );

  const getActiveWalletName = useCallback(
    (name: string | ReactNode): string | undefined | ReactNode => {
      if (name === 'Wallet') {
        if (walletIsHD(walletId)) return wallet?.name;
        if (walletIsImported(walletId))
          return intl.formatMessage({ id: 'wallet__imported_accounts' });
        if (wallet?.type === 'watching')
          return intl.formatMessage({ id: 'wallet__watched_accounts' });
        if (wallet?.type === 'external')
          return intl.formatMessage({ id: 'content__external_account' });
      }
      return name;
    },
    [intl, walletId, wallet],
  );

  // const navigationNode = useNav<Route>();
  const tabs = useMemo(
    () =>
      routes.map((route, index) => {
        const isActive = index === state.index;
        const { options } = descriptors[route.key];
        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isActive && !event.defaultPrevented) {
            navigation.dispatch({
              ...CommonActions.navigate({
                name: route.name,
                merge: true,
              }),
              target: state.key,
            });
          }
          if (route.name === 'home') {
            if (!wallet || !wallet.accounts.length) return;
            setOpenAccord2({
              isPanelShow: false,
              activePanel: -1,
            });
            setOpenAccord(() => !openAccord);
          } else {
            setOpenAccord(false);
          }
        };
        return (
          <>
            {/* {console.log(options)} */}
            <Tooltip
              key={route.key}
              label={options.tabBarLabel as string}
              isDisabled={!isCollpase}
              placement="right"
            >
              <Pressable
                key={route.name}
                onPress={onPress}
                flexDirection="row"
                alignItems="center"
                mt={index === routes.length - 1 ? 'auto' : undefined}
                p="8px"
                borderRadius="xl"
                bg={isActive ? 'surface-highlight-default' : undefined}
                borderWidth={isActive ? 2 : '0'}
                borderColor={isActive ? 'interactive-default' : 'none'}
                _hover={
                  !isActive
                    ? { bg: 'surface-hovered', color: 'text-on-primary' }
                    : undefined
                }
                aria-current={isActive ? 'page' : undefined}
                color={isActive ? 'surface-selected' : undefined}
              >
                <Box>
                  <Icon
                    // @ts-expect-error
                    name={options?.tabBarIcon?.() as ICON_NAMES}
                    color={isActive ? 'icon-default' : 'icon-subdued'}
                    size={24}
                  />
                </Box>

                <AnimatePresence initial={false}>
                  {!isCollpase && (
                    <MotiView
                      key={route.key}
                      from={{ opacity: 0 }}
                      animate={{ opacity: 1 }}
                      exit={{
                        opacity: 0,
                      }}
                      transition={{
                        type: 'timing',
                        duration: 150,
                      }}
                      style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}
                    >
                      <Typography.Body2Strong
                        flex={1}
                        ml="3"
                        color={isActive ? activeFontColor : inactiveFontColor}
                        isTruncated
                      >
                        {getActiveWalletName(options.tabBarLabel ?? route.name)}
                      </Typography.Body2Strong>
                    </MotiView>
                  )}
                </AnimatePresence>
                {route.name === 'home' && !isCollpase && (
                  <span
                    style={{
                      transform: openAccord ? 'rotate(90deg)' : 'rotate(0deg)',
                      transition: '0.5s all ease',
                      borderRadius: '90px',
                    }}
                  >
                    <Box bgColor="interactive-default" rounded="full">
                      <Icon
                        name="ChevronRightMini"
                        size={18}
                        color="text-on-primary"
                      />
                    </Box>
                  </span>
                )}
              </Pressable>
            </Tooltip>
            {openAccord && !isCollpase ? (
              <>
                {route.name === 'home' ? (
                  <div
                    style={{
                      padding: '0 5px',
                      display: 'flex',
                      flexDirection: 'column',
                      gap: '5px',
                    }}
                  >
                    {Object.keys(consolidatedWallet).map((_key, idx) => {
                      const isActivePanelMenu =
                        networkId === derivedChainFromCoinType(_key)?.networkId;
                      return (
                        <Pressable
                          key={_key}
                          onPress={() => onPressAddress(idx, _key)}
                          flexDirection="row"
                          alignItems="center"
                          mt={index === routes.length - 1 ? 'auto' : undefined}
                          p="8px"
                          borderRadius="xl"
                          borderWidth={isActivePanelMenu ? '2' : '0'}
                          borderColor={
                            isActivePanelMenu ? 'interactive-default' : 'none'
                          }
                          bg={
                            isActivePanelMenu
                              ? 'surface-highlight-default'
                              : undefined
                          }
                          _hover={
                            !isActivePanelMenu
                              ? {
                                  bg: 'surface-hovered',
                                  color: 'text-on-primary',
                                }
                              : undefined
                          }
                          // aria-current={isActive ? 'page' : undefined}
                          color={
                            !isActivePanelMenu ? 'surface-selected' : undefined
                          }
                        >
                          <Box
                            key={Number(_key) + 21}
                            flexDirection="column"
                            w="100%"
                          >
                            <Box flexDirection="row" alignItems="center">
                              <Box>
                                <div style={{ position: 'relative' }}>
                                  <Image
                                    src={derivedChainFromCoinType(_key)?.logo}
                                    style={{ width: '22px', height: '22px' }}
                                  />
                                  {isActivePanelMenu && (
                                    <Speedindicator
                                      position="absolute"
                                      top={-1}
                                      right={-1}
                                      size="10px"
                                      backgroundColor="interactive-default"
                                    />
                                  )}
                                </div>
                              </Box>
                              <AnimatePresence initial={false}>
                                <MotiView
                                  key={Number(_key) + 61}
                                  from={{ opacity: 0 }}
                                  animate={{ opacity: 1 }}
                                  exit={{
                                    opacity: 0,
                                  }}
                                  transition={{
                                    type: 'timing',
                                    duration: 150,
                                  }}
                                  style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                  }}
                                >
                                  <Typography.Body2Strong
                                    flex={1}
                                    ml="3"
                                    color={
                                      isActivePanelMenu
                                        ? activeFontColor
                                        : inactiveFontColor
                                    }
                                    isTruncated
                                  >
                                    {derivedChainFromCoinType(_key)?.name}
                                  </Typography.Body2Strong>
                                </MotiView>
                                <span
                                  style={{
                                    transform:
                                      openAccord2?.isPanelShow &&
                                      isActivePanelMenu
                                        ? 'rotate(90deg)'
                                        : 'rotate(0deg)',
                                    transition: '0.5s all ease',
                                    borderRadius: '90px',
                                  }}
                                >
                                  <Box>
                                    <Icon
                                      name="ChevronRightMini"
                                      size={18}
                                      color="icon-subdued"
                                    />
                                  </Box>
                                </span>
                              </AnimatePresence>
                            </Box>
                            {openAccord2?.isPanelShow &&
                              openAccord2?.activePanel === idx && (
                                <div key={idx} style={{ width: '100%' }}>
                                  <Box
                                    key={idx}
                                    p={0}
                                    mt={3}
                                    // bgColor="surface-highlight-default"
                                    flexDirection="column"
                                    w="100%"
                                    alignItems="center"
                                    overflowX="hidden"
                                    maxHeight="150px"
                                    overflowY="auto"
                                  >
                                    {consolidatedWallet[_key]?.map((data) => {
                                      const isActiveSubMenu =
                                        networkId ===
                                          derivedChainFromCoinType(_key)
                                            ?.networkId &&
                                        accountAddress === data?.address;
                                      const isInactiveAddress =
                                        networkId ===
                                          derivedChainFromCoinType(_key)
                                            ?.networkId &&
                                        accountAddress !== data?.address;
                                      return (
                                        <>
                                          <Pressable
                                            key={data.id}
                                            p="5px 2px"
                                            onPress={(e) => {
                                              e.preventDefault();
                                              e.stopPropagation();
                                              onPressChangeAccount({
                                                accountId: data.id,
                                                networkId:
                                                  derivedChainFromCoinType(_key)
                                                    ?.networkId,
                                                walletId,
                                                accountSelectorMode,
                                                fromDesktopSideBar: true,
                                              });
                                            }}
                                            flexDirection="row"
                                            alignItems="center"
                                            w="100%"
                                            mt={
                                              index === routes.length - 1
                                                ? 'auto'
                                                : undefined
                                            }
                                            // borderRadius="xl"
                                            bg={
                                              isActiveSubMenu
                                                ? 'surface-highlight-default'
                                                : undefined
                                            }
                                            _hover={
                                              isInactiveAddress
                                                ? {
                                                    bg: 'surface-hovered',
                                                    color: 'text-on-primary',
                                                  }
                                                : undefined
                                            }
                                            color={
                                              isActiveSubMenu
                                                ? 'surface-selected'
                                                : undefined
                                            }
                                          >
                                            <AnimatePresence
                                              key={data.id}
                                              initial={false}
                                            >
                                              {!isCollpase && (
                                                <MotiView
                                                  key={data.id}
                                                  from={{ opacity: 0 }}
                                                  animate={{ opacity: 1 }}
                                                  exit={{
                                                    opacity: 0,
                                                  }}
                                                  transition={{
                                                    type: 'timing',
                                                    duration: 150,
                                                  }}
                                                  style={{
                                                    flex: 1,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                  }}
                                                >
                                                  {isActiveSubMenu && (
                                                    <Speedindicator
                                                      // position="absolute"
                                                      // top={-1}
                                                      // right={-1}
                                                      size="10px"
                                                      backgroundColor="interactive-default"
                                                    />
                                                  )}
                                                  <Text
                                                    typography={
                                                      isActiveSubMenu
                                                        ? 'Body2Strong'
                                                        : 'Caption'
                                                    }
                                                    flex={1}
                                                    ml="3"
                                                    color={
                                                      isActiveSubMenu
                                                        ? 'interactive-default'
                                                        : inactiveFontColor
                                                    }
                                                    isTruncated
                                                  >
                                                    {shortenAddress(
                                                      data?.displayAddress ??
                                                        data?.address ??
                                                        '',
                                                    )}
                                                  </Text>
                                                  {isActiveSubMenu ? (
                                                    <DesktopSidebarMoreMenu>
                                                      <IconButton
                                                        name="EllipsisVerticalOutline"
                                                        size="sm"
                                                        type="plain"
                                                        circle
                                                      />
                                                    </DesktopSidebarMoreMenu>
                                                  ) : null}
                                                </MotiView>
                                              )}
                                            </AnimatePresence>
                                          </Pressable>
                                          <Divider bg="border-subdued" />
                                        </>
                                      );
                                    })}
                                  </Box>
                                </div>
                              )}
                          </Box>
                        </Pressable>
                      );
                    })}
                  </div>
                ) : null}
              </>
            ) : null}
          </>
        );
      }),
    [
      activeFontColor,
      descriptors,
      inactiveFontColor,
      isCollpase,
      navigation,
      routes,
      state.index,
      state.key,
      onPressAddress,
      openAccord,
      openAccord2,
      wallet,
      accountAddress,
      accountSelectorMode,
      onPressChangeAccount,
      walletId,
      networkId,
      consolidatedWallet,
      getActiveWalletName,
    ],
  );

  return (
    <MotiView
      animate={{ width: isCollpase ? 72 : 224 }}
      transition={{
        type: 'timing',
        duration: 150,
      }}
      style={{
        height: '100%',
        width: 224,
        backgroundColor: sidebarBackgroundColor,
        paddingHorizontal: 16,
        paddingTop: paddingTopValue,
        paddingBottom: 20,
      }}
      testID="Desktop-AppSideBar-Container"
    >
      <DesktopDragZoneAbsoluteBar
        testID="Desktop-AppSideBar-DragZone"
        h={dragZoneAbsoluteBarHeight}
      />
      {/* Scrollable area */}
      <Box zIndex={1} testID="Desktop-AppSideBar-WalletSelector-Container">
        {/* <AccountSelector /> */}
        <WalletSelectorTrigger showWalletName={!isCollpase} />
      </Box>
      <VStack
        testID="Desktop-AppSideBar-Content-Container"
        flex={1}
        mt={4}
        mb={2}
      >
        <ScrollView
          _contentContainerStyle={{
            flex: 1,
          }}
        >
          <VStack space={1} flex={1}>
            {tabs}
          </VStack>
        </ScrollView>
      </VStack>
      <Box testID="Legacy-Desktop-AppSideBar-NetworkAccountSelector-Container">
        {/* <ChainSelector /> */}
        {/* <NetworkAccountSelectorTrigger /> */}
      </Box>
      <Pressable
        onPress={() => {
          setIsCollapse?.(!isCollpase);
        }}
        position="absolute"
        top="0"
        bottom="0"
        right="-10px"
        w="20px"
        testID="Desktop-AppSideBar-Collapse-Bar"
      >
        {({ isHovered, isPressed }) => (
          <MotiView
            animate={{ opacity: isHovered || isPressed ? 1 : 0 }}
            transition={{ type: 'timing', duration: 150 }}
            style={{ height: '100%', flexDirection: 'row' }}
          >
            <LinearGradient
              colors={['transparent', shadowColor]}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={{
                flex: 1,
                height: '100%',
                opacity: 0.1,
              }}
            />
            <Box
              h="full"
              w="1px"
              bgColor="interactive-default"
              alignItems="center"
            >
              <Center mt={`${paddingTopValue}px`} size="40px">
                <Box
                  p="4px"
                  rounded="full"
                  bgColor="background-default"
                  borderWidth="1px"
                  borderColor="border-subdued"
                  shadow="depth.1"
                >
                  <MotiView
                    animate={{ rotate: isCollpase ? '180deg' : '0deg' }}
                    transition={{ type: 'timing' }}
                  >
                    <Icon name="ChevronLeftMini" size={16} />
                  </MotiView>
                </Box>
              </Center>
            </Box>
            <Box flex={1} />
          </MotiView>
        )}
      </Pressable>
    </MotiView>
  );
};
export default Sidebar;
