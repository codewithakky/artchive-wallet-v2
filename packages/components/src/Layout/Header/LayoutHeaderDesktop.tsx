import { useCallback, useEffect } from 'react';

import LayoutHeader from './index';

import { Box, HStack, IconButton, useTheme } from '@onekeyhq/components';
import backgroundApiProxy from '@onekeyhq/kit/src/background/instance/backgroundApiProxy';
import { NetworkAccountSelectorTriggerDesktop } from '@onekeyhq/kit/src/components/NetworkAccountSelector';
import { useCheckUpdate } from '@onekeyhq/kit/src/hooks/useCheckUpdate';
import {
  THEME_PRELOAD_STORAGE_KEY,
  setTheme,
} from '@onekeyhq/kit/src/store/reducers/settings';
import HomeMoreMenu from '@onekeyhq/kit/src/views/Overlay/HomeMoreMenu';
import debugLogger from '@onekeyhq/shared/src/logger/debugLogger';

import type { MessageDescriptor } from 'react-intl';

export function LayoutHeaderDesktop({
  i18nTitle,
}: {
  i18nTitle?: MessageDescriptor['id'];
}) {
  const { showUpdateBadge } = useCheckUpdate();
  const { isLight } = useTheme();
  const { dispatch } = backgroundApiProxy;
  useEffect(() => {
    debugLogger.autoUpdate.debug(
      'LayoutHeaderDesktop showUpdateBadge effect: ',
      showUpdateBadge,
    );
  }, [showUpdateBadge]);
  const onChangeTheme = useCallback(() => {
    const key = THEME_PRELOAD_STORAGE_KEY;
    const localTheme = localStorage.getItem(key);
    if (localTheme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  }, [dispatch]);

  const headerRight = useCallback(
    () => (
      <HStack space={2} alignItems="center">
        <NetworkAccountSelectorTriggerDesktop />
        <Box>
          <IconButton
            onPress={onChangeTheme}
            name={isLight ? 'MoonOutline' : 'SunOutline'}
            size="lg"
            type="plain"
            circle
            m={-2}
          />
        </Box>
        <Box>
          <HomeMoreMenu>
            <IconButton
              name="EllipsisVerticalOutline"
              size="lg"
              type="plain"
              circle
              m={-2}
            />
          </HomeMoreMenu>
          {showUpdateBadge && (
            <Box
              position="absolute"
              top="-3px"
              right="-8px"
              rounded="full"
              p="2px"
              pr="9px"
            >
              <Box rounded="full" bgColor="interactive-default" size="8px" />
            </Box>
          )}
        </Box>
      </HStack>
    ),
    [showUpdateBadge, isLight, onChangeTheme],
  );

  return (
    <LayoutHeader
      testID="App-Layout-Header-Desktop"
      showOnDesktop
      // headerLeft={() => <AccountSelector />}
      // headerLeft={() => null}
      i18nTitle={i18nTitle}
      // headerRight={() => <ChainSelector />}
      // headerRight={() => <NetworkAccountSelectorTrigger />}
      headerRight={headerRight}
    />
  );
}
