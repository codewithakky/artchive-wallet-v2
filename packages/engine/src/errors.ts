import { Web3RpcError } from '@onekeyfe/cross-inpage-provider-errors';
import { get } from 'lodash';

/* eslint max-classes-per-file: "off" */

import type { LocaleIds } from '@onekeyhq/components/src/locale';

export enum ArtChiveErrorClassNames {
  ArtChiveError = 'ArtChiveError',
  ArtChiveHardwareError = 'ArtChiveHardwareError',
  ArtChiveValidatorError = 'ArtChiveValidatorError',
  ArtChiveValidatorTip = 'ArtChiveValidatorTip',
  ArtChiveAbortError = 'ArtChiveAbortError',
  ArtChiveWalletConnectModalCloseError = 'ArtChiveWalletConnectModalCloseError',
  ArtChiveAlreadyExistWalletError = 'ArtChiveAlreadyExistWalletError',
}

export type IArtChiveErrorInfo = Record<string | number, string | number>;

export type ArtChiveHardwareErrorData = {
  reconnect?: boolean | undefined;
  connectId?: string;
  deviceId?: string;
};

export type ArtChiveHardwareErrorPayload = {
  code?: number;
  error?: string;
  message?: string;
  params?: any;
  connectId?: string;
  deviceId?: string;
};

export class ArtChiveError<T = Error> extends Web3RpcError<T> {
  className = ArtChiveErrorClassNames.ArtChiveError;

  info: IArtChiveErrorInfo;

  key = 'onekey_error';

  constructor(message?: string, info?: IArtChiveErrorInfo) {
    super(-99999, message || 'Unknown onekey internal error.');
    this.info = info || {};
  }

  override get message() {
    // TODO key message with i18n
    // @ts-ignore
    return super.message || this.key;
  }
}

class NumberLimit extends ArtChiveError {
  override key = 'generic_number_limitation';

  constructor(limit: number) {
    super('', { limit: limit.toString() });
  }
}

class StringLengthRequirement extends ArtChiveError {
  override key = 'generic_string_length_requirement';

  constructor(minLength: number, maxLength: number) {
    super('', {
      minLength: minLength.toString(),
      maxLength: maxLength.toString(),
    });
  }
}

// Generic errors.

export class NotImplemented extends ArtChiveError {
  constructor(message?: string) {
    super(message || 'ArtChiveError: NotImplemented', {});
  }

  override key = 'msg__engine__not_implemented';
}

export class ArtChiveInternalError extends ArtChiveError {
  override key = 'msg__engine__internal_error';

  constructor(message?: string, key?: LocaleIds) {
    super(message || 'ArtChiveError: Internal error', {});
    if (key) {
      this.key = key;
    }
  }
}

export class ArtChiveHardwareError<
  T extends ArtChiveHardwareErrorData = ArtChiveHardwareErrorData,
> extends ArtChiveError<T> {
  override className = ArtChiveErrorClassNames.ArtChiveHardwareError;

  codeHardware?: string;

  override key: LocaleIds = 'msg__hardware_default_error';

  static handleErrorParams(
    params?: any,
    errorParams?: Record<string | number, string>,
  ): IArtChiveErrorInfo {
    const info: IArtChiveErrorInfo = {};
    Object.keys(errorParams || {}).forEach((key) => {
      const valueKey = errorParams?.[key];
      if (valueKey) {
        const value = get(params, valueKey, '');
        info[key] = value;
      }
    });

    return info;
  }

  /**
   * create ArtChiveHardwareError from ArtChiveHardware error payload
   * @param errorPayload Hardware error payload
   * @param errorParams Hardware Error params, key is i18n placeholder, value is error payload key
   */
  constructor(
    errorPayload?: ArtChiveHardwareErrorPayload,
    errorParams?: Record<string | number, string>,
    data?: T,
  ) {
    super(
      errorPayload?.error ?? errorPayload?.message ?? 'Unknown hardware error',
      ArtChiveHardwareError.handleErrorParams(
        errorPayload?.params,
        errorParams,
      ) || {},
    );
    const { code, deviceId, connectId } = errorPayload || {};
    this.codeHardware = code?.toString();
    this.data = {
      deviceId,
      connectId,
      reconnect: this.data?.reconnect,
      ...data,
    } as T;
  }
}

export class ArtChiveHardwareAbortError extends ArtChiveError {
  override className = ArtChiveErrorClassNames.ArtChiveAbortError;

  override key = 'msg__engine__internal_error';
}

export class ArtChiveAlreadyExistWalletError extends ArtChiveHardwareError<
  {
    walletId: string;
    walletName: string | undefined;
  } & ArtChiveHardwareErrorData
> {
  override className = ArtChiveErrorClassNames.ArtChiveAlreadyExistWalletError;

  override key: LocaleIds = 'msg__wallet_already_exist';

  constructor(walletId: string, walletName: string | undefined) {
    super(undefined, undefined, { walletId, walletName });
  }
}

export class ArtChiveValidatorError extends ArtChiveError {
  override className = ArtChiveErrorClassNames.ArtChiveValidatorError;

  override key = 'onekey_error_validator';

  constructor(key: string, info?: IArtChiveErrorInfo, message?: string) {
    super(message, info);
    this.key = key;
  }
}

export class ArtChiveValidatorTip extends ArtChiveError {
  override className = ArtChiveErrorClassNames.ArtChiveValidatorTip;

  override key = 'onekey_tip_validator';

  constructor(key: string, info?: IArtChiveErrorInfo, message?: string) {
    super(message, info);
    this.key = key;
  }
}

export class FailedToTransfer extends ArtChiveError {
  override key = 'msg__engine__failed_to_transfer';
}

export class WrongPassword extends ArtChiveError {
  override key = 'msg__engine__incorrect_password';
}

export class PasswordStrengthValidationFailed extends ArtChiveError {
  override key = 'msg__password_validation';
}

// Simple input errors.

export class InvalidMnemonic extends ArtChiveError {
  override key = 'msg__engine__invalid_mnemonic';
}

export class MimimumBalanceRequired extends ArtChiveError {
  override key = 'msg__str_minimum_balance_is_str';

  constructor(token: string, amount: string) {
    super('', { token, amount });
  }
}

export class RecipientHasNotActived extends ArtChiveError {
  override key = 'msg__recipient_hasnt_activated_str';

  constructor(tokenName: string) {
    super('', { '0': tokenName });
  }
}

export class InvalidAddress extends ArtChiveError {
  override key = 'msg__engine__incorrect_address';

  constructor(message?: string, info?: IArtChiveErrorInfo) {
    super(message || 'InvalidAddress.', info);
  }
}

export class InvalidSameAddress extends ArtChiveError {
  override key = 'form__address_cannot_send_to_myself';

  constructor(message?: string, info?: IArtChiveErrorInfo) {
    super(message || 'InvalidAddress.', info);
  }
}

export class InvalidAccount extends ArtChiveError {
  override key = 'msg__engine__account_not_activated';

  constructor(message?: string, info?: IArtChiveErrorInfo) {
    super(message || 'InvalidAccount.', info);
  }
}

export class InvalidTokenAddress extends ArtChiveError {
  override key = 'msg__engine__incorrect_token_address';
}

export class InvalidTransferValue extends ArtChiveError {
  override key = 'msg__engine__incorrect_transfer_value';

  constructor(key?: string, info?: IArtChiveErrorInfo) {
    super('Invalid Transfer Value', info);
    this.key = key ?? 'msg__engine__incorrect_transfer_value';
  }
}

export class TransferValueTooSmall extends ArtChiveError {
  override key = 'msg__amount_too_small';

  constructor(key?: string, info?: IArtChiveErrorInfo) {
    super('Transfer Value too small', info);
    this.key = key ?? 'msg__amount_too_small';
  }
}

export class InsufficientBalance extends ArtChiveError {
  // For situations that utxo selection failed.
  override key = 'form__amount_invalid';
}

export class WalletNameLengthError extends StringLengthRequirement {
  override key = 'msg__engine__wallet_name_length_error';
}

export class AccountNameLengthError extends StringLengthRequirement {
  override key = 'msg__engine__account_name_length_error';

  constructor(name: string, minLength: number, maxLength: number) {
    super(minLength, maxLength);
    this.info = { name, ...this.info };
  }
}

export class WatchedAccountTradeError extends ArtChiveError {
  override key = 'form__error_trade_with_watched_acocunt';
}

// Limitations.

export class AccountAlreadyExists extends ArtChiveError {
  override key = 'msg__engine__account_already_exists';
}

export class PreviousAccountIsEmpty extends ArtChiveError {
  override key = 'content__previous_str_account_is_empty';

  constructor(accountTypeStr: string, key?: LocaleIds) {
    super('', { '0': accountTypeStr });
    if (key) {
      this.key = key;
    }
  }
}

export class TooManyWatchingAccounts extends NumberLimit {
  override key = 'msg__engine_too_many_watching_accounts';
}

export class TooManyExternalAccounts extends NumberLimit {
  override key = 'msg__engine_too_many_external_accounts';
}

export class TooManyImportedAccounts extends NumberLimit {
  override key = 'msg__engine__too_many_imported_accounts';
}

export class TooManyHDWallets extends NumberLimit {
  override key = 'msg__engine__too_many_hd_wallets';
}

export class TooManyHWWallets extends NumberLimit {
  override key = 'msg__engine__too_many_hw_wallets';
}

export class TooManyHWPassphraseWallets extends NumberLimit {
  override key = 'msg__engine__too_many_hw_passphrase_wallets';
}

export class TooManyDerivedAccounts extends NumberLimit {
  override key = 'msg__engine__too_many_derived_accounts';

  constructor(limit: number, coinType: number, purpose: number) {
    super(limit);
    this.info = {
      coinType: coinType.toString(),
      purpose: purpose.toString(),
      ...this.info,
    };
  }
}

export class PendingQueueTooLong extends NumberLimit {
  override key = 'msg__engine__pending_queue_too_long';
}

// WalletConnect ----------------------------------------------
export class ArtChiveWalletConnectModalCloseError extends ArtChiveError {
  override className =
    ArtChiveErrorClassNames.ArtChiveWalletConnectModalCloseError;
  // override key = 'msg__engine__internal_error';
}

export class FailedToEstimatedGasError extends ArtChiveError {
  override key = 'msg__estimated_gas_failure';
}
