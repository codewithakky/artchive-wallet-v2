const endpointsMap: Record<
  'fiat' | 'wss' | 'covalent',
  { prd: string; test: string; custom: string }
> = {
  fiat: {
    prd: 'https://api.onekeycn.com/api',
    test: 'https://api-sandbox.onekeytest.com/api',
    custom: 'https://api.covalenthq.com/v1/',
  },
  wss: {
    prd: 'wss://api.onekeycn.com',
    test: 'wss://api-sandbox.onekeytest.com',
    custom: 'https://api.covalenthq.com/v1/',
  },
  covalent: {
    prd: 'https://node.onekey.so/covalent/client1-HghTg3a33',
    test: 'https://node.onekeytest.com/covalent/client1-HghTg3a33',
    custom: 'https://api.covalenthq.com',
  },
};

let endpoint = '';
let websocketEndpoint = '';
let covalentApiEndpoint = '';

export const switchTestEndpoint = (isTestEnable?: boolean) => {
  const key = isTestEnable ? 'test' : 'prd';
  // const key = 'custom';
  endpoint = endpointsMap.fiat[key];
  websocketEndpoint = endpointsMap.wss[key];
  covalentApiEndpoint = endpointsMap.covalent.custom;
};

switchTestEndpoint(false);

export const getFiatEndpoint = () => endpoint;
export const getSocketEndpoint = () => websocketEndpoint;
export const getCovalentApiEndpoint = () => covalentApiEndpoint;
