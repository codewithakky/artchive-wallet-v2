import type { StringPublicKey } from '../types';

export class Creator {
  address: StringPublicKey;

  verified: boolean;

  share: number;

  constructor(args: {
    address: StringPublicKey;
    verified: boolean;
    share: number;
  }) {
    this.address = args.address;
    this.verified = args.verified;
    this.share = args.share;
  }
}
