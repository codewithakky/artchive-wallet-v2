import {
  type Commitment,
  Connection,
  PublicKey,
  VersionedTransaction,
  clusterApiUrl,
} from '@solana/web3.js';
import { BinaryReader, BinaryWriter, deserializeUnchecked } from 'borsh';
import bs58 from 'bs58';

import type { SignedTx, UnsignedTx } from '@onekeyhq/engine/src/types/provider';

import {
  METADATA_PREFIX,
  METADATA_PROGRAM,
  METADATA_SCHEMA,
  Metadata,
} from './config/metplex';

import type { Signer } from '../../../proxy';
import type { INativeTxSol } from './types';

export const extendBorsh = () => {
  (BinaryReader.prototype as any).readPubkey = function () {
    const reader = this as unknown as BinaryReader;
    const array = reader.readFixedArray(32);
    return new PublicKey(array);
  };

  (BinaryWriter.prototype as any).writePubkey = function (value: PublicKey) {
    const writer = this as unknown as BinaryWriter;
    writer.writeFixedArray(value.toBuffer());
  };
};

extendBorsh();

export async function signTransaction(
  unsignedTx: UnsignedTx,
  signer: Signer,
): Promise<SignedTx> {
  const { nativeTx: transaction, feePayer } = unsignedTx.payload as {
    nativeTx: INativeTxSol;
    feePayer: PublicKey;
  };

  const isVersionedTransaction = transaction instanceof VersionedTransaction;

  const [sig] = await signer.sign(
    isVersionedTransaction
      ? Buffer.from(transaction.message.serialize())
      : transaction.serializeMessage(),
  );
  transaction.addSignature(feePayer, sig);

  return {
    txid: bs58.encode(sig),
    rawTx: Buffer.from(
      transaction.serialize({ requireAllSignatures: false }),
    ).toString('base64'),
  };
}

export async function signMessage(
  message: string,
  signer: Signer,
): Promise<string> {
  const [signature] = await signer.sign(Buffer.from(message));
  return bs58.encode(signature);
}

const metaProgamPublicKey = new PublicKey(METADATA_PROGRAM);
const metaProgamPublicKeyBuffer = metaProgamPublicKey.toBuffer();
// Create UTF-8 bytes Buffer from string
// similar to Buffer.from(METADATA_PREFIX) but should work by default in node.js/browser
const metaProgamPrefixBuffer = new TextEncoder().encode(METADATA_PREFIX);

export const decodeTokenMetadata = (buffer: Buffer) =>
  deserializeUnchecked(METADATA_SCHEMA, Metadata, buffer);

/**
 * Get Addresses of Metadata account assosiated with Mint Token
 */
export async function getSolanaMetadataAddress(tokenMint: PublicKey) {
  const metaProgamPublicKeyNew = new PublicKey(METADATA_PROGRAM);
  return (
    await PublicKey.findProgramAddress(
      [metaProgamPrefixBuffer, metaProgamPublicKeyBuffer, tokenMint.toBuffer()],
      metaProgamPublicKeyNew,
    )
  )[0];
}

export const createConnectionConfig = (
  clusterApi = clusterApiUrl('mainnet-beta'),
  commitment = 'confirmed',
) => new Connection(clusterApi, commitment as Commitment);
