module.exports = {
  singleQuote: true,
  trailingComma: 'all',
  endOfLine: 'auto',
  quoteProps: 'preserve', // let eslint fix quoteProps
  // "printWidth": 120,
  // "tabWidth": 8,
};
